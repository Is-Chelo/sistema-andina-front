import {Button, Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';

export const columnsRender = (
	handleEdit,
	handleDelete,
	dataPermission,
	handlerShow,
	setShowModal
) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 50},
		{
			field: 'name',
			headerName: 'Nombre del Programa',
			description: 'Nombre del Programa',
			width: 180,
		},
		{
			field: 'sede',
			headerName: 'Sede',
			description: 'Sede del Programa',
			width: 80,
			valueGetter: (params) =>
				`${params.row.sede.charAt(0).toUpperCase() + params.row.sede.substring(1) || ''}`,
		},
		{
			field: 'resolution_number',
			headerName: 'Número de Resolución',
			description: 'Número de Resolución',
			width: 180,
		},
		{
			field: 'version',
			headerName: 'Versión del programa',
			description: 'Versión del Programa',
			width: 180,
		},
		{
			field: 'area_name',
			headerName: 'Nombre del Area',
			description: 'Nombre del Area',
			width: 150,
		},
		{
			field: 'date_init',
			headerName: 'Fecha de Inicio',
			description: 'Fecha de Inicio',
			width: 200,
			valueGetter: (params) => `${moment(params.row.date_init).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'date_end',
			headerName: 'Fecha de Finalizacion',
			description: 'Fecha de Finalizacion',
			width: 200,
			valueGetter: (params) => `${moment(params.row.date_end).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'total_students',
			headerName: 'Estudiantes',
			width: 150,
			renderCell: (params) => (
				<Button
					onClick={() => {
						handlerShow(params.id);
						setShowModal(true);
					}}
					startIcon={<RemoveRedEyeIcon />}>
					ver
				</Button>
			),
		},
		{
			field: 'active',
			headerName: 'Activo',
			width: 100,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},

		accionOperations,
	];
};
