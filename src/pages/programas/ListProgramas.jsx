import {Backdrop, Box, Drawer, Fade, Modal, useMediaQuery} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useEffect, useState} from 'react';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {deleteRequest, getRequest} from '../../api/Requests';
import Edit from './Edit';
import {AddPrograma} from './AddPrograma';
import Swal from 'sweetalert2';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import moment from 'moment';
import {CustomButtons2} from '../../components/CustomButtons2';
import {useTheme} from '@emotion/react';
import {columnsRenderEstudiante} from './columnsRenderEstudiante';
import {CustomButtons3} from '../../components/CustomButtons3';

export const ListProgramas = () => {
	// MODAL VER
	const [open, setOpen] = useState(false);
	const handleClose = () => setOpen(false);
	const [estudiantesData, setEstudiantessData] = useState([]);
	const [idPRintReporte, setIdPRintReporte] = useState();
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));

	const [change, setChange] = useState(false);
	const [state, setState] = useState(false);
	const [edit, setEdit] = useState(false);
	const [changePermissions, setChangePermissions] = useState(false);

	const [initialValues, setInitialValues] = useState({});

	const data = useFetch('api/v1/program', change);
	const toggleDrawer = (open) => (event) => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		setEdit(false);
		setState(open);
	};

	// PERMISOS
	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/program');
				setChange(!change);
			}
		});
	};

	const handleEdit = async (id) => {
		const datos = await getRequest(`api/v1/program/${id}`);
		setInitialValues({
			name: datos.name,
			date_init: moment(datos.date_init).format('YYYY-MM-DD'),
			date_end: moment(datos.date_end).format('YYYY-MM-DD'),
			id_area: {id: datos.id_area, name: datos.area_name},
			version: datos.version,
			active: datos.active ? 1 : 0,
			sede: datos.sede,
			resolution_number: datos.resolution_number,
			id: id,
		});
		setEdit(true);
		setState(true);
	};

	const handlerShow = async (id) => {
		const datos = await getRequest(`api/v1/student/student-by-program/${id}`);
		setEstudiantessData(datos);
		setIdPRintReporte(id);
	};

	const columns = columnsRender(handleEdit, handleDelete, dataPermission, handlerShow, setOpen);
	const columnsCoodinador = columnsRenderEstudiante();

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud
					title='Programas'
					parrafo='Listado de Programas.'
					category={'Gestión académica'}
				/>
			</div>
			<MainCard>
				<div className='animate__animated animate__fadeIn'>
					<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
						{data?.status === false ? (
							<></>
						) : (
							<>
								{data !== undefined ? (
									<DataGrid
										rows={data}
										columns={columns}
										pageSize={5}
										rowsPerPageOptions={[5]}
										slots={{
											toolbar: CustomButtons2,
										}}
										// slots={{toolbar: GridToolbar}}
										checkboxSelection
										disableRowSelectionOnClick
										unstable_cellSelection
										experimentalFeatures={{clipboardPaste: true}}
										unstable_ignoreValueFormatterDuringExport
										// FIlters
										slotProps={{
											toolbar: {
												showQuickFilter: true,
												quickFilterProps: {debounceMs: 500},
												url: '/api/v1/program/reporte',
												toggleDrawer: toggleDrawer,
												okInsert: dataPermission?.ok_insert,
												name: 'reporte-programas',
											},
										}}
										disableColumnFilter
										disableColumnSelector
										disableDensitySelector
									/>
								) : (
									<SkeletonLoading />
								)}
							</>
						)}
					</div>
				</div>
			</MainCard>

			<div>
				<Drawer anchor='right' open={state} onClose={toggleDrawer(false)}>
					<Box sx={{width: matchXS ? 320 : 500}} role='presentation'>
						{edit ? (
							<Edit
								change={change}
								setChange={setChange}
								setState={setState}
								initialValues={initialValues}
							/>
						) : (
							<AddPrograma
								change={change}
								setChange={setChange}
								setState={setState}
							/>
						)}
					</Box>
				</Drawer>
			</div>

			<div>
				<Modal
					aria-labelledby='transition-modal-title'
					aria-describedby='transition-modal-description'
					open={open}
					onClose={handleClose}
					closeAfterTransition
					slots={{backdrop: Backdrop}}
					slotProps={{
						backdrop: {
							timeout: 500,
						},
					}}>
					<Fade in={open}>
						<Box
							sx={{
								position: 'absolute',
								top: '50%',
								left: '50%',
								transform: 'translate(-50%, -50%)',
								width: '30vw',
								height: '50vh',
								bgcolor: 'background.paper',
								boxShadow: 24,
								borderRadius: '10px',
								p: 4,
							}}>
							{/* <Typography id='transition-modal-title' variant='h4' component='h2'>
								Estudiantes
							</Typography> */}

							<DataGrid
								rows={estudiantesData}
								columns={columnsCoodinador}
								pageSize={5}
								rowsPerPageOptions={[5]}
								slots={{
									toolbar: CustomButtons3,
								}}
								// slots={{toolbar: GridToolbar}}
								checkboxSelection
								disableRowSelectionOnClick
								unstable_cellSelection
								experimentalFeatures={{clipboardPaste: true}}
								unstable_ignoreValueFormatterDuringExport
								// FIlters
								slotProps={{
									toolbar: {
										showQuickFilter: true,
										quickFilterProps: {debounceMs: 500},
										url: `api/v1/student/student-by-program-report/${idPRintReporte}`,
										name: 'reporte-estudiante-programas',
										nameView: 'Estudiantes',
									},
								}}
								disableColumnFilter
								disableColumnSelector
								disableDensitySelector
							/>
						</Box>
					</Fade>
				</Modal>
			</div>
		</MainLayout>
	);
};
