/* eslint-disable react/prop-types */
import {
	Alert,
	Box,
	Button,
	Card,
	CardContent,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {updateRequest} from '../../api/Requests';
import DeleteIcon from '@mui/icons-material/Delete';

import {AutoCompleteSimple} from '../../components/AutoCompleteSimple';
import {useState} from 'react';

const validationFields = Yup.object({
	name: Yup.string().required('El nombre del programa es requerido.'),
	date_init: Yup.string().required('Este campo es requerido.'),
	date_end: Yup.string().required('Este campo es requerido.'),
});

export default function Edit({change, setChange, setState, initialValues}) {
	const [activo, setActivo] = useState(initialValues.active);
	const [sede, setSede] = useState(
		initialValues.sede.charAt(0).toUpperCase() + initialValues.sede.substring(1)
	);
	const [idArea, setIdArea] = useState(initialValues.id_area.id);

	const [serverError, setServerError] = useState('');

	const handleChange = (event) => {
		initialValues.active = event.target.value;
		setActivo(event.target.value);
	};

	const handleChangeSede = (event) => {
		initialValues.sede = event.target.value;
		setSede(event.target.value);
	};

	const onSubmit = async (values) => {
		values.id_area = idArea;
		const response = await updateRequest(initialValues.id, '/program', values);
		if (response.status) {
			setChange(!change);
			setState(false);
		} else setServerError(response.message);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '55px'}}>
			<form onSubmit={formik.handleSubmit}>
				<Card
					sx={{
						maxHeight: '80vh',
						overflowY: 'auto',
						boxShadow: 'none',
						marginBottom: '50px',
						display: 'flex',
					}}>
					<CardContent sx={{maxHeight: '100vh'}} style={{marginBotton: '250px'}}>
						<Typography variant='h3'>Editar Programas</Typography>
						<div style={{paddingTop: '35px'}}>
							<Box
								sx={{
									'& .MuiTextField-root': {mb: 3},
								}}
								noValidate
								autoComplete='off'>
								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={6}>
										<AutoCompleteSimple
											url={'api/v1/area'}
											setValues={setIdArea}
											campoIterar={'name'}
											campoDisable={'active'}
											labelText='Seleccionar Area'
											valuesEdit={initialValues.id_area}
										/>
									</Grid>
									<Grid item xs={12} sm={6}>
										<FormControl fullWidth>
											<InputLabel id='demo-sede'>Sede</InputLabel>
											<Select
												labelId='sede'
												id='sede'
												label='Sede'
												value={sede}
												onChange={handleChangeSede}
												onBlur={formik.handleBlur}>
												<MenuItem value={'Sucre'}>Sucre</MenuItem>
												<MenuItem value={'La Paz'}>La Paz</MenuItem>
												<MenuItem value={'Santa Cruz'}>Santa Cruz</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>
								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Nombre'
											fullWidth
											name='name'
											variant='outlined'
											value={formik.values.name}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.name && Boolean(formik.errors.name)
											}
											helperText={formik.touched.name && formik.errors.name}
										/>
									</Grid>
									<Grid item xs={12} sm={6}>
										<TextField
											fullWidth
											label='Versión del programa'
											name='version'
											variant='outlined'
											value={formik.values.version}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.version &&
												Boolean(formik.errors.version)
											}
											helperText={
												formik.touched.version && formik.errors.version
											}
										/>
									</Grid>
								</Grid>
								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Fecha de Inicio'
											name='date_init'
											fullWidth
											type='date'
											variant='outlined'
											InputLabelProps={{
												shrink: true,
												placeholder: 'Selecciona una fecha',
											}}
											value={formik.values.date_init}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.date_init &&
												Boolean(formik.errors.date_init)
											}
											helperText={
												formik.touched.date_init && formik.errors.date_init
											}
										/>
									</Grid>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Fecha de Finalización'
											name='date_end'
											fullWidth
											type='date'
											variant='outlined'
											InputLabelProps={{
												shrink: true,
												placeholder: 'Selecciona una fecha',
											}}
											value={formik.values.date_end}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.date_end &&
												Boolean(formik.errors.date_end)
											}
											helperText={
												formik.touched.date_end && formik.errors.date_end
											}
										/>
									</Grid>
								</Grid>

								<Grid item xs={12} sm={6} sx={{marginBottom: '10px'}}>
									<TextField
										fullWidth
										label='Número de Resolución'
										name='resolution_number'
										variant='outlined'
										value={formik.values.resolution_number}
										onChange={formik.handleChange}
										onBlur={formik.handleBlur}
										error={
											formik.touched.resolution_number &&
											Boolean(formik.errors.resolution_number)
										}
										helperText={
											formik.touched.resolution_number &&
											formik.errors.resolution_number
										}
									/>
								</Grid>

								<FormControl fullWidth>
									<InputLabel id='demo-simple-select-label'>Activo</InputLabel>
									<Select
										labelId='demo-simple-select-label'
										id='demo-simple-select'
										label='Activo'
										value={activo}
										onChange={handleChange}
										onBlur={formik.handleBlur}>
										<MenuItem value={1}>Si</MenuItem>
										<MenuItem value={0}>No</MenuItem>
									</Select>
								</FormControl>

								{serverError && (
									<Alert
										sx={{
											marginY: '20px',
										}}
										severity='error'>
										{serverError}
									</Alert>
								)}
							</Box>
						</div>
					</CardContent>
				</Card>
				<Box
					sx={{
						height: '60px',
						position: 'absolute',
						marginLeft: '18px',
						bottom: 0,
					}}>
					<Button color='primary' variant='contained' type='submit' endIcon={<AddIcon />}>
						Agregar
					</Button>
					<Button
						color='error'
						variant='text'
						onClick={() => {
							setState(false);
						}}
						endIcon={<DeleteIcon />}>
						Cancelar
					</Button>
				</Box>
			</form>
		</div>
	);
}
