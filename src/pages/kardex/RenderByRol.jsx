import {useAuthStore} from '../../store';
import {ListKardex} from './ListKardex';
import {ListKardexOther} from './ListKardexOther';

export const RenderKardexByRol = () => {
	const userAuth = useAuthStore.getState().user;
	if (userAuth.rol === 4) {
		return <ListKardex />;
	} else {
		return <ListKardexOther />;
	}
};
