import {Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';

export const columnsRender = (handleEdit, handleDelete, dataPermission) => {
	return [
		{
			field: 'matricula_name',
			headerName: 'Gestión',
			description: 'Gestión',
			sortable: false,
			width: 150,
		},
		{
			field: 'created',
			headerName: 'Fecha de Creación',
			description: 'Fecha de Creación',
			sortable: false,
			width: 250,
			valueGetter: (params) => `${params.row.created || ''} `,
		},

		{
			field: 'active',
			headerName: 'Habilitado',
			width: 250,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},

		{
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					<IconButton
						disabled={dataPermission?.ok_update ? false : true}
						aria-label='add'
						color='secondary'
						onClick={() => handleEdit(params.id)}>
						<CreateOutlinedIcon />
					</IconButton>
					<IconButton
						disabled={dataPermission?.ok_delete ? false : true}
						aria-label='add'
						color='error'
						onClick={() => handleDelete(params.id)}>
						<DeleteIcon />
					</IconButton>
				</div>
			),
		},
	];
};
