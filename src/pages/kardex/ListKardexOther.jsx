import {
	Button,
	ButtonGroup,
	Card,
	Chip,
	Grid,
	IconButton,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	TextField,
	Typography,
	useMediaQuery,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import {MainLayout} from '../../layouts/MainLayout';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import MainCard from '../../components/MainCard';

import {getPDF, getRequests} from '../../api/Requests';
import {useState} from 'react';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useTheme} from '@emotion/react';

export const ListKardexOther = () => {
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));
	const [data, setData] = useState([]);
	const [filterNumberCi, setFilterNumberCi] = useState();
	// const columns = columnsRender(null, null, dataPermission);

	const submitData = async () => {
		const estudiante = document.getElementById('estudianteFiltro').value;
		setFilterNumberCi(estudiante);
		const datos = await getRequests(`api/v1/kardex/other/${estudiante}`);
		setData(datos.data.data);
	};

	const printReport = async () => {
		await getPDF(`api/v1/kardex/reporte/${filterNumberCi}`, 'Matricula');
	};
	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<MainCard sx={{marginBottom: '20px'}}>
					<div style={{minHeight: 100, width: '100%'}}>
						<Card
							sx={{
								padding: '20px',
								boxShadow: 'none',
								borderRadius: `${10}px`,
							}}>
							<Grid container spacing={0} alignItems='center'>
								<Grid item lg={8} md={6} xs={12} sm={8} sx={{marginBottom: '10px'}}>
									<Typography variant='h2'>Kardex</Typography>
									<Typography variant='subtitle2' mt={1}>
										Kardex de los estudiantes
									</Typography>
								</Grid>
								<Grid item lg={4} md={6} xs={12} sm={4} sx={{marginBottom: '10px'}}>
									<Grid
										spacing={0}
										container
										sx={{
											display: 'flex',
											justifyContent: matchXS ? 'start' : 'end',
											alignItems: 'center',
										}}>
										<ButtonGroup aria-label='outlined primary button group'>
											<TextField
												id='estudianteFiltro'
												label='Buscar Estudiante'
												name='estudiante'
												variant='outlined'
												size='small'
												fullWidth
											/>
											<Button
												color='secondary'
												variant=''
												type='submit'
												sx={{marginBottom: '10px'}}
												onClick={submitData}>
												<SearchIcon color='secondary' />
											</Button>
										</ButtonGroup>
									</Grid>
								</Grid>
							</Grid>
						</Card>
					</div>
				</MainCard>
			</div>

			<Grid container spacing={2}>
				<Grid item xs={12} sm={12}>
					<div className='animate__animated animate__fadeIn'>
						{data?.kardex?.length > 0 ? (
							<>
								<MainCard>
									<div
										style={{
											minHeight: 120,
											width: '100%',
											borderStyle: 'none',
											paddingTop: '10px',
										}}>
										<Grid
											container
											spacing={0}
											alignItems='center'
											justifyContent='center'>
											<Grid item lg={10} md={8} xs={3} sm={5}>
												<Typography
													variant='h5'
													sx={{marginLeft: '20px', marginRight: '20px'}}>
													Datos del Estudiante
												</Typography>
											</Grid>
											<Grid
												item
												lg={2}
												md={4}
												xs={7}
												sm={7}
												display='flex'
												alignItems='center'
												justifyContent='end'>
												<IconButton
													color='secondary'
													aria-label='add an print'
													onClick={printReport}
													sx={{
														marginRight: '15px',
													}}>
													<LocalPrintshopOutlinedIcon />
												</IconButton>
											</Grid>
										</Grid>
										<Grid
											container
											spacing={0}
											sx={{marginLeft: '10px', marginTop: '10px'}}>
											<Grid
												item
												lg={4}
												md={4}
												xs={12}
												sm={5}
												display={'flex'}
												alignItems='center'
												justifyContent='center'>
												<Typography variant='h5'>
													Nombre del Estudiante:
												</Typography>
												<Typography variant='body2'>
													{data?.student?.person_full_name}
												</Typography>
											</Grid>
											<Grid
												item
												lg={4}
												md={4}
												xs={12}
												sm={5}
												display={'flex'}
												alignItems='center'
												justifyContent='center'>
												<Typography variant='h5'>
													Doc. de Identificación:
												</Typography>
												<Typography variant='body2'>
													{data?.student?.person_ci_number}
												</Typography>
											</Grid>
											<Grid
												item
												lg={4}
												md={4}
												xs={12}
												sm={2}
												display={'flex'}
												alignItems='center'
												justifyContent='center'>
												<Typography variant='h5'>
													Record Académico:
												</Typography>
												<Typography variant='body2'>
													{data?.promedio}
												</Typography>
											</Grid>
										</Grid>
									</div>
								</MainCard>
								<MainCard sx={{marginTop: '20px'}}>
									{/* KARDEX */}
									{}
									<div
										style={{
											height: 500,
											width: '100%',
											borderStyle: 'none',
											boxShadow: 'none',
										}}>
										{data?.status === false ? (
											<></>
										) : (
											<>
												{data !== undefined ? (
													<TableContainer
														component={Paper}
														sx={{borderStyle: 'none'}}>
														<Table
															sx={{minWidth: 650}}
															aria-label='simple table'>
															<TableHead>
																<TableRow>
																	<TableCell align='center'>
																		Gestión
																	</TableCell>
																	<TableCell align='center'>
																		Nombre del Programa
																	</TableCell>
																	<TableCell align='center'>
																		Nombre de la Asignatura
																	</TableCell>
																	<TableCell align='center'>
																		Calificación Final
																	</TableCell>
																	<TableCell align='center'>
																		Estado
																	</TableCell>
																</TableRow>
															</TableHead>
															<TableBody>
																{data.kardex.map((row) => (
																	<TableRow
																		key={row.id}
																		sx={{
																			'&:last-child td, &:last-child th':
																				{border: 0},
																		}}>
																		<TableCell
																			align='center'
																			component='th'
																			scope='row'>
																			{row.matricula_name}
																		</TableCell>
																		<TableCell align='center'>
																			{row.program_name}
																		</TableCell>
																		<TableCell align='center'>
																			{row.subject_name}
																		</TableCell>
																		<TableCell align='center'>
																			{row.qualification}
																		</TableCell>
																		<TableCell align='center'>
																			<Chip
																				label={
																					row.approved
																						? 'Aprobado'
																						: 'Reprobado'
																				}
																				sx={{
																					backgroundColor:
																						row.approved
																							? '#b9f6ca60'
																							: '#fbe9e7',
																					color: row.approved
																						? '#00c853'
																						: '#d84315',
																				}}
																				size='small'
																			/>
																		</TableCell>
																	</TableRow>
																))}
															</TableBody>
														</Table>
													</TableContainer>
												) : (
													<SkeletonLoading />
												)}
											</>
										)}
									</div>
								</MainCard>
							</>
						) : (
							<Typography display={'center'} justifyContent={'center'}>
								Seleccione al estudiante porfavor
							</Typography>
						)}
					</div>
				</Grid>
			</Grid>
		</MainLayout>
	);
};
