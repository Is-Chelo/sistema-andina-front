import {Grid} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';

import {SkeletonLoading} from '../../components/SkeletonLoading';
import {CustomButtons} from '../../components/CustomButtons';
// import AddIcon from '@mui/icons-material/Add';

export const ListPermisos = () => {
	const data = useFetch('api/v1/module');

	const columns = columnsRender();

	return (
		<MainLayout>
			<CardCrud
				title='Modulos'
				parrafo='Listado de todos los módulos del sistema, recuerde que no puede hacer ninguna operación en esta sección.'
				category={'Permisos'}
			/>

			<Grid container spacing={2}>
				<Grid item xs={12} sm={12}>
					<MainCard>
						<div style={{height: 600, width: '100%', borderStyle: 'none'}}>
							{data?.status === false ? (
								<></>
							) : (
								<>
									{data !== undefined ? (
										<DataGrid
											rows={data}
											columns={columns}
											pageSize={5}
											rowsPerPageOptions={[5]}
											slots={{
												toolbar: CustomButtons,
											}}
											checkboxSelection
											disableRowSelectionOnClick
											unstable_cellSelection
											experimentalFeatures={{clipboardPaste: true}}
											unstable_ignoreValueFormatterDuringExport
											// FIlters
											slotProps={{
												toolbar: {
													showQuickFilter: true,
													quickFilterProps: {debounceMs: 500},
													url: '/api/v1/module/reporte',
													name:'reporte-permisos'

												},
											}}
											disableColumnFilter
											disableColumnSelector
											disableDensitySelector
										/>
									) : (
										<SkeletonLoading />
									)}
								</>
							)}
						</div>
					</MainCard>
				</Grid>
			</Grid>
		</MainLayout>
	);
};
