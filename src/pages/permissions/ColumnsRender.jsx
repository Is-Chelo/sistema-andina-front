export const columnsRender = () => {
	return [
		{field: 'id', headerName: 'ID', width: 150},
		{
			field: 'name',
			headerName: 'Nombre del Módulo',
			description: 'Nombre del Módulo',
			sortable: false,
			width: 250,
			valueGetter: (params) => `${params.row.name || ''} `,
		},
		{
			field: 'path_front',
			headerName: 'Ruta del Módulo',
			description: 'Url del módulo para el front',
			sortable: false,
			width: 250,
			valueGetter: (params) => `${params.row.path_front || ''} `,
		},
		{
			field: 'type',
			headerName: 'Sección del Menú',
			description: 'Sección al que pertenece el módulo',
			sortable: false,
			width: 250,
			valueGetter: (params) => `${params.row.type || ''} `,
		},
		{
			field: 'created',
			headerName: 'Fecha de Creación',
			description: 'Fecha de Creación',
			sortable: false,
			width: 250,
			valueGetter: (params) => `${params.row.created || ''} `,
		},
	];
};
