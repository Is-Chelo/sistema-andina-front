import {Backdrop, Box, Fade, Grid, Modal} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useEffect, useState} from 'react';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {deleteRequest, getRequest} from '../../api/Requests';
import Edit from './Edit';
import Swal from 'sweetalert2';
import {AddAsignatura} from './AddAsignatura';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import {CustomButtons} from '../../components/CustomButtons';
import {columnsRenderEstudiante} from './columnsRenderEstudiante';
import {CustomButtons3} from '../../components/CustomButtons3';

export const ListAsignaturas = () => {
	// MODAL VER
	const [open, setOpen] = useState(false);
	const handleClose = () => setOpen(false);
	const [estudiantesData, setEstudiantessData] = useState([]);
	const [idPRintReporte, setIdPRintReporte] = useState();

	const [change, setChange] = useState(false);
	const [changePermissions, setChangePermissions] = useState(false);
	const [edit, setEdit] = useState(false);
	const [initialValues, setInitialValues] = useState({});
	const data = useFetch('api/v1/subject', change);

	// PERMISOS
	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/subject');
				setChange(!change);
			}
		});
	};

	const handleEdit = async (id) => {
		setEdit(false);
		const datos = await getRequest(`api/v1/subject/${id}`);
		setEdit(true);
		setInitialValues({
			name: datos.name,
			descripcion: datos.descripcion,
			active: datos.active ? 1 : 0,
			id: id,
			academics_hours: datos.academics_hours,
			id_program: {
				id: datos.program_id,
				name: datos.program_name,
			},
			id_teacher: {
				id: datos.teacher_id,
				person_full_name: datos.teacher_full_name,
			},
		});
	};

	const handlerShow = async (id) => {
		const datos = await getRequest(`api/v1/student/student-by-subject/${id}`);
		setEstudiantessData(datos);
		setIdPRintReporte(id);
	};

	const columns = columnsRender(handleEdit, handleDelete, dataPermission, handlerShow, setOpen);
	const columnsEstudiante = columnsRenderEstudiante();
	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud
					title='Asignaturas'
					parrafo='Listado de Asignaturas.'
					category={'Gestión académica'}
				/>
			</div>

			<Grid container spacing={2}>
				<Grid item xs={12} md={7} sm={12} lg={dataPermission?.ok_insert || edit ? 8 : 12}>
					<div className='animate__animated animate__fadeIn'>
						<MainCard>
							<div style={{height: 537, width: '100%', borderStyle: 'none'}}>
								{data?.status === false ? (
									<></>
								) : (
									<>
										{data !== undefined ? (
											<DataGrid
												rows={data}
												columns={columns}
												pageSize={1}
												rowsPerPageOptions={[5]}
												slots={{
													toolbar: CustomButtons,
												}}
												checkboxSelection
												disableRowSelectionOnClick
												unstable_cellSelection
												experimentalFeatures={{clipboardPaste: true}}
												unstable_ignoreValueFormatterDuringExport
												// FIlters
												slotProps={{
													toolbar: {
														showQuickFilter: true,
														quickFilterProps: {debounceMs: 500},
														url: '/api/v1/subject/reporte',
														name: 'reporte-asignaturas',
													},
												}}
												disableColumnFilter
												disableColumnSelector
												disableDensitySelector
											/>
										) : (
											<SkeletonLoading />
										)}
									</>
								)}
							</div>
						</MainCard>
					</div>
				</Grid>
				<Grid item xs={12} sm={12} md={5} lg={4}>
					{dataPermission?.ok_insert && dataPermission?.ok_update ? (
						<div className='animate__animated animate__fadeIn'>
							<MainCard>
								{edit ? (
									<Edit
										change={change}
										setChange={setChange}
										initialValues={initialValues}
										setEdit={setEdit}
									/>
								) : (
									<AddAsignatura change={change} setChange={setChange} />
								)}
							</MainCard>
						</div>
					) : dataPermission?.ok_insert && dataPermission?.ok_update === false ? (
						<div className='animate__animated animate__fadeInRight'>
							<MainCard>
								<AddAsignatura change={change} setChange={setChange} />
							</MainCard>
						</div>
					) : dataPermission?.ok_update && dataPermission?.ok_insert === false ? (
						edit && (
							<div className='animate__animated animate__fadeInRight'>
								<MainCard sm={4}>
									{edit ? (
										<Edit
											change={change}
											setChange={setChange}
											initialValues={initialValues}
											setEdit={setEdit}
										/>
									) : (
										<> Recuerde que su rol solo tiene permisos para editar</>
									)}
								</MainCard>
							</div>
						)
					) : (
						<></>
					)}
				</Grid>
			</Grid>

			<div>
				<Modal
					aria-labelledby='transition-modal-title'
					aria-describedby='transition-modal-description'
					open={open}
					onClose={handleClose}
					closeAfterTransition
					slots={{backdrop: Backdrop}}
					slotProps={{
						backdrop: {
							timeout: 500,
						},
					}}>
					<Fade in={open}>
						<Box
							sx={{
								position: 'absolute',
								top: '50%',
								left: '50%',
								transform: 'translate(-50%, -50%)',
								width: '30vw',
								height: '50vh',
								bgcolor: 'background.paper',
								boxShadow: 24,
								borderRadius: '10px',
								p: 4,
							}}>
							<DataGrid
								rows={estudiantesData}
								columns={columnsEstudiante}
								pageSize={5}
								rowsPerPageOptions={[5]}
								slots={{
									toolbar: CustomButtons3,
								}}
								// slots={{toolbar: GridToolbar}}
								checkboxSelection
								disableRowSelectionOnClick
								unstable_cellSelection
								experimentalFeatures={{clipboardPaste: true}}
								unstable_ignoreValueFormatterDuringExport
								// FIlters
								slotProps={{
									toolbar: {
										showQuickFilter: true,
										quickFilterProps: {debounceMs: 500},
										url: `api/v1/student/student-by-subject-report/${idPRintReporte}`,
										name: 'reporte-estudiante-asignatura',
										nameView: 'Estudiantes',
									},
								}}
								disableColumnFilter
								disableColumnSelector
								disableDensitySelector
							/>
						</Box>
					</Fade>
				</Modal>
			</div>
		</MainLayout>
	);
};
