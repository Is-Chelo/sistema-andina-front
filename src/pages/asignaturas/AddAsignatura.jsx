import {
	Box,
	Button,
	Card,
	CardContent,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
	Alert,
	CircularProgress,
	useTheme,
	useMediaQuery,
} from '@mui/material';
import PropTypes from 'prop-types';
import AddIcon from '@mui/icons-material/Add';
import React, {useEffect, useState} from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {postRequest} from '../../api/Requests';
import DeleteIcon from '@mui/icons-material/Delete';
import {AutoCompleteSimple} from '../../components/AutoCompleteSimple';

const initialValues = {
	name: '',
	academics_hours: '',
	active: 1,
};

const validationFields = Yup.object({
	name: Yup.string().required('El nombre de la asignatura es requerido.'),
});

export const AddAsignatura = ({change, setChange}) => {
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));

	const [activo, setActivo] = React.useState(1);
	const [serverError, setServerError] = React.useState('');
	const [limpiar, setLimpiar] = useState(false);
	const [loading, setLoading] = useState(false);

	// Relations
	const [idProgram, setIdProgram] = useState('');
	const [idTeacher, setIdTeacher] = useState('');

	useEffect(() => {
		initialValues.id_program = idProgram;
		initialValues.id_teacher = idTeacher;
	}, [idProgram, idTeacher]);

	const handleChange = (event) => {
		setActivo(event.target.value);
	};

	const onSubmit = async (values) => {
		values.active = activo;
		values.id_program = idProgram;
		values.id_teacher = idTeacher;
		setLoading(true);
		setTimeout(async () => {
			const response = await postRequest('/subject', values);
			if (response.status) {
				setChange(!change);
				setLoading(false);
				formik.resetForm();
				setServerError('');
			} else {
				setServerError(response.message);
				setChange(!change);
				setLoading(false);
			}
		}, 1000);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '35px'}}>
			{loading ? (
				<Grid justifyContent={'center'} alignContent={'center'} display={'flex'}>
					<CircularProgress />
				</Grid>
			) : (
				<form onSubmit={formik.handleSubmit}>
					<Card
						sx={{
							maxHeight: '80vh',
							overflowY: 'auto',
							boxShadow: 'none',
							marginBottom: '30px',
						}}>
						<CardContent
							sx={{maxHeight: '100vh', width: '100%'}}
							style={{marginBotton: '250px'}}>
							<Typography variant='h3' sx={{display: 'inline-flex'}}>
								Agregar
								<Box
									sx={{
										color: theme.palette.secondary.dark,
										marginLeft: '5px',
									}}>
									Asignatura
								</Box>
							</Typography>
							<div style={{paddingTop: '35px'}}>
								<Box
									sx={{
										'& .MuiTextField-root': {mb: 3},
									}}
									noValidate
									autoComplete='off'>
									<Grid container spacing={2}>
										<Grid item xs={12} sm={12} sx={{mb: 2}}>
											<AutoCompleteSimple
												url={'api/v1/program'}
												setValues={setIdProgram}
												campoIterar={'name'}
												campoDisable={'active'}
												labelText='Seleccionar Programa'
												limpiar={limpiar}
											/>
											<TextField
												label='Nombre'
												fullWidth
												name='name'
												variant='outlined'
												value={formik.values.name}
												onChange={formik.handleChange}
												onBlur={formik.handleBlur}
												error={
													formik.touched.name &&
													Boolean(formik.errors.name)
												}
												helperText={
													formik.touched.name && formik.errors.name
												}
											/>
										</Grid>
									</Grid>
									<Grid container spacing={2} sx={{mb: 2}}>
										<Grid item xs={12} sm={12} lg={6}>
											<TextField
												label='Horas Académicas'
												fullWidth
												type='number'
												name='academics_hours'
												variant='outlined'
												value={formik.values.academics_hours}
												onChange={formik.handleChange}
												onBlur={formik.handleBlur}
												error={
													formik.touched.academics_hours &&
													Boolean(formik.errors.academics_hours)
												}
												helperText={
													formik.touched.academics_hours &&
													formik.errors.academics_hours
												}
											/>
										</Grid>
										<Grid item xs={12} sm={12} lg={6}>
											<AutoCompleteSimple
												url={
													'api/v1/teacher/teacher-with-contract?active=true'
												}
												setValues={setIdTeacher}
												campoIterar={'person_full_name'}
												campoDisable={'active'}
												labelText='Seleccionar Docente'
												limpiar={limpiar}
											/>
										</Grid>
									</Grid>

									<FormControl fullWidth>
										<InputLabel id='demo-simple-select-label'>
											Activo
										</InputLabel>
										<Select
											labelId='demo-simple-select-label'
											id='demo-simple-select'
											label='Activo'
											value={activo}
											onChange={handleChange}
											onBlur={formik.handleBlur}>
											<MenuItem value={1}>Si</MenuItem>
											<MenuItem value={0}>No</MenuItem>
										</Select>
									</FormControl>

									{serverError && (
										<Alert
											sx={{
												marginY: '20px',
											}}
											severity='error'>
											{serverError}
										</Alert>
									)}
								</Box>
							</div>
						</CardContent>
					</Card>
					<Grid item display={'flex'} sx={{marginLeft: matchXS ? '0' : '20px'}}>
						<Button
							color='secondary'
							variant='contained'
							type='submit'
							endIcon={<AddIcon />}>
							Agregar
						</Button>
						<Button
							sx={{ml: 2}}
							color='error'
							onClick={() => {
								setLimpiar(!limpiar);
								formik.resetForm();
							}}
							endIcon={<DeleteIcon />}>
							Cancelar
						</Button>
					</Grid>
				</form>
			)}
		</div>
	);
};

AddAsignatura.propTypes = {
	change: PropTypes.bool,
	setChange: PropTypes.func,
};
