import {Button, Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';

export const columnsRender = (
	handleEdit,
	handleDelete,
	dataPermission,
	handlerShow,
	setShowModal
) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 100},
		{
			field: 'name',
			headerName: 'Nombre asignatura',
			description: 'Nombre de la Asignatura',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${params.row.name || ''} `,
		},
		{
			field: 'academics_hours',
			headerName: 'Horas Académicas',
			description: 'Horas Académicas',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${params.row.academics_hours || ''} `,
		},
		{
			field: 'teacher_full_name',
			headerName: 'Docente',
			description: 'Nombre completo del Docente',
			width: 150,
		},
		{
			field: 'program_name',
			headerName: 'Programa',
			description: 'Nombre del Programa',
			width: 150,
		},
		{
			field: 'total_students',
			headerName: 'Estudiantes',
			width: 150,
			renderCell: (params) => (
				<Button
					onClick={() => {
						handlerShow(params.id);
						setShowModal(true);
					}}
					startIcon={<RemoveRedEyeIcon />}>
					ver
				</Button>
			),
		},
		{
			field: 'active',
			headerName: 'Habilitado',
			width: 100,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},

		accionOperations,
	];
};
