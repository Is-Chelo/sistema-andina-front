import {useAuthStore} from '../../store';
import {ListCalificaciones} from './ListCalificaciones';
// import {ListCalificacionesDocentes} from './ListCalificacionesDocentes';
import {ListCalificacionesDocentes2} from './ListCalificacionesDocentes2';
import {ListCalificacionesEstudiantes} from './ListCalificacionesEstudiantes';

export const RenderByRol = () => {
	const userAuth = useAuthStore.getState().user;

	if (userAuth.rol === 4) {
		return <ListCalificacionesEstudiantes />;
	}
	if (userAuth.rol === 2) {
		return <ListCalificacionesDocentes2 />;
	}

	return <ListCalificaciones />;
};
