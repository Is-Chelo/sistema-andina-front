import {useEffect, useState} from 'react';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import {
	Box,
	Card,
	CardContent,
	Collapse,
	Grid,
	IconButton,
	List,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Paper,
	Table,
	TableCell,
	TableContainer,
	TableRow,
	Typography,
} from '@mui/material';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import MainCard from '../../components/MainCard';
import {ExpandLess, ExpandMore} from '@mui/icons-material';
import SearchIcon from '@mui/icons-material/Search';
import {getPDF, getRequest} from '../../api/Requests';
import {DataGrid} from '@mui/x-data-grid';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import {useFetch} from '../../hooks/useFetch';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import {SelectDepend} from '../../components/SelectDepend';
import {columnsRenderEstudiante} from './ColumnsRenderEstudiante';
import moment from 'moment';

const initialValues = {
	id_program: '',
	id_matricula: '',
};

const validationFields = Yup.object({});

export const ListCalificacionesEstudiantes = () => {
	const [open, setOpen] = useState(true);
	const handleClick = () => {
		setOpen(!open);
	};
	// const [change, setChange] = useState(false);
	const [changePermissions, setChangePermissions] = useState(false);

	const idModuleMenu = useAuthStore.getState().id_menu;

	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);

	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	// Relations
	const [idProgram, setIdProgram] = useState(0);
	const [idMatricula, isetIdMatricula] = useState(0);
	// eslint-disable-next-line no-unused-vars
	const [allNotes, setAllNotes] = useState([]);
	const [dataCalificaciones, setDataCalificaciones] = useState({grades: []});

	useEffect(() => {
		if ((idProgram !== '' || idProgram !== 0) && (idMatricula !== '' || idMatricula !== 0)) {
			getCalificaciones();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idProgram, idMatricula]);

	const getCalificaciones = async () => {
		const datos = await getRequest(
			// `grades-student/students-by-subject?id_program=&id_matricula=`
			`api/v1/grades-student/grades-by-student-by-matricula?id_program=${idProgram}&id_matricula=${idMatricula}`
		);

		setDataCalificaciones(datos);
		setAllNotes(datos);
	};
	const printGrades = async () => {
		await getPDF(
			`/api/v1/grades-student/grades-by-student-by-matricula-reporte?id_program=${idProgram}&id_matricula=${idMatricula}`,
			'Calificaciones'
		);
	};
	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		// onSubmit: onSubmit,
	});

	const columns = columnsRenderEstudiante();

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud title='Calificaciones' parrafo='' category={'Inicio'} />
			</div>
			<MainCard>
				<List>
					<ListItemButton onClick={handleClick}>
						<ListItemIcon>
							<SearchIcon />
						</ListItemIcon>
						<ListItemText primary='Filtros para Programas y asignaturas.' />
						{open ? <ExpandLess /> : <ExpandMore />}
					</ListItemButton>
					<Collapse in={open} timeout='auto' unmountOnExit>
						<List component='div' disablePadding>
							<form onSubmit={formik.handleSubmit}>
								<Card
									sx={{
										boxShadow: 'none',
									}}>
									<CardContent
										sx={{maxHeight: '100vh', width: '100%', px: 4, py: 0}}>
										<div style={{paddingTop: '20px'}}>
											<Box
												sx={{
													'& .MuiTextField-root': {mb: 3},
												}}
												noValidate
												autoComplete='off'>
												{/* SELECT WITH DEPENDENCIES */}
												<SelectDepend
													setId1={setIdProgram}
													setId2={isetIdMatricula}
													name={'Programa'}
													name2={'Matricula'}
													valor1={'program_id'}
													valor2={'program_name'}
													valor3={'matricula_id'}
													valor4={'matricula_name'}
													url={
														'api/v1/grades-student/programs-by-student'
													}
													urlDependency={
														'api/v1/grades-student/matricula-by-student'
													}
												/>
											</Box>
										</div>
									</CardContent>
								</Card>
							</form>
						</List>
					</Collapse>
				</List>
			</MainCard>
			{dataPermission?.ok_select && (
				<div style={{marginTop: '20px'}}>
					{dataCalificaciones?.grades.length > 0 ? (
						<Grid container spacing={2}>
							<Grid item xs={12} sm={12} md={6} lg={6}>
								<MainCard>
									<div
										style={{
											height: 80,
											width: '100%',
											borderStyle: 'none',
											paddingTop: '20px',
										}}>
										<Grid
											container
											spacing={0}
											alignItems='center'
											justifyContent='center'>
											<Grid item lg={10} md={8} xs={3} sm={5}>
												<Typography variant='h5' sx={{marginLeft: '20px'}}>
													Calificaciones del Estudiante
												</Typography>
											</Grid>
											<Grid
												item
												lg={2}
												md={4}
												xs={7}
												sm={7}
												display='flex'
												alignItems='center'
												justifyContent='center'>
												<IconButton
													color=''
													aria-label='add an print'
													sx={{
														marginRight: '15px',
													}}
													onClick={printGrades}>
													<LocalPrintshopOutlinedIcon />
												</IconButton>
											</Grid>
										</Grid>
									</div>
									<div style={{height: 400, width: '100%', borderStyle: 'none'}}>
										{dataCalificaciones?.status === false ? (
											<></>
										) : (
											<>
												{dataCalificaciones !== undefined ? (
													<DataGrid
														rows={dataCalificaciones.grades}
														columns={columns}
														pageSize={5}
														rowsPerPageOptions={[5]}
													/>
												) : (
													<SkeletonLoading />
												)}
											</>
										)}
									</div>
								</MainCard>
							</Grid>
							<Grid item xs={12} sm={12} md={6} lg={6}>
								{dataCalificaciones !== undefined && (
									<MainCard>
										<div
											style={{
												height: 80,
												width: '100%',
												borderStyle: 'none',
												paddingTop: '20px',
											}}>
											<Grid
												container
												spacing={0}
												alignItems='center'
												justifyContent='center'>
												<Grid item lg={10} md={8} xs={3} sm={5}>
													<Typography
														variant='h5'
														sx={{marginLeft: '20px'}}>
														Matrícula del Estudiante
													</Typography>
												</Grid>
											</Grid>
										</div>
										<div
											style={{
												height: 400,
												width: '100%',
												borderStyle: 'none',
											}}>
											<Grid container sx={{paddingX: '20px'}}>
												<TableContainer component={Paper}>
													<Table
														sx={{minWidth: 650}}
														aria-label='simple table'>
														<TableRow>
															<TableCell
																sx={{
																	width: '100%',
																	display: 'flex',
																}}>
																<Typography variant='h5'>
																	Nombre del Estudiante:{' '}
																</Typography>
																<Typography variant='body2' ml={1}>
																	{
																		dataCalificaciones
																			.dataStudent
																			.person_full_name
																	}
																</Typography>
															</TableCell>

															<TableCell
																sx={{
																	width: '100%',
																	display: 'flex',
																}}>
																<Typography variant='h5'>
																	Nro de Documento de Identidad:
																</Typography>
																<Typography variant='body2' ml={1}>
																	{
																		dataCalificaciones
																			.dataStudent.person_ci
																	}
																</Typography>
															</TableCell>
														</TableRow>
														<TableRow>
															<TableCell
																sx={{
																	width: '100%',
																	display: 'flex',
																}}>
																<Typography variant='h5'>
																	Nombre Programa:{' '}
																</Typography>
																<Typography variant='body2' ml={1}>
																	{
																		dataCalificaciones
																			.dataStudent
																			.program_name
																	}
																</Typography>
															</TableCell>
															<TableCell
																sx={{
																	width: '100%',
																	display: 'flex',
																}}>
																<Typography variant='h5'>
																	Matrícula:{' '}
																</Typography>
																<Typography variant='body2' ml={1}>
																	{
																		dataCalificaciones
																			.dataStudent
																			.matricula_name
																	}
																</Typography>
															</TableCell>
															<TableCell
																sx={{
																	width: '100%',
																	display: 'flex',
																}}>
																<Typography variant='h5'>
																	Fecha:{' '}
																</Typography>
																<Typography variant='body2' ml={1}>
																	{moment(
																		`${dataCalificaciones.dataStudent.matricula_fecha}`
																	).format('DD-MMMM-YYYY')}
																</Typography>
															</TableCell>
														</TableRow>
													</Table>
												</TableContainer>
											</Grid>
										</div>
									</MainCard>
								)}
							</Grid>
						</Grid>
					) : (
						<Typography
							variant='body2'
							display={'flex'}
							justifyContent={'center'}
							alignContent={'center'}>
							Debe seleccionar los filtros por favor
						</Typography>
					)}
				</div>
			)}
		</MainLayout>
	);
};
