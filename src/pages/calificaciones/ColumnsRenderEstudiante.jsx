import {Chip} from '@mui/material';

export const columnsRenderEstudiante = () => {
	return [
		{field: 'id', headerName: 'ID', width: 70},
		{
			field: 'subject_name',
			headerName: 'Nombre de la Asignatura',
			description: 'Nombre del la Asignatura',
			sortable: false,
			width: 220,
		},
		{
			field: 'approved',
			headerName: 'Estado',
			description: 'Aqui se refleja el estado del estudiante',
			width: 200,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.approved ? 'Aprobado' : 'Reprobado'}
						sx={{
							backgroundColor: params.row.approved ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.approved ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
		{
			field: 'qualification',
			headerName: 'Calificación',
			description: 'Aqui se refleja la calificación del estudiante',
			width: 200,
			// renderCell: (params) => {
			// 	return (
			// 		<input
			// 			autoComplete={'off'}
			// 			type='text' // usar type='text' para que el input acepte solo números del 0 al 9
			// 			name={`field_${params.row.id}`}
			// 			id={`field_${params.row.id}`}
			// 			onChange={(e) => {
			// 				handleBlur(e, setAllNotes, allNotes, params.row.id);
			// 			}}
			// 			placeholder={0}
			// 			defaultValue={`${params.row.qualification}`}
			// 			disabled={dataPermission?.ok_update ? false : true}
			// 			style={{
			// 				border: ' 1px solid #ccc',
			// 				minWidth: '30px',
			// 				maxWidth: '50%',
			// 				borderRadius: '6px',
			// 				padding: '5px',
			// 			}}
			// 		/>
			// 	);
			// },
		},
	];
};
