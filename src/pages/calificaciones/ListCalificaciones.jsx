import {useEffect, useState} from 'react';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import {
	Box,
	Card,
	CardContent,
	Collapse,
	Fab,
	Grid,
	IconButton,
	List,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Tooltip,
	Typography,
} from '@mui/material';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import MainCard from '../../components/MainCard';
import {ExpandLess, ExpandMore} from '@mui/icons-material';
import SearchIcon from '@mui/icons-material/Search';
import {getPDF, getRequest, updateAllRequest, updateRequest} from '../../api/Requests';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import {useFetch} from '../../hooks/useFetch';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import SaveAsIcon from '@mui/icons-material/SaveAs';
import {SelectDepend2} from '../../components/SelectDepend2';
import moment from 'moment';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

const initialValues = {
	id_program: '',
	id_subject: '',
};

const validationFields = Yup.object({
	name: Yup.string().required('El nombre de la asignatura es requerido.'),
});

export const ListCalificaciones = () => {
	const navigate = useNavigate();
	const [open, setOpen] = useState(true);
	const handleClick = () => {
		setOpen(!open);
	};
	const [change, setChange] = useState(false);
	const [changePermissions, setChangePermissions] = useState(false);

	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	// Relations
	const [idProgram, setIdProgram] = useState(0);
	const [idSubject, setIdSubject] = useState(0);
	const [idMatricula, setIdMatricula] = useState(0);
	const [allNotes, setAllNotes] = useState([]);
	const [dataCalificaciones, setDataCalificaciones] = useState([]);

	useEffect(() => {
		comprobarFechas();
		if (
			(idProgram !== '' || idProgram !== 0) &&
			(idSubject !== '' || idSubject !== 0) &&
			(idMatricula !== '' || idMatricula !== 0)
		) {
			getCalificaciones();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idProgram, idSubject, idMatricula, change]);

	const getCalificaciones = async () => {
		const datos = await getRequest(
			`api/v1/grades-student/students-by-subject?id_program=${idProgram}&id_subject=${idSubject}&id_matricula=${idMatricula}`
		);

		setDataCalificaciones(datos);
		setAllNotes(datos);
	};

	const comprobarFechas = async () => {
		const datos = await getRequest(`api/v1/delivery-qualification/active`);
		const dateNow = moment();
		const start_time = moment(datos.start_time);
		const close_time = moment(datos.close_time);
		if (!dateNow.isBetween(start_time, close_time)) {
			Swal.fire({
				icon: 'error',
				title: 'La asignación de notas no esta habilitado...',
				text: 'Regresé luego por favor!',
				customClass: {
					title: 'my-title',
				},
			});
			setTimeout(() => {
				navigate('/dashboard');
			}, 1000);
		}
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		// onSubmit: onSubmit,
	});
	const handleEdit = async (id) => {
		const values = document.querySelector(`#field_${id}`).value;
		const dataSend = {
			qualification: values,
		};
		const response = await updateRequest(id, '/grades-student/student-grade', dataSend);
		if (response.status) {
			setChange(!change);
		}
	};
	const saveTodasNotas = async () => {
		const response = await updateAllRequest('/grades-student/all-student-grade', {
			qualifications: allNotes,
		});
		if (response.status) {
			setChange(!change);
		}
	};
	const columns = columnsRender(handleEdit, null, dataPermission, setAllNotes, allNotes);

	const printGrades = async () => {
		await getPDF(
			`api/v1/grades-student/students-by-subject-reporte?id_program=${idProgram}&id_subject=${idSubject}&id_matricula=${idMatricula}`,
			'Calificaciones'
		);
	};

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud
					title='Calificaciones'
					parrafo='En este apartado asigna las calificaiones a sus estudiantes.'
					category={'Inicio'}
				/>
			</div>
			<MainCard>
				<List>
					<ListItemButton onClick={handleClick}>
						<ListItemIcon>
							<SearchIcon />
						</ListItemIcon>
						<ListItemText primary='Filtros para Programas y asignaturas.' />
						{open ? <ExpandLess /> : <ExpandMore />}
					</ListItemButton>
					<Collapse in={open} timeout='auto' unmountOnExit>
						<List component='div' disablePadding>
							<form onSubmit={formik.handleSubmit}>
								<Card
									sx={{
										boxShadow: 'none',
									}}>
									<CardContent
										sx={{maxHeight: '100vh', width: '100%', px: 4, py: 0}}>
										<div style={{paddingTop: '20px'}}>
											<Box
												sx={{
													'& .MuiTextField-root': {mb: 3},
												}}
												noValidate
												autoComplete='off'>
												{/* SELECT WITH DEPENDENCIES */}
												<SelectDepend2
													setId1={setIdProgram}
													setId2={setIdSubject}
													setId3={setIdMatricula}
													name={'Programa'}
													name2={'Asignatura'}
													valor1={'id'}
													valor2={'name'}
													valor3={'subject_id'}
													valor4={'subject_name'}
													url={
														'api/v1/grades-student/programs-all?active=true'
													}
													urlDependency={
														'api/v1/grades-student/subjects-by-program-other'
													}
												/>
											</Box>
										</div>
									</CardContent>
								</Card>
							</form>
						</List>
					</Collapse>
				</List>
			</MainCard>
			<div style={{marginTop: '20px'}}>
				{dataCalificaciones.length > 0 ? (
					<MainCard>
						<div
							style={{
								height: 80,
								width: '100%',
								borderStyle: 'none',
								paddingTop: '20px',
							}}>
							<Grid container spacing={0} alignItems='center' justifyContent='center'>
								<Grid item lg={10} md={8} xs={3} sm={5}></Grid>
								<Grid
									item
									lg={2}
									md={4}
									xs={7}
									sm={7}
									display='flex'
									alignItems='center'
									justifyContent='center'>
									<IconButton
										color='secondary'
										aria-label='add an print'
										sx={{
											marginRight: '15px',
										}}
										onClick={printGrades}>
										<LocalPrintshopOutlinedIcon />
									</IconButton>

									{dataPermission?.ok_update && (
										<Tooltip
											title={'Registrar todas las calificaciones'}
											placement='top'>
											<Fab
												size='small'
												onClick={saveTodasNotas}
												color='primary'
												aria-label='add'>
												<SaveAsIcon />
											</Fab>
										</Tooltip>
									)}
								</Grid>
							</Grid>
						</div>
						<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
							{dataCalificaciones?.status === false ? (
								<></>
							) : (
								<>
									{dataCalificaciones !== undefined ? (
										<DataGrid
											rows={dataCalificaciones}
											columns={columns}
											pageSize={5}
											rowsPerPageOptions={[5]}
										/>
									) : (
										<SkeletonLoading />
									)}
								</>
							)}
						</div>
					</MainCard>
				) : (
					<Typography
						variant='body2'
						display={'flex'}
						justifyContent={'center'}
						alignContent={'center'}>
						{dataCalificaciones.length === 0 && idSubject > 0
							? 'No existe matriculados en esta materia'
							: 'Debe seleccionar los filtros por favor'}
					</Typography>
				)}
			</div>
		</MainLayout>
	);
};
