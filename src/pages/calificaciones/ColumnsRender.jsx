import {Chip, IconButton, Tooltip} from '@mui/material';
// import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import SaveAsIcon from '@mui/icons-material/SaveAs';
const handleBlur = (event, setAllNotes, allNotes, id) => {
	const valor = parseInt(event.target.value);
	if (isNaN(valor)) {
		// si no es un número, restablecer el valor a ''
		event.target.value = '';
	} else if (valor < 0) {
		// si es menor que 0, establecer el valor a 0
		event.target.value = 0;
	} else if (valor > 100) {
		// si es mayor que 100, establecer el valor a 100
		event.target.value = 100;
	}

	allNotes.forEach((estudianteNota) => {
		if (estudianteNota.id === id) {
			estudianteNota.qualification = valor;
			if (valor < 70) estudianteNota.approved = false;
			else estudianteNota.approved = true;
		}
	});
	setAllNotes(allNotes);
};
export const columnsRender = (handleEdit, handleDelete, dataPermission, setAllNotes, allNotes) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 200,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<Tooltip title={'Registrar todas las notas'} placement='top'>
							<IconButton
								disabled={dataPermission?.ok_update ? false : true}
								aria-label='add'
								color='secondary'
								onClick={() => handleEdit(params.id)}>
								<SaveAsIcon />
							</IconButton>
						</Tooltip>
					)}
				</div>
			),
		};

	return [
		{field: 'id', headerName: 'ID', width: 70},
		{
			field: 'person_full_name',
			headerName: 'Nombre Completo',
			description: 'Nombre del Estudiante',
			sortable: false,
			width: 260,
		},

		{
			field: 'person_ci',
			headerName: 'Carnet de Identidad',
			width: 250,
			description: 'Cédula de Identidad del estudiante',
		},
		{
			field: 'person_cellphone',
			headerName: 'Número de Celular',
			description: 'Número de celular de estudiante',
			width: 200,
		},
		{
			field: 'approved',
			headerName: 'Estado',
			description: 'Aqui se refleja el estado del estudiante',
			width: 200,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.approved ? 'Aprobado' : 'Reprobado'}
						sx={{
							backgroundColor: params.row.approved ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.approved ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
		{
			field: 'qualification',
			headerName: 'Calificación',
			description: 'Aqui se refleja la calificación del estudiante',
			width: 200,
			renderCell: (params) => {
				return (
					<input
						autoComplete={'off'}
						type='text' // usar type='text' para que el input acepte solo números del 0 al 9
						name={`field_${params.row.id}`}
						id={`field_${params.row.id}`}
						onChange={(e) => {
							handleBlur(e, setAllNotes, allNotes, params.row.id);
						}}
						placeholder={0}
						defaultValue={`${params.row.qualification}`}
						disabled={dataPermission?.ok_update ? false : true}
						style={{
							border: ' 1px solid #ccc',
							minWidth: '30px',
							maxWidth: '50%',
							borderRadius: '6px',
							padding: '5px',
						}}
					/>
				);
			},
		},

		accionOperations,
	];
};
