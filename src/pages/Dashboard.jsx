import {Avatar, Box, Card, Divider, Grid, Stack, Typography} from '@mui/material';
import {MainLayout} from '../layouts/MainLayout';
import {borderRadius} from '../config';
import FacebookIcon from '@mui/icons-material/Facebook';
import {blue, pink, red} from '@mui/material/colors';
import YouTubeIcon from '@mui/icons-material/YouTube';
import InstagramIcon from '@mui/icons-material/Instagram';
import imagen from '../assets/images/blanco.png';
import {CarouselComponent} from '../components/CarouselComponent';

export const Dashboard = () => {
	const handleRedirect = (url) => {
		window.open(url, '_blank');
	};
	// const handlePrint = async (url, name = 'reporte') => {
	// 	await getPDF(`${url}`, name);
	// };

	return (
		<MainLayout>
			{/* <Grid container spacing={4} marginBottom={'20px'}>
				<Grid item xs={12} sm={4} md={4} lg={4}>
					<Card
						component={Button}
						onClick={() =>
							handlePrint('/api/v1/program/reporte', 'universidad-andina-programas')
						}
						fullWidth
						sx={{
							padding: '20px',
							boxShadow: 'none',
							borderRadius: `${borderRadius}px`,
						}}>
						<Grid container display={'flex'} justifyContent={'space-around'}>
							<Grid
								item
								xs={12}
								sm={6}
								md={7}
								lg={8}
								display={'flex'}
								flexDirection={'column'}
								justifyContent={'start'}
								alignItems={'start'}>
								<Typography variant='h4'>Programas</Typography>
								<Typography variant='body2' sx={{marginTop: '5px'}}>
									Descargas todos los programas
								</Typography>
							</Grid>
							<Grid
								item
								xs={12}
								sm={6}
								md={5}
								lg={4}
								display={'flex'}
								justifyContent={'end'}
								alignItems={'center'}>
								<SystemUpdateAltIcon
									style={{width: '30px', height: '30px'}}
									color='secondary'
								/>
							</Grid>
						</Grid>
					</Card>
				</Grid>
				<Grid item xs={12} sm={4} md={4} lg={4}>
					<Card
						component={Button}
						onClick={() =>
							handlePrint('/api/v1/subject/reporte', 'universidad-andina-asignaturas')
						}
						fullWidth
						sx={{
							padding: '20px',
							boxShadow: 'none',
							borderRadius: `${borderRadius}px`,
						}}>
						<Grid container display={'flex'} justifyContent={'space-around'}>
							<Grid
								item
								xs={12}
								sm={6}
								md={7}
								lg={8}
								display={'flex'}
								flexDirection={'column'}
								justifyContent={'start'}
								alignItems={'start'}>
								<Typography variant='h4'>Asignaturas</Typography>
								<Typography variant='body2' sx={{marginTop: '5px'}}>
									Descargas todos las asignaturas que existe
								</Typography>
							</Grid>
							<Grid
								item
								xs={12}
								sm={6}
								md={5}
								lg={4}
								display={'flex'}
								justifyContent={'end'}
								alignItems={'center'}>
								<SystemUpdateAltIcon
									style={{width: '30px', height: '30px'}}
									color='secondary'
								/>
							</Grid>
						</Grid>
					</Card>
				</Grid>
				<Grid item xs={12} sm={4} md={4} lg={4}>
					<Card
						component={Button}
						onClick={() =>
							handlePrint('/api/v1/area/reporte', 'universidad-andina-areas')
						}
						fullWidth
						sx={{
							padding: '20px',
							boxShadow: 'none',
							borderRadius: `${borderRadius}px`,
						}}>
						<Grid container display={'flex'} justifyContent={'space-around'}>
							<Grid
								item
								xs={12}
								sm={6}
								md={7}
								lg={8}
								display={'flex'}
								flexDirection={'column'}
								justifyContent={'start'}
								alignItems={'start'}>
								<Typography variant='h4'>Areas</Typography>
								<Typography variant='body2' sx={{marginTop: '5px'}}>
									Descargas todos las areas que existen
								</Typography>
							</Grid>
							<Grid
								item
								xs={12}
								sm={6}
								md={5}
								lg={4}
								display={'flex'}
								justifyContent={'end'}
								alignItems={'center'}>
								<SystemUpdateAltIcon
									style={{width: '30px', height: '30px'}}
									color='secondary'
								/>
							</Grid>
						</Grid>
					</Card>
				</Grid>
			</Grid> */}

			<Grid container spacing={4}>
				<Grid item xs={12} sm={8} md={8} lg={8}>
					<Box
						sx={{
							width: '100%',
							bgcolor: 'background.paper',
							padding: '20px',
							boxShadow: 'none',
							borderRadius: `${borderRadius}px`,
						}}>
						<Box sx={{my: 1}}>
							<Grid container alignItems='center'>
								<Grid item xs>
									<Typography gutterBottom variant='h4' component='div'>
										Sobre nosotros
									</Typography>
								</Grid>
								<Grid item>
									<Typography gutterBottom variant='h6' component='div'>
										{/* $4.50 */}
									</Typography>
								</Grid>
							</Grid>
							<Typography color='text.secondary' variant='body2'></Typography>
						</Box>
						<Divider variant='' />
						<Box sx={{my: 2, marginBottom: '25px'}}>
							<Typography variant='body2'>
								La Universidad Andina Simón Bolívar es una institución académica
								autónoma que se dedica a la investigación, la enseñanza
								universitaria y la prestación de servicios; con especial énfasis en
								la transmisión de conocimientos científicos/tecnológicos, el
								desarrollo de la cultura, el fomento al espíritu de cooperación
								entre las universidades de la Comunidad Andina, el fortalecimiento
								de los principios fundamentales que presiden la integración y el
								desarrollo de la Subregión.
							</Typography>
							<br />
							<Typography variant='h5'>Entre sus objetivos se encuentran:</Typography>
							<br />

							<Typography marginBottom={'12px'}>
								Concurrir a la solución práctica de los grandes problemas de la
								Comunidad Andina, mediante la investigación y el estudio, en el
								marco del desarrollo y la integración andina; con esta finalidad,
								contribuir a la capacitación científica, técnica y profesional de
								recursos humanos necesarios.
							</Typography>
							<Typography marginBottom={'12px'}>
								Fomentar y difundir los valores culturales que expresen los ideales,
								tradiciones y peculiaridades nacionales de los pueblos de la
								Comunidad Andina.
							</Typography>
							<Typography marginBottom={'12px'}>
								Desarrollar actividades en todos los países miembros de la Comunidad
								Andina; así como crear mecanismos de trabajo que lleven al
								fortalecimiento de la institución educativa comunitaria y su
								eficiente vínculo con los demás órganos del Sistema Andino.
							</Typography>
						</Box>
					</Box>
				</Grid>
				<Grid item xs={12} sm={4} md={4} lg={4}>
					<Box
						sx={{
							width: '100%',
							bgcolor: 'background.paper',
							padding: '20px',
							boxShadow: 'none',
							borderRadius: `${borderRadius}px`,
						}}>
						<Box sx={{my: 1}}>
							<Grid container alignItems='center'>
								<Grid item xs>
									<Typography gutterBottom variant='h4' component='div'>
										Contactanos
									</Typography>
								</Grid>
								<Grid item>
									<Typography gutterBottom variant='h6' component='div'>
										{/* $4.50 */}
									</Typography>
								</Grid>
							</Grid>
							<Typography color='text.secondary' variant='body2'></Typography>
						</Box>
						<Divider variant='' />
						<Box sx={{m: 2, marginBottom: '30px'}}>
							<Stack direction='column' spacing={2}>
								<Grid display={'flex'} alignItems={'center'}>
									<Avatar
										style={{cursor: 'pointer'}}
										sx={{bgcolor: '#fdc318'}}
										onClick={() => handleRedirect('http://info@uasb.edu.bo')}>
										<img
											src={imagen}
											alt='Imagen'
											style={{
												height: '5vh',
											}}
										/>
									</Avatar>

									<Typography
										marginLeft={3}
										variant='h6'
										style={{cursor: 'pointer'}}
										onClick={() => handleRedirect('http://info@uasb.edu.bo')}>
										Pagina Web
									</Typography>
								</Grid>
								<Grid display={'flex'} alignItems={'center'}>
									<Avatar
										style={{cursor: 'pointer'}}
										sx={{bgcolor: blue[500]}}
										onClick={() =>
											handleRedirect(
												'https://www.facebook.com/universidadandina.bolivar'
											)
										}>
										<FacebookIcon />
									</Avatar>
									<Typography
										style={{cursor: 'pointer'}}
										marginLeft={3}
										variant='h6'
										onClick={() =>
											handleRedirect(
												'https://www.facebook.com/universidadandina.bolivar'
											)
										}>
										Facebook
									</Typography>
								</Grid>
								<Grid display={'flex'} alignItems={'center'}>
									<Avatar
										style={{cursor: 'pointer'}}
										sx={{bgcolor: red[500]}}
										onClick={() =>
											handleRedirect('https://youtube.com/@uasbolivia')
										}>
										<YouTubeIcon />
									</Avatar>
									<Typography
										style={{cursor: 'pointer'}}
										marginLeft={3}
										variant='h6'
										onClick={() =>
											handleRedirect('https://youtube.com/@uasbolivia')
										}>
										YouTube
									</Typography>
								</Grid>
								<Grid display={'flex'} alignItems={'center'}>
									<Avatar
										style={{cursor: 'pointer'}}
										sx={{bgcolor: pink[500]}}
										onClick={() =>
											handleRedirect('https://www.instagram.com/uasbbolivia/')
										}>
										<InstagramIcon />
									</Avatar>
									<Typography
										style={{cursor: 'pointer'}}
										marginLeft={3}
										variant='h6'
										onClick={() =>
											handleRedirect('https://www.instagram.com/uasbbolivia/')
										}>
										Instagram
									</Typography>
								</Grid>
							</Stack>
						</Box>
						<Divider variant='' />
						<Box sx={{mt: 3, ml: 1, mb: 1}}></Box>
					</Box>
				</Grid>
			</Grid>
			<br />
			<Card
				sx={{
					width: '100%',
					bgcolor: 'background.paper',
					padding: '20px',
					boxShadow: 'none',
					borderRadius: `${borderRadius}px`,
				}}>
				<Typography
					variant='h3'
					alignItems={'center'}
					justifyContent={'center'}
					display={'flex'}>
					Ofertas académicas
				</Typography>
			</Card>

			<Grid container marginTop={'20px'}>
				<Grid item xs={12} sm={12} md={12} lg={12}>
					<CarouselComponent />
				</Grid>
			</Grid>
		</MainLayout>
	);
};
