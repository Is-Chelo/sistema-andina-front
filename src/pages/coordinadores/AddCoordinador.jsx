import {
	Box,
	Button,
	Card,
	CardContent,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
	Alert,
} from '@mui/material';
import PropTypes from 'prop-types';
import AddIcon from '@mui/icons-material/Add';
import React, {useState} from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {postRequest} from '../../api/Requests';
import DeleteIcon from '@mui/icons-material/Delete';

const initialValues = {
	name: '',
	last_name: '',
	email: '',
	username: '',
	date_birth: '',
	cellphone: '',
	active: 1,
	ci_number: '',
};

const validationFields = Yup.object({
	name: Yup.string().required('El nombre del coordinador es requerido.'),
	last_name: Yup.string().required('El apellido del coordinador es requerido.'),
	email: Yup.string().email('Correo electrónico inválido.').required('Este campo es requerido.'),
	username: Yup.string().required('El nombre de usuario es requerido.'),
	cellphone: Yup.string()
		.required('El número de celular es requerido.')
		.min(6, 'Introduzca un numero mayor a 5 dígitos'),
});

export const AddCoordinador = ({change, setChange, setState}) => {
	const [disableButton, setDisableButton] = useState(false);

	const [activo, setActivo] = React.useState(1);
	const [serverError, setServerError] = React.useState('');

	const handleChange = (event) => {
		initialValues.active = event.target.value;
		setActivo(event.target.value);
	};

	const onSubmit = async (values) => {
		setDisableButton(true);
		const response = await postRequest('/coordinador', values);
		if (response.status) {
			setChange(!change);

			setState(false);
		} else setServerError(response.message);
		setDisableButton(false);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '55px'}}>
			<form onSubmit={formik.handleSubmit}>
				<Card
					sx={{
						maxHeight: '80vh',
						overflowY: 'auto',
						boxShadow: 'none',
						marginBottom: '50px',
						display: 'flex',
					}}>
					<CardContent sx={{maxHeight: '100vh'}} style={{marginBotton: '250px'}}>
						<Typography variant='h3'>Agregar Coordinadores</Typography>
						<div style={{paddingTop: '35px'}}>
							<Box
								sx={{
									'& .MuiTextField-root': {mb: 3},
								}}
								noValidate
								autoComplete='off'>
								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Nombre'
											fullWidth
											name='name'
											variant='outlined'
											value={formik.values.name}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.name && Boolean(formik.errors.name)
											}
											helperText={formik.touched.name && formik.errors.name}
										/>
									</Grid>
									<Grid item xs={12} sm={6}>
										<TextField
											fullWidth
											label='Apellidos'
											name='last_name'
											variant='outlined'
											value={formik.values.last_name}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.last_name &&
												Boolean(formik.errors.last_name)
											}
											helperText={
												formik.touched.last_name && formik.errors.last_name
											}
										/>
									</Grid>
								</Grid>

								<TextField
									sx={{marginBottom: '15px'}}
									label='Nombre de Usuario'
									name='username'
									fullWidth
									variant='outlined'
									value={formik.values.username}
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									error={
										formik.touched.username && Boolean(formik.errors.username)
									}
									helperText={formik.touched.username && formik.errors.username}
								/>
								<TextField
									sx={{marginBottom: '15px'}}
									label='Correo Electrónico'
									name='email'
									type='email'
									fullWidth
									variant='outlined'
									value={formik.values.email}
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									error={formik.touched.email && Boolean(formik.errors.email)}
									helperText={formik.touched.email && formik.errors.email}
								/>
								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Número de Celular'
											name='cellphone'
											fullWidth
											variant='outlined'
											type='number'
											value={formik.values.cellphone}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.cellphone &&
												Boolean(formik.errors.cellphone)
											}
											helperText={
												formik.touched.cellphone && formik.errors.cellphone
											}
										/>
									</Grid>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Fecha de Nacimiento'
											name='date_birth'
											fullWidth
											type='date'
											variant='outlined'
											InputLabelProps={{
												shrink: true,
												placeholder: 'Selecciona una fecha',
											}}
											value={formik.values.date_birth}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.date_birth &&
												Boolean(formik.errors.date_birth)
											}
											helperText={
												formik.touched.date_birth &&
												formik.errors.date_birth
											}
										/>
									</Grid>
								</Grid>

								<TextField
									sx={{marginBottom: '15px'}}
									label='Número de Carnet'
									name='ci_number'
									fullWidth
									type='number'
									variant='outlined'
									value={formik.values.ci_number}
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									error={
										formik.touched.ci_number && Boolean(formik.errors.ci_number)
									}
									helperText={formik.touched.ci_number && formik.errors.ci_number}
								/>
								<FormControl fullWidth>
									<InputLabel id='demo-simple-select-label'>Activo</InputLabel>
									<Select
										labelId='demo-simple-select-label'
										id='demo-simple-select'
										label='Activo'
										value={activo}
										onChange={handleChange}
										onBlur={formik.handleBlur}>
										<MenuItem value={1}>Si</MenuItem>
										<MenuItem value={0}>No</MenuItem>
									</Select>
								</FormControl>

								{serverError && (
									<Alert
										sx={{
											marginY: '20px',
										}}
										severity='error'>
										{serverError}
									</Alert>
								)}
							</Box>
						</div>
					</CardContent>
				</Card>
				<Box
					sx={{
						height: '60px',
						position: 'absolute',
						marginLeft: '18px',
						bottom: 0,
					}}>
					<Button
						disabled={disableButton}
						color='primary'
						variant='contained'
						type='submit'
						endIcon={<AddIcon />}>
						Agregar
					</Button>
					<Button
						color='error'
						variant='text'
						onClick={() => {
							setState(false);
						}}
						endIcon={<DeleteIcon />}>
						Cancelar
					</Button>
				</Box>
			</form>
		</div>
	);
};

AddCoordinador.propTypes = {
	change: PropTypes.bool,
	setChange: PropTypes.func,
	setState: PropTypes.func,
};
