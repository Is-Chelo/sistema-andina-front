import {Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';

export const columnsRender = (handleEdit, handleDelete, dataPermission) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 130,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};

	return [
		{field: 'index', headerName: 'Nro', width: 50},
		{
			field: 'fullName',
			headerName: 'Nombre Completo',
			description: 'Nombre Completo del Coordinador',
			sortable: false,
			width: 220,
			valueGetter: (params) =>
				`${params.row.person_name || ''} ${params.row.person_last_name || ''}`,
		},
		// { field: 'person_address', headerName: 'Dirección', width: 150 },
		{field: 'person_email', headerName: 'Email', width: 250, description: 'Correo electrónico'},
		{field: 'person_cellphone', headerName: 'Número de Celular', width: 180},
		{
			field: 'person_date_birth',
			headerName: 'Fecha de Nacimiento',
			description: 'Fecha de nacimiento.',
			sortable: false,
			width: 200,
			valueGetter: (params) =>
				`${moment(params.row.person_date_birth).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'ci',
			headerName: 'Carnet de Identidad',
			description: 'Carnet de Identidad del Coordinador',
			sortable: false,
			width: 200,
			valueGetter: (params) => `${params.row.person_ci_number || ''}`,
		},
		{
			field: 'coordinador_active',
			headerName: 'Activo',
			width: 150,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.coordinador_active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.coordinador_active
								? '#b9f6ca60'
								: '#fbe9e7',
							color: params.row.coordinador_active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
		{
			field: 'coordinador_type_rol',
			headerName: 'Rol',
			width: 130,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.coordinador_type_rol}
						sx={{backgroundColor: '#ffc107', color: '#fff'}}
						size='small'
					/>
				);
			},
		},
		accionOperations,
	];
};
