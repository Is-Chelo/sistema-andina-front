import {Backdrop, Box, Drawer, Fade, Modal,  useMediaQuery} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useEffect, useState} from 'react';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {deleteRequest, getRequest} from '../../api/Requests';
import Edit from './Edit';
import Swal from 'sweetalert2';
import {AddEstudiante} from './AddEstudiante';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import moment from 'moment';
import {useTheme} from '@emotion/react';
import {CustomButtons2} from '../../components/CustomButtons2';
import {columnsRenderPrograms} from './columnsRenderPrograms';
import {CustomButtons3} from '../../components/CustomButtons3';

export const ListEstudiantes = () => {
	// MODAL VER
	const [open, setOpen] = useState(false);
	const handleClose = () => setOpen(false);
	const [estudiantesData, setEstudiantessData] = useState([]);
	const [idPRintReporte, setIdPRintReporte] = useState();

	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));

	const [change, setChange] = useState(false);
	const [state, setState] = useState(false);
	const [edit, setEdit] = useState(false);
	const [initialValues, setInitialValues] = useState({});
	const [changePermissions, setChangePermissions] = useState(false);
	const data = useFetch('api/v1/student', change);

	const toggleDrawer = (open) => (event) => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		setEdit(false);
		setState(open);
	};

	// PERMISOS
	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/student');
				setChange(!change);
			}
		});
	};

	const handleEdit = async (id) => {
		const datos = await getRequest(`api/v1/student/${id}`);
		setInitialValues({
			name: datos.person_name,
			last_name: datos.person_last_name,
			email: datos.person_email,
			username: datos.person_username,
			date_birth: moment(datos.person_date_birth).format('YYYY-MM-DD'),
			cellphone: datos.person_cellphone,
			active: datos.student_active ? 1 : 0,
			ci_number: datos.person_ci_number,
			id: id,
		});
		setEdit(true);
		setState(true);
	};

	const handlerShow = async (id) => {
		const datos = await getRequest(`api/v1/student/programs-by-student/${id}`);
		setIdPRintReporte(id);
		setEstudiantessData(datos);
	};

	const columns = columnsRender(handleEdit, handleDelete, dataPermission, handlerShow, setOpen);
	const columnsPrograms = columnsRenderPrograms();

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud
					title='Estudiantes'
					parrafo='Listado de Estudiantes.'
					category={'Usuarios'}
				/>
			</div>

			<MainCard>
				<div className='animate__animated animate__fadeIn'>
					<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
						{data?.status === false ? (
							<></>
						) : (
							<>
								{data !== undefined ? (
									<DataGrid
										rows={data}
										columns={columns}
										pageSize={5}
										rowsPerPageOptions={[5]}
										slots={{
											toolbar: CustomButtons2,
										}}
										checkboxSelection
										disableRowSelectionOnClick
										unstable_cellSelection
										experimentalFeatures={{clipboardPaste: true}}
										unstable_ignoreValueFormatterDuringExport
										// FIlters
										slotProps={{
											toolbar: {
												showQuickFilter: true,
												quickFilterProps: {debounceMs: 500},
												url: '/api/v1/student/reporte',
												toggleDrawer: toggleDrawer,
												okInsert: dataPermission?.ok_insert,
												name: 'reporte-estudiantes',
											},
										}}
										disableColumnFilter
										disableColumnSelector
										disableDensitySelector
									/>
								) : (
									<SkeletonLoading />
								)}
							</>
						)}
					</div>
				</div>
			</MainCard>

			<div>
				<Drawer anchor='right' open={state} onClose={toggleDrawer(false)}>
					<Box sx={{width: matchXS ? 320 : 500}} role='presentation'>
						{edit ? (
							<Edit
								change={change}
								setChange={setChange}
								setState={setState}
								initialValues={initialValues}
							/>
						) : (
							<AddEstudiante
								change={change}
								setChange={setChange}
								setState={setState}
							/>
						)}
					</Box>
				</Drawer>
			</div>

			<div>
				<Modal
					aria-labelledby='transition-modal-title'
					aria-describedby='transition-modal-description'
					open={open}
					onClose={handleClose}
					closeAfterTransition
					slots={{backdrop: Backdrop}}
					slotProps={{
						backdrop: {
							timeout: 500,
						},
					}}>
					<Fade in={open}>
						<Box
							sx={{
								position: 'absolute',
								top: '50%',
								left: '50%',
								transform: 'translate(-50%, -50%)',
								width: '30vw',
								height: '50vh',
								bgcolor: 'background.paper',
								boxShadow: 24,
								borderRadius: '10px',
								p: 4,
							}}>
							<DataGrid
								rows={estudiantesData}
								columns={columnsPrograms}
								pageSize={5}
								rowsPerPageOptions={[5]}
								slots={{
									toolbar: CustomButtons3,
								}}
								// slots={{toolbar: GridToolbar}}
								checkboxSelection
								disableRowSelectionOnClick
								unstable_cellSelection
								experimentalFeatures={{clipboardPaste: true}}
								unstable_ignoreValueFormatterDuringExport
								// FIlters
								slotProps={{
									toolbar: {
										showQuickFilter: true,
										quickFilterProps: {debounceMs: 500},
										url: `api/v1/student/programs-by-student-report/${idPRintReporte}`,
										name: 'reporte-programas-del-estudiante',
										nameView: 'Programas',
									},
								}}
								disableColumnFilter
								disableColumnSelector
								disableDensitySelector
							/>
						</Box>
					</Fade>
				</Modal>
			</div>
		</MainLayout>
	);
};
