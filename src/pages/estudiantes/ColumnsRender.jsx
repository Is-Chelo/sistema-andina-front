import {Button, Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';

export const columnsRender = (
	handleEdit,
	handleDelete,
	dataPermission,
	handlerShow,
	setShowModal
) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};

	return [
		{field: 'index', headerName: 'Nro', width: 100},
		{
			field: 'fullName',
			headerName: 'Nombre Completo',
			description: 'Nombre Completo del Estudiante',
			sortable: false,
			width: 180,
			valueGetter: (params) =>
				`${params.row.person_name || ''} ${params.row.person_last_name || ''}`,
		},
		// { field: 'person_address', headerName: 'Dirección', width: 150 },
		{field: 'person_email', headerName: 'Email', width: 250, description: 'Correo electrónico'},
		{field: 'person_cellphone', headerName: 'Número de Celular', width: 180},
		{
			field: 'person_date_birth',
			headerName: 'Fecha de Nacimiento',
			description: 'Fecha de nacimiento.',
			sortable: false,
			width: 180,
			valueGetter: (params) =>
				`${moment(params.row.person_date_birth).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'ci',
			headerName: 'Carnet de Identidad',
			description: 'Carnet de Identidad del estudiante.',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${params.row.person_ci_number || ''}`,
		},
		{
			field: 'student_active',
			headerName: 'Activo',
			width: 150,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.student_active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.student_active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.student_active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
		{
			field: 'total_students',
			headerName: 'Programas',
			width: 150,
			renderCell: (params) => (
				<Button
					onClick={() => {
						handlerShow(params.id);
						setShowModal(true);
					}}
					startIcon={<RemoveRedEyeIcon />}>
					ver
				</Button>
			),
		},
		{
			field: 'student_type_rol',
			headerName: 'Rol',
			width: 150,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.student_type_rol}
						sx={{backgroundColor: '#ffc107', color: '#fff'}}
						size='small'
					/>
				);
			},
		},

		accionOperations,
	];
};
