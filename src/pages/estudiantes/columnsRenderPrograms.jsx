import moment from 'moment';

export const columnsRenderPrograms = () => {
	return [
		{field: 'id', headerName: 'Nro', width: 60},

		{
			field: 'program_name',
			headerName: 'Nombre del Programa',
			description: 'Nombre del Programa',
			sortable: false,
			width: 100,
		},
		{
			field: 'program_version',
			headerName: 'Versión',
			description: 'Versión del programa',
			sortable: false,
			width: 100,
		},
		{
			field: 'sede',
			headerName: 'Sede',
			description: 'Sede del Programa',
			width: 100,
		},
		{
			field: 'date_init',
			headerName: 'Fecha de Inicio',
			description: 'Fecha de Inicio',
			width: 100,
			valueGetter: (params) => `${moment(params.row.date_init).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'date_end',
			headerName: 'Fecha de Finalizacion',
			description: 'Fecha de Finalizacion',
			width: 100,
			valueGetter: (params) => `${moment(params.row.date_end).format('YYYY-MM-DD') || ''}`,
		},
	];
};
