import {Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';

export const columnsRender = (handleEdit, handleDelete) => {
	return [
		{field: 'id', headerName: 'ID', width: 150},
		{
			field: 'module_name',
			headerName: 'Nombre del Módulo ',
			description: 'Describe el nombre del módulo',
			sortable: false,
			width: 200,
			valueGetter: (params) => `${params.row.module_name || ''} `,
		},
		{
			field: 'module_type',
			headerName: 'Sección del Módulo',
			description: 'Dscribe la sección del módulo',
			sortable: false,
			width: 200,
			valueGetter: (params) => `${params.row.module_type || ''} `,
		},

		{
			field: 'ok_select',
			headerName: 'Permisos para Listar',
			description: 'Permisos para Listar',
			sortable: false,
			width: 200,
			renderCell: (params) => (
				<Chip
					label={params.row.ok_select ? 'Si' : 'No'}
					sx={{
						backgroundColor: params.row.ok_select ? '#b9f6ca60' : '#fbe9e7',
						color: params.row.ok_select ? '#00c853' : '#d84315',
					}}
					size='small'
				/>
			),
		},
		{
			field: 'ok_insert',
			headerName: 'Permisos para Agregar',
			description: 'Permisos para Agregar',
			sortable: false,
			width: 200,
			renderCell: (params) => (
				<Chip
					label={params.row.ok_insert ? 'Si' : 'No'}
					sx={{
						backgroundColor: params.row.ok_insert ? '#b9f6ca60' : '#fbe9e7',
						color: params.row.ok_insert ? '#00c853' : '#d84315',
					}}
					size='small'
				/>
			),
		},
		{
			field: 'ok_update',
			headerName: 'Permisos para Actualizar',
			description: 'Permisos para Actualizar',
			sortable: false,
			width: 200,
			renderCell: (params) => (
				<Chip
					label={params.row.ok_update ? 'Si' : 'No'}
					sx={{
						backgroundColor: params.row.ok_update ? '#b9f6ca60' : '#fbe9e7',
						color: params.row.ok_update ? '#00c853' : '#d84315',
					}}
					size='small'
				/>
			),
		},

		{
			field: 'ok_delete',
			headerName: 'Permisos para Eliminar',
			description: 'Permisos para Eliminar',
			sortable: false,
			width: 200,
			renderCell: (params) => (
				<Chip
					label={params.row.ok_delete ? 'Si' : 'No'}
					sx={{
						backgroundColor: params.row.ok_delete ? '#b9f6ca60' : '#fbe9e7',
						color: params.row.ok_delete ? '#00c853' : '#d84315',
					}}
					size='small'
				/>
			),
		},
		{
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					<IconButton
						aria-label='add'
						color='secondary'
						onClick={() => handleEdit(params.id, params.row)}>
						<CreateOutlinedIcon />
					</IconButton>
					<IconButton
						aria-label='add'
						color='error'
						onClick={() => handleDelete(params.id)}>
						<DeleteIcon />
					</IconButton>
				</div>
			),
		},
	];
};
