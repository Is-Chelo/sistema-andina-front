import {useParams} from 'react-router-dom';
import {MainLayout} from '../../layouts/MainLayout';
import {useFetch} from '../../hooks/useFetch';
import {useState} from 'react';
import CardCrud from '../../components/CardCrud';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import MainCard from '../../components/MainCard';
import {deleteRequest} from '../../api/Requests';
import Swal from 'sweetalert2';
import {Box, Drawer, Fab, Grid, IconButton} from '@mui/material';
import SearchTable from '../../components/searchTable';
import AddIcon from '@mui/icons-material/Add';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import Edit from './Edit';
import {AddRol} from './AddRol';

export const RolePermission = () => {
	const {id} = useParams();
	// const [change] = useState(false);
	const [change, setChange] = useState(false);
	const [state, setState] = useState(false);
	const [edit, setEdit] = useState(false);
	const [initialValues, setInitialValues] = useState({});

	const handleEdit = async (idRoleModule, values) => {
		setInitialValues({
			id_module: values.module_id,
			module_name: values.module_name,
			id_rol: id,
			ok_select: values.ok_select ? 1 : 0,
			ok_update: values.ok_update ? 1 : 0,
			ok_delete: values.ok_delete ? 1 : 0,
			ok_insert: values.ok_insert ? 1 : 0,
			id: values.id,
		});
		setEdit(true);
		setState(true);
	};

	const data = useFetch(`api/v1/role/${id}`, change);
	const toggleDrawer = (open) => (event) => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		setEdit(false);
		setState(open);
	};

	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/role-module');
				setChange(!change);
			}
		});
	};

	const columns = columnsRender(handleEdit, handleDelete);

	return (
		<>
			<MainLayout>
				{data ? (
					<>
						<CardCrud
							title={`Rol ${data.name}`}
							parrafo={`Listado de los permisos para el rol ${data.name}.`}
							category={'Permisos'}
						/>
						<MainCard>
							<div
								style={{
									height: 80,
									width: '100%',
									borderStyle: 'none',
									paddingTop: '20px',
								}}>
								<Grid
									container
									spacing={0}
									alignItems='center'
									justifyContent='center'>
									<Grid item lg={10} md={8} xs={3} sm={6}>
										<SearchTable content='Buscar Estudiantes' />
									</Grid>
									<Grid
										item
										lg={2}
										md={4}
										xs={7}
										sm={6}
										display='flex'
										alignItems='center'
										justifyContent='center'>
										<IconButton
											color=''
											disabled
											aria-label='add an print'
											sx={{
												marginRight: '15px',
											}}>
											<LocalPrintshopOutlinedIcon />
										</IconButton>

										<Fab
											size='small'
											onClick={toggleDrawer(true)}
											color='primary'
											aria-label='add'>
											<AddIcon />
										</Fab>
									</Grid>
								</Grid>
							</div>
							<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
								{data !== undefined ? (
									<DataGrid
										rows={data?.roleModule}
										columns={columns}
										pageSize={5}
										rowsPerPageOptions={[5]}
									/>
								) : (
									<SkeletonLoading />
								)}
							</div>
						</MainCard>
					</>
				) : (
					<SkeletonLoading />
				)}
				<div>
					<Drawer anchor='right' open={state} onClose={toggleDrawer(false)}>
						<Box sx={{width: 500}} role='presentation'>
							{edit ? (
								<Edit
									change={change}
									setChange={setChange}
									setState={setState}
									initialValues={initialValues}
								/>
							) : (
								<AddRol change={change} setChange={setChange} setState={setState} />
							)}
						</Box>
					</Drawer>
				</div>
			</MainLayout>
		</>
	);
};
