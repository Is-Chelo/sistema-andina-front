/* eslint-disable react/prop-types */
import {
	Alert,
	Box,
	Button,
	Card,
	CardContent,
	FormControl,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	OutlinedInput,
	Select,
	Typography,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {updateRequest} from '../../api/Requests';
import {useState} from 'react';

const validationFields = Yup.object({
	id_module: Yup.number().required('Por favor seleccione el módulo.'),
});

export default function Edit({change, setChange, setState, initialValues}) {
	const [disableButton, setDisableButton] = useState(false);

	const [serverError, setServerError] = useState('');
	const [idModule] = useState({
		id_module: initialValues.id_module,
	});
	const [selectOptions, setSelectOptions] = useState({
		ok_select: initialValues.ok_select,
		ok_update: initialValues.ok_update,
		ok_delete: initialValues.ok_delete,
		ok_insert: initialValues.ok_insert,
	});

	const handleChangeOptions = (event) => {
		setSelectOptions({
			...selectOptions,
			[event.target.name]: event.target.value,
		});
	};

	const onSubmit = async (values) => {
		setDisableButton(true);
		values.id_rol = initialValues.id_rol;
		initialValues.ok_select = selectOptions.ok_select;
		initialValues.ok_update = selectOptions.ok_update;
		initialValues.ok_delete = selectOptions.ok_delete;
		initialValues.ok_insert = selectOptions.ok_insert;

		const response = await updateRequest(initialValues.id, '/role-module', values);
		if (response.status) {
			setChange(!change);
			setState(false);
		} else setServerError(response.message);
		setDisableButton(false);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '55px'}}>
			<form onSubmit={formik.handleSubmit}>
				<Card
					sx={{
						maxHeight: '80vh',
						overflowY: 'auto',
						boxShadow: 'none',
						marginBottom: '50px',
						display: 'flex',
					}}>
					<CardContent
						sx={{maxHeight: '100vh', width: '100%'}}
						style={{marginBotton: '250px'}}>
						<Typography variant='h3'>Editar los permisos</Typography>
						<div style={{paddingTop: '35px'}}>
							<Box
								sx={{
									'& .MuiTextField-root': {mb: 3},
								}}
								noValidate
								autoComplete='off'>
								<FormControl
									sx={{marginBottom: '20px'}}
									fullWidth
									error={
										formik.touched.id_module && Boolean(formik.errors.id_module)
									}>
									<InputLabel id='demo-multiple-name-label'>
										Nombre del Módulo
									</InputLabel>
									<Select
										labelId='id_module'
										id='id_module'
										label='Nombre del Módulo'
										value={idModule}
										disabled
										name='id_module'
										input={<OutlinedInput label='Nombre del Módulo' />}>
										<MenuItem value={idModule}>
											{initialValues.module_name}
										</MenuItem>
									</Select>
									{formik.touched.id_module &&
										Boolean(formik.errors.id_module) && (
											<FormHelperText>
												{formik.errors.id_module}
											</FormHelperText>
										)}
								</FormControl>

								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Listar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth sx={{marginBottom: '20px'}}>
											<InputLabel id='ok_select'>Permitir listar:</InputLabel>
											<Select
												labelId='ok_select'
												id='ok_select'
												name='ok_select'
												label='Permitir listar'
												value={selectOptions.ok_select}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>

								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Insertar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth>
											<InputLabel id='ok_insert'>
												Permitir insertar:
											</InputLabel>
											<Select
												labelId='ok_insert'
												id='ok_insert'
												name='ok_insert'
												label='Permitir insertar:'
												value={selectOptions.ok_insert}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>

								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Actualizar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth>
											<InputLabel id='ok_update'>
												Permitir actualizar:
											</InputLabel>
											<Select
												labelId='ok_update'
												id='ok_update'
												name='ok_update'
												label='Permitir actualizar:'
												value={selectOptions.ok_update}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>
								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Eliminar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth>
											<InputLabel id='ok_delete'>
												Permitir eliminar:
											</InputLabel>
											<Select
												labelId='ok_delete'
												id='ok_delete'
												name='ok_delete'
												label='Permitir eliminar:'
												value={selectOptions.ok_delete}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>
								{serverError && (
									<Alert
										sx={{
											marginY: '20px',
										}}
										severity='error'>
										{serverError}
									</Alert>
								)}
							</Box>
						</div>
					</CardContent>
				</Card>
				<Box
					sx={{
						height: '60px',
						position: 'absolute',
						marginLeft: '18px',
						bottom: 0,
					}}>
					<Button
						disabled={disableButton}
						color='primary'
						variant='contained'
						type='submit'
						endIcon={<AddIcon />}>
						Guardar
					</Button>
					<Button
						color='error'
						variant='text'
						onClick={() => {
							setState(false);
						}}>
						Cancelar
					</Button>
				</Box>
			</form>
		</div>
	);
}
