import {
	Box,
	Button,
	Card,
	CardContent,
	FormControl,
	InputLabel,
	MenuItem,
	Select,
	Typography,
	Alert,
	OutlinedInput,
	FormHelperText,
	Grid,
} from '@mui/material';
import PropTypes from 'prop-types';
import AddIcon from '@mui/icons-material/Add';
import {useEffect, useState} from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {postRequest} from '../../api/Requests';
import {useFetch} from '../../hooks/useFetch';
import {useParams} from 'react-router-dom';

const initialValues = {
	id_module: '',
	id_rol: '',
};

const validationFields = Yup.object({
	id_module: Yup.number().required('Por favor seleccione el módulo.'),
});

export const AddRol = ({change, setChange, setState}) => {
	const [disableButton, setDisableButton] = useState(false);

	const [serverError, setServerError] = useState('');
	const [idModule, setIdModule] = useState(0);
	const {id} = useParams();
	const [dataModule, setDataModule] = useState([]);
	const [selectOptions, setSelectOptions] = useState({
		ok_select: 0,
		ok_update: 0,
		ok_delete: 0,
		ok_insert: 0,
	});

	const handleChange = (event) => {
		if (event.target.value !== 0) initialValues.id_module = event.target.value;
		setIdModule(event.target.value);
	};

	const handleChangeOptions = (event) => {
		setSelectOptions({
			...selectOptions,
			[event.target.name]: event.target.value,
		});
	};

	const data = useFetch(`api/v1/module/modules-by-rol/${id}`);
	useEffect(() => {
		setDataModule(data);
	}, [data]);

	const onSubmit = async (values) => {
		setDisableButton(true);
		initialValues.id_rol = id;
		initialValues.ok_select = selectOptions.ok_select;
		initialValues.ok_update = selectOptions.ok_update;
		initialValues.ok_delete = selectOptions.ok_delete;
		initialValues.ok_insert = selectOptions.ok_insert;
		const response = await postRequest('/role-module', values);
		if (response.status) {
			setChange(!change);
			setState(false);
		} else setServerError(response.message);
		setDisableButton(false);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '55px'}}>
			<form onSubmit={formik.handleSubmit}>
				<Card
					sx={{
						maxHeight: '80vh',
						overflowY: 'auto',
						boxShadow: 'none',
						marginBottom: '50px',
						display: 'flex',
					}}>
					<CardContent
						sx={{maxHeight: '100vh', width: '100%'}}
						style={{marginBotton: '250px'}}>
						<Typography variant='h3'>Agregar Permisos</Typography>
						<div style={{paddingTop: '35px'}}>
							<Box
								sx={{
									'& .MuiTextField-root': {mb: 3},
								}}
								noValidate
								autoComplete='off'>
								<FormControl
									sx={{marginBottom: '20px'}}
									fullWidth
									error={
										formik.touched.id_module && Boolean(formik.errors.id_module)
									}>
									<InputLabel id='demo-multiple-name-label'>
										Nombre del Módulo
									</InputLabel>
									<Select
										labelId='id_module'
										id='id_module'
										label='Nombre del Módulo'
										value={idModule}
										name='id_module'
										onChange={handleChange}
										input={<OutlinedInput label='Nombre del Módulo' />}>
										<MenuItem value={0}>Seleccione un el Módulo</MenuItem>
										{dataModule &&
											dataModule.map((moduleDb) => (
												<MenuItem key={moduleDb.id} value={moduleDb.id}>
													{moduleDb.name}
												</MenuItem>
											))}
									</Select>
									{formik.touched.id_module &&
										Boolean(formik.errors.id_module) && (
											<FormHelperText>
												{formik.errors.id_module}
											</FormHelperText>
										)}
								</FormControl>
								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Listar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth sx={{marginBottom: '20px'}}>
											<InputLabel id='ok_select'>Permitir listar:</InputLabel>
											<Select
												labelId='ok_select'
												id='ok_select'
												name='ok_select'
												label='Permitir listar'
												value={selectOptions.ok_select}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>

								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Insertar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth>
											<InputLabel id='ok_insert'>
												Permitir insertar:
											</InputLabel>
											<Select
												labelId='ok_insert'
												id='ok_insert'
												name='ok_insert'
												label='Permitir insertar:'
												value={selectOptions.ok_insert}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>

								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Actualizar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth>
											<InputLabel id='ok_update'>
												Permitir actualizar:
											</InputLabel>
											<Select
												labelId='ok_update'
												id='ok_update'
												name='ok_update'
												label='Permitir actualizar:'
												value={selectOptions.ok_update}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>
								<Grid
									container
									spacing={2}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center',
										marginBottom: '20px',
									}}>
									<Grid item xs={12} sm={4}>
										<Typography
											variant='body2'
											style={{justifyContent: 'center', fontWeight: 500}}>
											Permitir Eliminar:
										</Typography>
									</Grid>
									<Grid item xs={12} sm={8}>
										<FormControl fullWidth>
											<InputLabel id='ok_delete'>
												Permitir eliminar:
											</InputLabel>
											<Select
												labelId='ok_delete'
												id='ok_delete'
												name='ok_delete'
												label='Permitir eliminar:'
												value={selectOptions.ok_delete}
												onChange={handleChangeOptions}>
												<MenuItem value={1}>Si</MenuItem>
												<MenuItem value={0}>No</MenuItem>
											</Select>
										</FormControl>
									</Grid>
								</Grid>

								{serverError && (
									<Alert
										sx={{
											marginY: '20px',
										}}
										severity='error'>
										{serverError}
									</Alert>
								)}
							</Box>
						</div>
					</CardContent>
				</Card>
				<Box
					sx={{
						height: '60px',
						position: 'absolute',
						marginLeft: '18px',
						bottom: 0,
					}}>
					<Button
						disabled={disableButton}
						color='primary'
						variant='contained'
						type='submit'
						endIcon={<AddIcon />}>
						Agregar
					</Button>
					<Button
						color='error'
						variant='text'
						onClick={() => {
							setState(false);
						}}>
						Cancelar
					</Button>
				</Box>
			</form>
		</div>
	);
};

AddRol.propTypes = {
	change: PropTypes.bool,
	setChange: PropTypes.func,
	setState: PropTypes.func,
};
