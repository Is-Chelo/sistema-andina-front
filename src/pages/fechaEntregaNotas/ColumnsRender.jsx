import {Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';

export const columnsRender = (handleEdit, handleDelete, dataPermission) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 100},
		{
			field: 'start_time',
			headerName: 'Fecha de Inicio',
			description: 'Fecha de Inicio',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${moment(params.row.start_time).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'close_time',
			headerName: 'Fecha de Finalizacion',
			width: 100,
			valueGetter: (params) => `${moment(params.row.close_time).format('YYYY-MM-DD') || ''}`,
		},

		{
			field: 'active',
			headerName: 'Activo',
			width: 100,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
		accionOperations,
	];
};
