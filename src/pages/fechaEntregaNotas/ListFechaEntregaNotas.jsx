import {Box, Drawer, useMediaQuery} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useEffect, useState} from 'react';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {deleteRequest, getRequest} from '../../api/Requests';
import Edit from './Edit';
import {AddUser} from './AddUser';
import Swal from 'sweetalert2';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import {useTheme} from '@emotion/react';
import moment from 'moment';
import {CustomButtons2} from '../../components/CustomButtons2';

export const ListFechaEntregaNotas = () => {
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));

	const [change, setChange] = useState(false);
	const [state, setState] = useState(false);
	const [edit, setEdit] = useState(false);
	const [changePermissions, setChangePermissions] = useState(false);

	const [initialValues, setInitialValues] = useState({});

	const data = useFetch('api/v1/delivery-qualification', change);

	const toggleDrawer = (open) => (event) => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		setEdit(false);
		setState(open);
	};

	// PERMISOS
	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/delivery-qualification');
				setChange(!change);
			}
		});
	};

	const handleEdit = async (id) => {
		const datos = await getRequest(`api/v1/delivery-qualification/${id}`);
		setInitialValues({
			start_time: moment(datos.start_time).format('YYYY-MM-DD'),
			close_time: moment(datos.close_time).format('YYYY-MM-DD'),
			id: id,
		});
		setEdit(true);
		setState(true);
	};

	const columns = columnsRender(handleEdit, handleDelete, dataPermission);

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud
					title='Entrega de Notas'
					parrafo='Listado de todos los registros.'
					category={'Permisos'}
				/>
			</div>
			<MainCard>
				<div className='animate__animated animate__fadeIn'>
					<div
						style={{
							width: '100%',
							borderStyle: 'none',
						}}></div>
					<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
						{data?.status === false ? (
							<></>
						) : (
							<>
								{data !== undefined ? (
									<DataGrid
										rows={data}
										columns={columns}
										pageSize={5}
										rowsPerPageOptions={[5]}
										slots={{
											toolbar: CustomButtons2,
										}}
										checkboxSelection
										disableRowSelectionOnClick
										unstable_cellSelection
										experimentalFeatures={{clipboardPaste: true}}
										unstable_ignoreValueFormatterDuringExport
										// FIlters
										slotProps={{
											toolbar: {
												showQuickFilter: true,
												quickFilterProps: {debounceMs: 500},
												url: '/api/v1/delivery-qualification/reporte',
												toggleDrawer: toggleDrawer,
												okInsert: dataPermission?.ok_insert,
												name: 'reporte-entrega-notas',
											},
										}}
										disableColumnFilter
										disableColumnSelector
										disableDensitySelector
									/>
								) : (
									<SkeletonLoading />
								)}
							</>
						)}
					</div>
				</div>
			</MainCard>

			<div>
				<Drawer anchor='right' open={state} onClose={toggleDrawer(false)}>
					<Box sx={{width: matchXS ? 320 : 500}} role='presentation'>
						{edit ? (
							<Edit
								change={change}
								setChange={setChange}
								setState={setState}
								initialValues={initialValues}
							/>
						) : (
							<AddUser change={change} setChange={setChange} setState={setState} />
						)}
					</Box>
				</Drawer>
			</div>
		</MainLayout>
	);
};
