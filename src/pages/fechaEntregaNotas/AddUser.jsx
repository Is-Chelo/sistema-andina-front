import {Box, Button, Card, CardContent, Grid, TextField, Typography, Alert} from '@mui/material';
import PropTypes from 'prop-types';
import AddIcon from '@mui/icons-material/Add';
import React, {useState} from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {postRequest} from '../../api/Requests';
import DeleteIcon from '@mui/icons-material/Delete';

const initialValues = {
	start_time: '',
	close_time: '',
};

const validationFields = Yup.object({
	start_time: Yup.date().required('La fecha inicial es requerida.'),
	close_time: Yup.date()
		.min(new Date(), 'La fecha final no puede ser hoy o anterior a hoy.')
		.notOneOf([Yup.ref('start_time')], 'La fecha final no puede ser igual a la fecha inicial.')
		.required('La fecha final es requerida.'),
});

export const AddUser = ({change, setChange, setState}) => {
	const [disableButton, setDisableButton] = useState(false);

	const [serverError, setServerError] = React.useState('');

	const onSubmit = async (values) => {
		setDisableButton(true);

		const response = await postRequest('/delivery-qualification', values);
		if (response.status) {
			setChange(!change);
			setState(false);
		} else setServerError(response.message);
		setDisableButton(false);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '55px'}}>
			<form onSubmit={formik.handleSubmit}>
				<Card
					sx={{
						maxHeight: '80vh',
						overflowY: 'auto',
						boxShadow: 'none',
						marginBottom: '50px',
						display: 'flex',
					}}>
					<CardContent sx={{maxHeight: '100vh'}} style={{marginBotton: '250px'}}>
						<Typography variant='h3'>Crear una fecha para entrega de notas</Typography>
						<div style={{paddingTop: '35px'}}>
							<Box
								sx={{
									'& .MuiTextField-root': {mb: 3},
								}}
								noValidate
								autoComplete='off'>
								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Fecha de Inicio'
											name='start_time'
											fullWidth
											type='date'
											variant='outlined'
											InputLabelProps={{
												shrink: true,
												placeholder: 'Selecciona una fecha',
											}}
											value={formik.values.start_time}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.start_time &&
												Boolean(formik.errors.start_time)
											}
											helperText={
												formik.touched.start_time &&
												formik.errors.start_time
											}
										/>
									</Grid>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Fecha de Finalización'
											name='close_time'
											fullWidth
											type='date'
											variant='outlined'
											InputLabelProps={{
												shrink: true,
												placeholder: 'Selecciona una fecha',
											}}
											value={formik.values.close_time}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.close_time &&
												Boolean(formik.errors.close_time)
											}
											helperText={
												formik.touched.close_time &&
												formik.errors.close_time
											}
										/>
									</Grid>
								</Grid>

								{serverError && (
									<Alert
										sx={{
											marginY: '20px',
										}}
										severity='error'>
										{serverError}
									</Alert>
								)}
							</Box>
						</div>
					</CardContent>
				</Card>
				<Box
					sx={{
						height: '60px',
						position: 'absolute',
						marginLeft: '18px',
						bottom: 0,
					}}>
					<Button
						disabled={disableButton}
						color='primary'
						variant='contained'
						type='submit'
						endIcon={<AddIcon />}>
						Agregar
					</Button>
					<Button
						color='error'
						variant='text'
						onClick={() => {
							setState(false);
						}}
						endIcon={<DeleteIcon />}>
						Cancelar
					</Button>
				</Box>
			</form>
		</div>
	);
};

AddUser.propTypes = {
	change: PropTypes.bool,
	setChange: PropTypes.func,
	setState: PropTypes.func,
};
