/* eslint-disable react/prop-types */
import {
	Alert,
	Box,
	Button,
	Card,
	CardContent,
	CircularProgress,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
	useMediaQuery,
} from '@mui/material';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {updateRequest} from '../../api/Requests';
import {useState} from 'react';
import SaveIcon from '@mui/icons-material/Save';
import {useTheme} from '@mui/material/styles';
import AutoCompleteComponent from '../../components/AutoCompleteComponent';
const validationFields = Yup.object({
	name: Yup.string().required('El nombre del area es requerido.'),
});

export default function Edit({change, setChange, initialValues, setEdit}) {
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));

	const [activo, setActivo] = useState(initialValues.active);
	const [loading, setLoading] = useState(false);
	const [serverError, setServerError] = useState('');
	const [coordinadors, setCoordinadors] = useState([]);

	const handleChange = (event) => {
		initialValues.active = event.target.value;
		setActivo(event.target.value);
	};
	const onSubmit = async (values) => {
		values.id_coordinadors = coordinadors;

		setLoading(true);
		setTimeout(async () => {
			const response = await updateRequest(initialValues.id, '/area', values);
			if (response.status) {
				setLoading(false);
				setChange(!change);
				setEdit(false);
				setCoordinadors([]);
			} else setServerError(response.message);
		}, 1000);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '35px'}}>
			{loading ? (
				<Grid justifyContent={'center'} alignContent={'center'} display={'flex'}>
					<CircularProgress />
				</Grid>
			) : (
				<form onSubmit={formik.handleSubmit}>
					<Card
						sx={{
							maxHeight: '80vh',
							overflowY: 'auto',
							boxShadow: 'none',
							marginBottom: '30px',
						}}>
						<CardContent
							sx={{maxHeight: '100vh', width: '100%'}}
							style={{marginBotton: '250px'}}>
							<Typography variant='h3' sx={{display: 'inline-flex'}}>
								Editar
								<Box sx={{color: theme.palette.secondary.dark, marginLeft: '5px'}}>
									Areas
								</Box>
							</Typography>
							<div style={{paddingTop: '35px'}}>
								<Box
									sx={{
										'& .MuiTextField-root': {mb: 3},
									}}
									noValidate
									autoComplete='off'>
									<Grid container spacing={2}>
										<Grid item xs={12} sm={12}>
											<TextField
												sx={{marginBottom: '15px'}}
												label='Nombre'
												fullWidth
												name='name'
												variant='outlined'
												value={formik.values.name}
												onChange={formik.handleChange}
												onBlur={formik.handleBlur}
												error={
													formik.touched.name &&
													Boolean(formik.errors.name)
												}
												helperText={
													formik.touched.name && formik.errors.name
												}
											/>
										</Grid>
									</Grid>

									<AutoCompleteComponent
										url={'api/v1/coordinador'}
										labelText={'Seleccionar Coordinador'}
										setValues={setCoordinadors}
										campoIterar={'person_full_name'}
										campoDisable={'coordinador_active'}
										valuesEdit={initialValues.id_coodinadors}
									/>

									<FormControl fullWidth sx={{marginTop: '10px'}}>
										<InputLabel id='demo-simple-select-label'>
											Activo
										</InputLabel>
										<Select
											labelId='demo-simple-select-label'
											id='demo-simple-select'
											label='Activo'
											value={activo}
											onChange={handleChange}
											onBlur={formik.handleBlur}>
											<MenuItem value={1}>Si</MenuItem>
											<MenuItem value={0}>No</MenuItem>
										</Select>
									</FormControl>

									{serverError && (
										<Alert
											sx={{
												marginY: '20px',
											}}
											severity='error'>
											{serverError}
										</Alert>
									)}
								</Box>
							</div>
						</CardContent>
					</Card>
					<Grid item display={'flex'} sx={{marginLeft: matchXS ? '0' : '20px'}}>
						<Button
							color='secondary'
							variant='contained'
							type='submit'
							endIcon={<SaveIcon />}>
							Guardar
						</Button>
						<Button
							sx={{
								marginLeft: '10px',
							}}
							color='error'
							variant='text'
							onClick={() => {
								setEdit(false);
							}}>
							Cancelar
						</Button>
					</Grid>
				</form>
			)}
		</div>
	);
}
