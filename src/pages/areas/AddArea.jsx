import {
	Box,
	Button,
	Card,
	CardContent,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
	Alert,
	CircularProgress,
	useTheme,
	useMediaQuery,
} from '@mui/material';
import PropTypes from 'prop-types';
import AddIcon from '@mui/icons-material/Add';
import React, {useState} from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {postRequest} from '../../api/Requests';
import DeleteIcon from '@mui/icons-material/Delete';
import AutoCompleteComponent from '../../components/AutoCompleteComponent';

const initialValues = {
	name: '',
	active: 1,
};

const validationFields = Yup.object({
	name: Yup.string().required('El nombre del area es requerido.'),
});

export const AddArea = ({change, setChange}) => {
	
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));
	const [coordinadors, setCoordinadors] = useState([]);

	const [activo, setActivo] = React.useState(1);
	const [serverError, setServerError] = React.useState('');
	const [loading, setLoading] = useState(false);
	const [limpiar, setLimpiar] = useState(false);

	const handleChange = (event) => {
		setActivo(event.target.value);
	};

	const onSubmit = async (values) => {
		values.active = activo;
		values.id_coordinadors = coordinadors;
		setLoading(true);
		setTimeout(async () => {
			const response = await postRequest('/area', values);
			if (response.status) {
				setChange(!change);
				setLoading(false);
				formik.resetForm();
				setCoordinadors([]);
			} else {
				setServerError(response.message);
				setChange(!change);
				setLoading(false);
			}
		}, 1000);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '35px'}}>
			{loading ? (
				<Grid justifyContent={'center'} alignContent={'center'} display={'flex'}>
					<CircularProgress />
				</Grid>
			) : (
				<form onSubmit={formik.handleSubmit}>
					<Card
						sx={{
							maxHeight: '80vh',
							overflowY: 'auto',
							boxShadow: 'none',
							marginBottom: '30px',
						}}>
						<CardContent
							sx={{maxHeight: '100vh', width: '100%'}}
							style={{marginBotton: '250px'}}>
							<Typography variant='h3' sx={{display: 'inline-flex'}}>
								Agregar
								<Box
									sx={{
										color: theme.palette.secondary.dark,
										marginLeft: '5px',
									}}>
									Areas
								</Box>
							</Typography>
							<div style={{paddingTop: '35px'}}>
								<Box
									sx={{
										'& .MuiTextField-root': {mb: 3},
									}}
									noValidate
									autoComplete='off'>
									<Grid container spacing={2}>
										<Grid item xs={12} sm={12}>
											<TextField
												sx={{marginBottom: '15px'}}
												label='Nombre'
												fullWidth
												name='name'
												variant='outlined'
												value={formik.values.name}
												onChange={formik.handleChange}
												onBlur={formik.handleBlur}
												error={
													formik.touched.name &&
													Boolean(formik.errors.name)
												}
												helperText={
													formik.touched.name && formik.errors.name
												}
											/>
										</Grid>
									</Grid>

									<AutoCompleteComponent
										url={'api/v1/coordinador'}
										labelText={'Seleccionar Coordinador'}
										setValues={setCoordinadors}
										campoIterar={'person_full_name'}
										campoDisable={'coordinador_active'}
									/>

									<FormControl fullWidth sx={{marginTop: '10px'}}>
										<InputLabel id='demo-simple-select-label'>
											Activo
										</InputLabel>
										<Select
											labelId='demo-simple-select-label'
											id='demo-simple-select'
											label='Activo'
											value={activo}
											onChange={handleChange}
											onBlur={formik.handleBlur}>
											<MenuItem value={1}>Si</MenuItem>
											<MenuItem value={0}>No</MenuItem>
										</Select>
									</FormControl>

									{serverError && (
										<Alert
											sx={{
												marginY: '20px',
											}}
											severity='error'>
											{serverError}
										</Alert>
									)}
								</Box>
							</div>
						</CardContent>
					</Card>
					<Grid item display={'flex'} sx={{marginLeft: matchXS ? '0' : '20px'}}>
						<Button
							color='secondary'
							variant='contained'
							type='submit'
							endIcon={<AddIcon />}>
							Agregar
						</Button>
						<Button
							sx={{ml: 2}}
							color='error'
							onClick={() => {
								setLimpiar(!limpiar);
								formik.resetForm();
							}}
							endIcon={<DeleteIcon />}>
							Cancelar
						</Button>
					</Grid>
				</form>
			)}
		</div>
	);
};

AddArea.propTypes = {
	change: PropTypes.bool,
	setChange: PropTypes.func,
};
