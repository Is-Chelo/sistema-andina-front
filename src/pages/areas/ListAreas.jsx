import {Backdrop, Box, Fade, Grid, Modal, Typography} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useEffect, useState} from 'react';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {deleteRequest, getRequest} from '../../api/Requests';
import Edit from './Edit';
import Swal from 'sweetalert2';
import {AddArea} from './AddArea';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import {CustomButtons} from '../../components/CustomButtons';
import {columnsRenderCoodinador} from './columnsRenderCoodinador';

export const ListAreas = () => {
	// MODAL VER
	const [open, setOpen] = useState(false);
	const handleClose = () => setOpen(false);
	const [coodinatorsData, setCoodinatorsData] = useState([]);

	const [change, setChange] = useState(false);
	const [changePermissions, setChangePermissions] = useState(false);
	const [edit, setEdit] = useState(false);
	const [initialValues, setInitialValues] = useState({});
	const data = useFetch('api/v1/area', change);

	// PERMISOS
	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/area');
				setChange(!change);
			}
		});
	};

	const handleEdit = async (id) => {
		setEdit(false);
		const datos = await getRequest(`api/v1/area/${id}`);
		setEdit(true);
		setInitialValues({
			name: datos.name,
			active: datos.active ? 1 : 0,
			id: id,
			id_coodinadors:datos.coordinadors
		});
	};
	const handlerShow = async (id) => {
		const datos = await getRequest(`api/v1/area/coodinadors/${id}`);
		setCoodinatorsData(datos);
	};

	const columns = columnsRender(handleEdit, handleDelete, handlerShow, setOpen, dataPermission);
	const columnsCoodinador = columnsRenderCoodinador();

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud title='Areas' parrafo='Listado de Areas.' category={'Gestión académica'} />
			</div>

			<Grid container spacing={2}>
				<Grid item xs={12} md={7} sm={12} lg={dataPermission?.ok_insert || edit ? 8 : 12}>
					<div className='animate__animated animate__fadeIn'>
						<MainCard>
							<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
								{data?.status === false ? (
									<></>
								) : (
									<>
										{data !== undefined ? (
											<DataGrid
												rows={data}
												columns={columns}
												pageSize={5}
												rowsPerPageOptions={[5]}
												slots={{
													toolbar: CustomButtons,
												}}
												checkboxSelection
												disableRowSelectionOnClick
												unstable_cellSelection
												experimentalFeatures={{clipboardPaste: true}}
												unstable_ignoreValueFormatterDuringExport
												// FIlters
												slotProps={{
													toolbar: {
														showQuickFilter: true,
														quickFilterProps: {debounceMs: 500},
														url: '/api/v1/area/reporte',
														name:'reporte-areas'
													},
												}}
												disableColumnFilter
												disableColumnSelector
												disableDensitySelector
											/>
										) : (
											<SkeletonLoading />
										)}
									</>
								)}
							</div>
						</MainCard>
					</div>
				</Grid>
				<Grid item xs={12} sm={12} md={5} lg={4}>
					{dataPermission?.ok_insert && dataPermission?.ok_update ? (
						<div className='animate__animated animate__fadeIn'>
							<MainCard>
								{edit ? (
									<Edit
										change={change}
										setChange={setChange}
										initialValues={initialValues}
										setEdit={setEdit}
									/>
								) : (
									<AddArea change={change} setChange={setChange} />
								)}
							</MainCard>
						</div>
					) : dataPermission?.ok_insert && dataPermission?.ok_update === false ? (
						<div className='animate__animated animate__fadeInRight'>
							<MainCard>
								<AddArea change={change} setChange={setChange} />
							</MainCard>
						</div>
					) : dataPermission?.ok_update && dataPermission?.ok_insert === false ? (
						edit && (
							<div className='animate__animated animate__fadeInRight'>
								<MainCard sm={4}>
									{edit ? (
										<Edit
											change={change}
											setChange={setChange}
											initialValues={initialValues}
											setEdit={setEdit}
										/>
									) : (
										<> Recuerde que su rol solo tiene permisos para editar</>
									)}
								</MainCard>
							</div>
						)
					) : (
						<></>
					)}
				</Grid>
			</Grid>
			<div>
				<Modal
					aria-labelledby='transition-modal-title'
					aria-describedby='transition-modal-description'
					open={open}
					onClose={handleClose}
					closeAfterTransition
					slots={{backdrop: Backdrop}}
					slotProps={{
						backdrop: {
							timeout: 500,
						},
					}}>
					<Fade in={open}>
						<Box
							sx={{
								position: 'absolute',
								top: '50%',
								left: '50%',
								transform: 'translate(-50%, -50%)',
								width: '30vw',
								height: '50vh',
								bgcolor: 'background.paper',
								boxShadow: 24,
								borderRadius: '10px',
								p: 4,
							}}>
							<Typography id='transition-modal-title' variant='h4' component='h2'>
								Coordinadores
							</Typography>

							<DataGrid
								rows={coodinatorsData}
								columns={columnsCoodinador}
								pageSize={5}
								rowsPerPageOptions={[5]}
							/>
						</Box>
					</Fade>
				</Modal>
			</div>
		</MainLayout>
	);
};
