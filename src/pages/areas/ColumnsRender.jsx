import {Button, Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';

export const columnsRender = (
	handleEdit,
	handleDelete,
	handlerShow,
	setShowModal,
	dataPermission
) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 100},
		{
			field: 'name',
			headerName: 'Nombre del Area',
			description: 'Nombre del Area',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${params.row.name || ''} `,
		},

		{
			field: 'created',
			headerName: 'Fecha de Creación',
			description: 'Fecha de Creación',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${params.row.created || ''} `,
		},

		{
			field: 'active',
			headerName: 'Habilitado',
			width: 180,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
		{
			field: 'total_coordinadores',
			headerName: 'Coordinadores',
			width: 150,
			renderCell: (params) => (
				<Button
					onClick={() => {
						handlerShow(params.id);
						setShowModal(true);
					}}
					startIcon={<RemoveRedEyeIcon />}>
					{' '}
					ver
				</Button>
			),
		},

		accionOperations,
	];
};
