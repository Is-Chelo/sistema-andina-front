import {Chip} from '@mui/material';

export const columnsRenderCoodinador = () => {
	return [
		{field: 'index', headerName: 'Nro', width: 60},

		{
			field: 'person_full_name',
			headerName: 'Nombre del Coodinador',
			description: 'Nombre del Coodinador',
			sortable: false,
			width: 190,
			valueGetter: (params) => `${params.row.person_full_name || ''} `,
		},
		{
			field: 'person_ci_number',
			headerName: 'Doc. Identidad',
			description: 'Documento de Identidad',
			sortable: false,
			width: 130,
			valueGetter: (params) => `${params.row.person_ci_number || ''} `,
		},

		{
			field: 'active',
			headerName: 'Habilitado',
			width: 130,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.coordinador_active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.coordinador_active
								? '#b9f6ca60'
								: '#fbe9e7',
							color: params.row.coordinador_active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
	];
};
