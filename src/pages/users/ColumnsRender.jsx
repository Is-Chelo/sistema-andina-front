import { IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';

export const columnsRender = (handleEdit, handleDelete, dataPermission) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 100},
		{
			field: 'fullName',
			headerName: 'Nombre Completo',
			description: 'Nombre Completo del Docente',
			sortable: false,
			width: 180,
			valueGetter: (params) =>
				`${params.row.user_name || ''} ${params.row.user_last_name || ''}`,
		},
		{field: 'user_username', headerName: 'Username', width: 100},

		// { field: 'person_address', headerName: 'Dirección', width: 150 },
		{field: 'user_email', headerName: 'Email', width: 150, description: 'Correo electrónico'},
		{field: 'user_cellphone', headerName: 'Número de Celular', width: 180},
		{
			field: 'user_date_birth',
			headerName: 'Fecha de Nacimiento',
			description: 'Fecha de Nacimiento',
			sortable: false,
			width: 180,
			valueGetter: (params) =>
				`${moment(params.row.user_date_birth).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'ci',
			headerName: 'Carnet de Identidad',
			description: 'Carnet de Identidad el docente',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${params.row.user_ci_number || ''}`,
		},

		{
			field: 'rol_name',
			headerName: 'Rol',
			width: 100,
			// renderCell: (params) => {
			// 	return (
			// 		<Chip
			// 			label={params.row.rol_name}
			// 			sx={{backgroundColor: '#ffc107', color: '#fff'}}
			// 			size='small'
			// 		/>
			// 	);
			// },
		},
		accionOperations,
	];
};
