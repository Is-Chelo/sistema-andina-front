/* eslint-disable react/prop-types */
import {
	Alert,
	Box,
	Button,
	Card,
	CardContent,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {updateRequest} from '../../api/Requests';
import DeleteIcon from '@mui/icons-material/Delete';

import {AutoCompleteSimple} from '../../components/AutoCompleteSimple';
import {useState} from 'react';

const validationFields = Yup.object({
	date_init: Yup.string().required('Este campo es requerido.'),
	date_end: Yup.string().required('Este campo es requerido.'),
	contract_number: Yup.string().required('Este campo es requerido.'),
});

export default function Edit({change, setChange, setState, initialValues}) {
	const [activo, setActivo] = useState(initialValues.active);

	const [idTeacher, serIdTeacher] = useState(initialValues.teacher_id.id);

	const [serverError, setServerError] = useState('');

	const handleChange = (event) => {
		initialValues.active = event.target.value;
		setActivo(event.target.value);
	};

	const onSubmit = async (values) => {
		values.teacher_id = idTeacher;
		const response = await updateRequest(initialValues.id, '/teacher-contract', values);
		if (response.status) {
			setChange(!change);
			setState(false);
		} else setServerError(response.message);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '55px'}}>
			<form onSubmit={formik.handleSubmit}>
				<Card
					sx={{
						maxHeight: '80vh',
						overflowY: 'auto',
						boxShadow: 'none',
						marginBottom: '50px',
						display: 'flex',
					}}>
					<CardContent sx={{maxHeight: '100vh'}} style={{marginBotton: '250px'}}>
						<Typography variant='h3'>Editar Contrato</Typography>
						<div style={{paddingTop: '35px'}}>
							<Box
								sx={{
									'& .MuiTextField-root': {mb: 3},
									width: '430px',
								}}
								noValidate
								autoComplete='off'>
								<Grid item xs={12} sm={6} sx={{marginBottom: '10px'}}>
									<TextField
										fullWidth
										label='Número de Contrato'
										name='contract_number'
										variant='outlined'
										value={formik.values.contract_number}
										onChange={formik.handleChange}
										onBlur={formik.handleBlur}
										error={
											formik.touched.contract_number &&
											Boolean(formik.errors.contract_number)
										}
										helperText={
											formik.touched.contract_number &&
											formik.errors.contract_number
										}
									/>
								</Grid>

								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={12}>
										<AutoCompleteSimple
											url={'api/v1/teacher'}
											setValues={serIdTeacher}
											campoIterar={'name'}
											campoDisable={'active'}
											labelText='Seleccionar Docente'
											valuesEdit={initialValues.teacher_id}
										/>
									</Grid>
								</Grid>

								<Grid container spacing={2} sx={{marginBottom: '15px'}}>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Fecha de Inicio'
											name='date_init'
											fullWidth
											type='date'
											variant='outlined'
											InputLabelProps={{
												shrink: true,
												placeholder: 'Selecciona una fecha',
											}}
											value={formik.values.date_init}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.date_init &&
												Boolean(formik.errors.date_init)
											}
											helperText={
												formik.touched.date_init && formik.errors.date_init
											}
										/>
									</Grid>
									<Grid item xs={12} sm={6}>
										<TextField
											label='Fecha de Finalización'
											name='date_end'
											fullWidth
											type='date'
											variant='outlined'
											InputLabelProps={{
												shrink: true,
												placeholder: 'Selecciona una fecha',
											}}
											value={formik.values.date_end}
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											error={
												formik.touched.date_end &&
												Boolean(formik.errors.date_end)
											}
											helperText={
												formik.touched.date_end && formik.errors.date_end
											}
										/>
									</Grid>
								</Grid>

								<FormControl fullWidth>
									<InputLabel id='demo-simple-select-label'>Activo</InputLabel>
									<Select
										labelId='demo-simple-select-label'
										id='demo-simple-select'
										label='Activo'
										value={activo}
										onChange={handleChange}
										onBlur={formik.handleBlur}>
										<MenuItem value={1}>Si</MenuItem>
										<MenuItem value={0}>No</MenuItem>
									</Select>
								</FormControl>

								{serverError && (
									<Alert
										sx={{
											marginY: '20px',
										}}
										severity='error'>
										{serverError}
									</Alert>
								)}
							</Box>
						</div>
					</CardContent>
				</Card>
				<Box
					sx={{
						height: '60px',
						position: 'absolute',
						marginLeft: '18px',
						bottom: 0,
					}}>
					<Button color='primary' variant='contained' type='submit' endIcon={<AddIcon />}>
						Agregar
					</Button>
					<Button
						color='error'
						variant='text'
						onClick={() => {
							setState(false);
						}}
						endIcon={<DeleteIcon />}>
						Cancelar
					</Button>
				</Box>
			</form>
		</div>
	);
}
