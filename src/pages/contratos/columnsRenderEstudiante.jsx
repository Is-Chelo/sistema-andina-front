import {Chip} from '@mui/material';

export const columnsRenderEstudiante = () => {
	return [
		{field: 'index', headerName: 'Nro', width: 60},

		{
			field: 'person_full_name',
			headerName: 'Nombre del Estudiante',
			description: 'Nombre del Estudiante',
			sortable: false,
			width: 190,
			valueGetter: (params) => `${params.row.person_full_name || ''} `,
		},
		{
			field: 'person_ci_number',
			headerName: 'Doc. Identidad',
			description: 'Documento de Identidad',
			sortable: false,
			width: 130,
			valueGetter: (params) => `${params.row.person_ci_number || ''} `,
		},
		{
			field: 'person_cellphone',
			headerName: 'Número de Celular',
			description: 'Número de Celular',
			sortable: false,
			width: 130,
			valueGetter: (params) => `${params.row.person_cellphone || ''} `,
		},

		{
			field: 'active',
			headerName: 'Habilitado',
			width: 130,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.student_active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.student_active
								? '#b9f6ca60'
								: '#fbe9e7',
							color: params.row.student_active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
	];
};
