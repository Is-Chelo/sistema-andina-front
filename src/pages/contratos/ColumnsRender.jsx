import {Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';

export const columnsRender = (handleEdit, handleDelete, dataPermission) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 50},
		{
			field: 'teacher_full_name',
			headerName: 'Nombre del Docente',
			description: 'Nombre del Docente',
			width: 180,
		},
		{
			field: 'contract_number',
			headerName: 'Número de contracto',
			description: 'Número de contracto',
			width: 180,
		},
		{
			field: 'date_init',
			headerName: 'Fecha de Inicio',
			description: 'Fecha de Inicio',
			width: 200,
			valueGetter: (params) => `${moment(params.row.date_init).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'date_end',
			headerName: 'Fecha de Finalizacion',
			description: 'Fecha de Finalizacion',
			width: 200,
			valueGetter: (params) => `${moment(params.row.date_end).format('YYYY-MM-DD') || ''}`,
		},

		{
			field: 'active',
			headerName: 'Activo',
			width: 100,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},

		accionOperations,
	];
};
