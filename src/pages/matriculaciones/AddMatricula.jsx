import {
	Box,
	Button,
	Card,
	CardContent,
	Grid,
	Typography,
	Alert,
	CircularProgress,
	useTheme,
} from '@mui/material';
import PropTypes from 'prop-types';
import AddIcon from '@mui/icons-material/Add';
import React, {useEffect, useState} from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {postRequest} from '../../api/Requests';
import DeleteIcon from '@mui/icons-material/Delete';
import {AutoCompleteSimple} from '../../components/AutoCompleteSimple';

const initialValues = {
	id_program: '',
	id_student: '',
};

const validationFields = Yup.object({});

export const AddMatricula = ({change, setChange}) => {
	const theme = useTheme();
	const [serverError, setServerError] = React.useState('');
	const [loading, setLoading] = useState(false);
	const [limpiar, setLimpiar] = useState(false);
	// Relations
	const [idProgram, setIdProgram] = useState(null);
	const [idStudent, setIdStudent] = useState(null);

	useEffect(() => {
		initialValues.id_program = idProgram;
		initialValues.id_student = idStudent;
	}, [idProgram, idStudent]);

	const onSubmit = async (values) => {
		values.id_program = idProgram;
		values.id_student = idStudent;
		setLoading(true);
		setTimeout(async () => {
			const response = await postRequest('/matricula', values);
			if (response.status) {
				setChange(!change);
				setLoading(false);
				setIdProgram(null);
				setIdStudent(null);
				// formik.resetForm();
				setServerError('');
			} else {
				setServerError(response.message);
				setChange(!change);
				setLoading(false);
			}
		}, 1000);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '35px'}}>
			{loading ? (
				<Grid justifyContent={'center'} alignContent={'center'} display={'flex'}>
					<CircularProgress />
				</Grid>
			) : (
				<form onSubmit={formik.handleSubmit}>
					<Card
						sx={{
							maxHeight: '80vh',
							overflowY: 'auto',
							boxShadow: 'none',
							marginBottom: '30px',
						}}>
						<CardContent
							sx={{maxHeight: '100vh', width: '100%'}}
							style={{marginBotton: '250px'}}>
							<Typography variant='h3' sx={{display: 'inline-flex'}}>
								Agregar
								<Box
									sx={{
										color: theme.palette.secondary.dark,
										marginLeft: '5px',
									}}>
									Matriculación
								</Box>
							</Typography>
							<div style={{paddingTop: '35px'}}>
								<Box
									sx={{
										'& .MuiTextField-root': {mb: 3},
									}}
									noValidate
									autoComplete='off'>
									<Grid container spacing={2}>
										<Grid item xs={12} sm={12} lg={12}>
											<AutoCompleteSimple
												url={'api/v1/program'}
												setValues={setIdProgram}
												campoIterar={'name'}
												campoDisable={'active'}
												labelText='Seleccionar Programa'
												limpiar={limpiar}
											/>
										</Grid>
										<Grid item xs={12} sm={12} lg={12}>
											<AutoCompleteSimple
												url={'api/v1/student'}
												setValues={setIdStudent}
												campoIterar={'person_full_name'}
												campoDisable={'active'}
												labelText='Seleccionar estudiante'
												limpiar={limpiar}
											/>
										</Grid>
									</Grid>

									{serverError && (
										<Alert
											sx={{
												marginY: '20px',
											}}
											severity='error'>
											{serverError}
										</Alert>
									)}
								</Box>
							</div>
						</CardContent>
					</Card>
					<Box
						sx={{
							height: '50px',
							marginLeft: '18px',
						}}>
						<Button
							disabled={idProgram === null || idStudent === null ? true : false}
							color='secondary'
							variant='contained'
							type='submit'
							endIcon={<AddIcon />}>
							Agregar
						</Button>
						<Button
							sx={{ml: 2}}
							color='error'
							onClick={() => {
								setLimpiar(!limpiar);
								formik.resetForm();
							}}
							endIcon={<DeleteIcon />}>
							Cancelar
						</Button>
					</Box>
				</form>
			)}
		</div>
	);
};

AddMatricula.propTypes = {
	change: PropTypes.bool,
	setChange: PropTypes.func,
};
