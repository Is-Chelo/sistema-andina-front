import {Grid} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useEffect, useState} from 'react';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {deleteRequest, getRequest} from '../../api/Requests';
import Edit from './Edit';
import Swal from 'sweetalert2';
import {AddMatricula} from './AddMatricula';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useAuthStore} from '../../store';
import {CustomButtons} from '../../components/CustomButtons';

export const ListMatriculas = () => {
	const [change, setChange] = useState(false);
	const [changePermissions, setChangePermissions] = useState(false);
	const [edit, setEdit] = useState(false);
	const [initialValues, setInitialValues] = useState({});
	const data = useFetch('api/v1/matricula', change);

	// PERMISOS
	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/matricula');
				setChange(!change);
			}
		});
	};

	const handleEdit = async (id) => {
		setEdit(false);
		const datos = await getRequest(`api/v1/matricula/${id}`);

		setEdit(true);
		setInitialValues({
			id: id,
			id_program: {
				id: datos.program_id,
				name: datos.program_name,
			},
			id_student: {
				id: datos.student_id,
				person_full_name: datos.student_full_name,
			},
		});
	};

	const columns = columnsRender(handleEdit, handleDelete, dataPermission);

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud
					title='Matriculación de Estudiantes'
					parrafo='Vista para controlar la matriculaciones de los estudiantes.'
					category={'Gestión académica'}
				/>
			</div>

			<Grid container spacing={2}>
				<Grid item xs={12} sm={dataPermission?.ok_insert || edit ? 8 : 12}>
					<div className='animate__animated animate__fadeIn'>
						<MainCard>
							<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
								{data?.status === false ? (
									<></>
								) : (
									<>
										{data !== undefined ? (
											<DataGrid
												rows={data}
												columns={columns}
												pageSize={1}
												rowsPerPageOptions={[5]}
												localeText={{
													noRowsLabel: 'No se ha encontrado datos.',
													noResultsOverlayLabel:
														'No se ha encontrado ningún resultado',
													toolbarColumns: 'Columnas',
													toolbarColumnsLabel: 'Seleccionar columnas',
													toolbarFilters: 'Filtros',
													toolbarFiltersLabel: 'Ver filtros',
													toolbarFiltersTooltipHide: 'Quitar filtros',
													toolbarFiltersTooltipShow: 'Ver filtros',
												}}
												slots={{
													toolbar: CustomButtons,
												}}
												checkboxSelection
												disableRowSelectionOnClick
												unstable_cellSelection
												experimentalFeatures={{clipboardPaste: true}}
												unstable_ignoreValueFormatterDuringExport
												// FIlters
												slotProps={{
													toolbar: {
														showQuickFilter: true,
														quickFilterProps: {debounceMs: 500},
														url: '/api/v1/matricula/reporte',
														name:'reporte-matriculados'

													},
												}}
												disableColumnFilter
												disableColumnSelector
												disableDensitySelector
											/>
										) : (
											<SkeletonLoading />
										)}
									</>
								)}
							</div>
						</MainCard>
					</div>
				</Grid>
				<Grid item xs={12} sm={4}>
					{dataPermission?.ok_insert && dataPermission?.ok_update ? (
						<div className='animate__animated animate__fadeIn'>
							<MainCard>
								{edit ? (
									<Edit
										change={change}
										setChange={setChange}
										initialValues={initialValues}
										setEdit={setEdit}
									/>
								) : (
									<AddMatricula change={change} setChange={setChange} />
								)}
							</MainCard>
						</div>
					) : dataPermission?.ok_insert && dataPermission?.ok_update === false ? (
						<div className='animate__animated animate__fadeInRight'>
							<MainCard>
								<AddMatricula change={change} setChange={setChange} />
							</MainCard>
						</div>
					) : dataPermission?.ok_update && dataPermission?.ok_insert === false ? (
						edit && (
							<div className='animate__animated animate__fadeInRight'>
								<MainCard sm={4}>
									{edit ? (
										<Edit
											change={change}
											setChange={setChange}
											initialValues={initialValues}
											setEdit={setEdit}
										/>
									) : (
										<> Recuerde que su rol solo tiene permisos para editar</>
									)}
								</MainCard>
							</div>
						)
					) : (
						<></>
					)}
				</Grid>
			</Grid>
		</MainLayout>
	);
};
