import {IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';

export const columnsRender = (handleEdit, handleDelete, dataPermission) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 100},

		{
			field: 'student_full_name',
			headerName: 'Nombre del Estudiante',
			description: 'Nombre completo del estudiante',
			width: 250,
		},
		{
			field: 'program_name',
			headerName: 'Nombre del Programa',
			description: 'Nombre del Programa',
			width: 200,
		},
		{field: 'matricula_gestion', headerName: 'Gestión', description: 'Gestión', width: 200},

		accionOperations,
	];
};
