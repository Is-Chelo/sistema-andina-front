/* eslint-disable react/prop-types */
import {
	Alert,
	Box,
	Button,
	Card,
	CardContent,
	CircularProgress,
	Grid,
	Typography,
} from '@mui/material';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {updateRequest} from '../../api/Requests';
import {useEffect, useState} from 'react';
import SaveIcon from '@mui/icons-material/Save';
import {useTheme} from '@mui/material/styles';

import DeleteIcon from '@mui/icons-material/Delete';
import {AutoCompleteSimple} from '../../components/AutoCompleteSimple';

const validationFields = Yup.object({});

export default function Edit({change, setChange, initialValues, setEdit}) {
	const theme = useTheme();
	const [loading, setLoading] = useState(false);
	const [serverError, setServerError] = useState('');

	const [idProgram, setIdProgram] = useState(initialValues.id_program.id);
	const [idStudent, setIdStudent] = useState(initialValues.id_student.id);

	useEffect(() => {
		// initialValues.id_program = idProgram;
		// initialValues.id_student = idStudent;
	}, [idProgram, idStudent, initialValues]);

	const onSubmit = async (values) => {
		values.id_program = idProgram;
		values.id_student = idStudent;
		setLoading(true);
		setTimeout(async () => {
			const response = await updateRequest(initialValues.id, '/matricula', values);
			if (response.status) {
				setChange(!change);
				setEdit(false);
			} else setServerError(response.message);
			setLoading(false);
		}, 1000);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validationFields,
		onSubmit: onSubmit,
	});

	return (
		<div style={{maxHeight: '100vh', width: '100%', padding: '20px', paddingTop: '35px'}}>
			{loading ? (
				<Grid justifyContent={'center'} alignContent={'center'} display={'flex'}>
					<CircularProgress />
				</Grid>
			) : (
				<form onSubmit={formik.handleSubmit}>
					<Card
						sx={{
							maxHeight: '80vh',
							overflowY: 'auto',
							boxShadow: 'none',
							marginBottom: '30px',
						}}>
						<CardContent
							sx={{maxHeight: '100vh', width: '100%'}}
							style={{marginBotton: '250px'}}>
							<Typography variant='h3' sx={{display: 'inline-flex'}}>
								Editar
								<Box
									sx={{
										color: theme.palette.secondary.dark,
										marginLeft: '5px',
									}}>
									Asignatura
								</Box>
							</Typography>
							<div style={{paddingTop: '35px'}}>
								<Box
									sx={{
										'& .MuiTextField-root': {mb: 3},
									}}
									noValidate
									autoComplete='off'>
									<Grid container spacing={2}>
										<Grid item xs={12} sm={12} lg={12}>
											{/* <FieldSelect
												url='api/v1/program?active=true'
												setValue={setIdProgram}
												formik={formik}
												name={'Programa'}
												dataValue={'id'}
												dataLabel={'name'}
												idEdit={initialValues.id_program}
											/> */}

											<AutoCompleteSimple
												url={'api/v1/program'}
												setValues={setIdProgram}
												campoIterar={'name'}
												campoDisable={'active'}
												labelText='Seleccionar Programa'
												valuesEdit={initialValues.id_program}
											/>
										</Grid>
										<Grid item xs={12} sm={12} lg={12}>
											{/* <FieldSelect
												url='api/v1/student?active=true'
												setValue={setIdStudent}
												formik={formik}
												name={'Estudiante'}
												dataValue={'id'}
												dataLabel={'person_full_name'}
												idEdit={initialValues.id_student}
											/> */}
											<AutoCompleteSimple
												url={'api/v1/student'}
												setValues={setIdStudent}
												campoIterar={'person_full_name'}
												campoDisable={'active'}
												labelText='Seleccionar Estudiante'
												valuesEdit={initialValues.id_student}
											/>
										</Grid>
									</Grid>

									{serverError && (
										<Alert
											sx={{
												marginY: '20px',
											}}
											severity='error'>
											{serverError}
										</Alert>
									)}
								</Box>
							</div>
						</CardContent>
					</Card>
					<Box
						sx={{
							height: '50px',
							marginLeft: '18px',
						}}>
						<Button
							disabled={idProgram === null || idStudent === null ? true : false}
							color='secondary'
							variant='contained'
							type='submit'
							endIcon={<SaveIcon />}>
							Guardar
						</Button>
						<Button
							sx={{
								marginLeft: '10px',
							}}
							color='error'
							variant='text'
							endIcon={<DeleteIcon />}
							onClick={() => {
								setEdit(false);
							}}>
							{' '}
							Cancelar
						</Button>
					</Box>
				</form>
			)}
		</div>
	);
}
