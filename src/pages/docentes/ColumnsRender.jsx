import {Chip, IconButton} from '@mui/material';
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from 'moment';

export const columnsRender = (handleEdit, handleDelete, dataPermission) => {
	let accionOperations;
	if (!dataPermission?.ok_delete && !dataPermission?.ok_update) accionOperations = {hide: true};
	else
		accionOperations = {
			field: 'actions',
			headerName: 'Acciones',
			width: 150,
			renderCell: (params) => (
				<div>
					{dataPermission?.ok_update && (
						<IconButton
							disabled={dataPermission?.ok_update ? false : true}
							aria-label='add'
							color='secondary'
							onClick={() => handleEdit(params.id)}>
							<CreateOutlinedIcon />
						</IconButton>
					)}

					{dataPermission?.ok_delete && (
						<IconButton
							disabled={dataPermission?.ok_delete ? false : true}
							aria-label='add'
							color='error'
							onClick={() => handleDelete(params.id)}>
							<DeleteIcon />
						</IconButton>
					)}
				</div>
			),
		};
	return [
		{field: 'index', headerName: 'Nro', width: 100},
		{
			field: 'fullName',
			headerName: 'Nombre Completo',
			description: 'Nombre Completo del Docente',
			sortable: false,
			width: 180,
			valueGetter: (params) =>
				`${params.row.person_name || ''} ${params.row.person_last_name || ''}`,
		},
		// { field: 'person_address', headerName: 'Dirección', width: 150 },
		{field: 'person_email', headerName: 'Email', width: 150, description: 'Correo electrónico'},
		{field: 'person_cellphone', headerName: 'Número de Celular', width: 180},
		{
			field: 'person_date_birth',
			headerName: 'Fecha de Nacimiento',
			description: 'Fecha de Nacimiento',
			sortable: false,
			width: 180,
			valueGetter: (params) =>
				`${moment(params.row.person_date_birth).format('YYYY-MM-DD') || ''}`,
		},
		{
			field: 'ci',
			headerName: 'Carnet de Identidad',
			description: 'Carnet de Identidad el docente',
			sortable: false,
			width: 180,
			valueGetter: (params) => `${params.row.person_ci_number || ''}`,
		},
		{
			field: 'teacher_active',
			headerName: 'Activo',
			width: 150,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.teacher_active ? 'Activo' : 'Inactivo'}
						sx={{
							backgroundColor: params.row.teacher_active ? '#b9f6ca60' : '#fbe9e7',
							color: params.row.teacher_active ? '#00c853' : '#d84315',
						}}
						size='small'
					/>
				);
			},
		},
		{
			field: 'teacher_type_rol',
			headerName: 'Rol',
			width: 100,
			renderCell: (params) => {
				return (
					<Chip
						label={params.row.teacher_type_rol}
						sx={{backgroundColor: '#ffc107', color: '#fff'}}
						size='small'
					/>
				);
			},
		},
		accionOperations,
	];
};
