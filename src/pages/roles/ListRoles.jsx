import {Grid} from '@mui/material';
import CardCrud from '../../components/CardCrud';
import {MainLayout} from '../../layouts/MainLayout';
import MainCard from '../../components/MainCard';
import {useEffect, useState} from 'react';
import {useFetch} from '../../hooks/useFetch';
import {DataGrid} from '@mui/x-data-grid';
import {columnsRender} from './ColumnsRender';
import {deleteRequest, getRequest} from '../../api/Requests';
import Edit from './Edit';
import Swal from 'sweetalert2';
import {AddRole} from './AddRole';
import {SkeletonLoading} from '../../components/SkeletonLoading';
import {useNavigate} from 'react-router-dom';
import {useAuthStore} from '../../store';
import {CustomButtons} from '../../components/CustomButtons';

export const ListRoles = () => {
	const [change, setChange] = useState(false);
	const [edit, setEdit] = useState(false);
	const [initialValues, setInitialValues] = useState({});
	const [changePermissions, setChangePermissions] = useState(false);

	const navigate = useNavigate();
	const data = useFetch('api/v1/role', change);
	const handleDelete = async (id) => {
		Swal.fire({
			title: 'Esta seguro de eliminar este registro?',
			icon: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#f44336',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar',
			customClass: {
				title: 'my-title',
				confirmButton: 'my-confirm-button',
				cancelButton: 'my-cancel-button',
			},
			background: '#fff',
			padding: '10px',
			width: '300px',
		}).then(async (result) => {
			if (result.isConfirmed) {
				await deleteRequest(id, '/Role');
				setChange(!change);
			}
		});
	};
	// PERMISOS
	const idModuleMenu = useAuthStore.getState().id_menu;
	const dataPermission = useFetch(
		`api/v1/role-module/valid-permission/${idModuleMenu}`,
		changePermissions
	);
	useEffect(() => {
		setChangePermissions(!changePermissions);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [idModuleMenu]);

	const handleEdit = async (id) => {
		setEdit(false);
		const datos = await getRequest(`api/v1/role/${id}`);
		setEdit(true);
		setInitialValues({
			name: datos.name,
			active: datos.active ? 1 : 0,
			id: id,
		});
	};
	const handleShowRolePermission = (id) => {
		navigate(`/roles-permission/${id}`);
	};
	const columns = columnsRender(
		handleEdit,
		handleDelete,
		handleShowRolePermission,
		dataPermission
	);

	return (
		<MainLayout>
			<div className='animate__animated animate__fadeIn'>
				<CardCrud title='Roles' parrafo='Listado de Roles.' category={'Permisos'} />
			</div>

			<Grid container spacing={2}>
				<Grid item xs={12} md={7} sm={12} lg={dataPermission?.ok_insert || edit ? 8 : 12}>
					<MainCard>
						<div style={{height: 500, width: '100%', borderStyle: 'none'}}>
							{data?.status === false ? (
								<></>
							) : (
								<>
									{data !== undefined ? (
										<DataGrid
											rows={data}
											columns={columns}
											pageSize={5}
											rowsPerPageOptions={[5]}
											slots={{
												toolbar: CustomButtons,
											}}
											checkboxSelection
											disableRowSelectionOnClick
											unstable_cellSelection
											experimentalFeatures={{clipboardPaste: true}}
											unstable_ignoreValueFormatterDuringExport
											// FIlters
											slotProps={{
												toolbar: {
													showQuickFilter: true,
													quickFilterProps: {debounceMs: 500},
													url: '/api/v1/role/reporte',
													name:'reporte-roles'

												},
											}}
											disableColumnFilter
											disableColumnSelector
											disableDensitySelector
										/>
									) : (
										<SkeletonLoading />
									)}
								</>
							)}
						</div>
					</MainCard>
				</Grid>
				<Grid item xs={12} sm={12} md={5} lg={4}>
					{dataPermission?.ok_insert && dataPermission?.ok_update ? (
						<div className='animate__animated animate__fadeIn'>
							<MainCard>
								{edit ? (
									<Edit
										change={change}
										setChange={setChange}
										initialValues={initialValues}
										setEdit={setEdit}
									/>
								) : (
									<AddRole change={change} setChange={setChange} />
								)}
							</MainCard>
						</div>
					) : dataPermission?.ok_insert && dataPermission?.ok_update === false ? (
						<div className='animate__animated animate__fadeInRight'>
							<MainCard>
								<AddRole change={change} setChange={setChange} />
							</MainCard>
						</div>
					) : dataPermission?.ok_update && dataPermission?.ok_insert === false ? (
						edit && (
							<div className='animate__animated animate__fadeInRight'>
								<MainCard sm={4}>
									{edit ? (
										<Edit
											change={change}
											setChange={setChange}
											initialValues={initialValues}
											setEdit={setEdit}
										/>
									) : (
										<> Recuerde que su rol solo tiene permisos para editar</>
									)}
								</MainCard>
							</div>
						)
					) : (
						<></>
					)}
				</Grid>
			</Grid>
		</MainLayout>
	);
};
