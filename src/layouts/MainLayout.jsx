import PropTypes from 'prop-types';
import {useState} from 'react';
import {Outlet} from 'react-router-dom';
import {AppBar, Box, CssBaseline, Toolbar} from '@mui/material';
import {Header} from './Header/Header';
import Sidebar from './Sidebar/Sidebar';
import {styled, useTheme} from '@mui/material/styles';
import {borderRadius, drawerWidth} from '../config';

const Main = styled('main', {shouldForwardProp: (prop) => prop !== 'open'})(({theme, open}) => ({
	...theme.typography.mainContent,
	...(!open && {
		borderBottomLeftRadius: 0,
		borderBottomRightRadius: 0,
		borderRadius: 10,
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		[theme.breakpoints.up('md')]: {
			marginLeft: -(drawerWidth - 20),
			width: `calc(100% - ${drawerWidth}px)`,
		},
		[theme.breakpoints.down('md')]: {
			marginLeft: '20px',
			width: `calc(100% - ${drawerWidth}px)`,
			padding: '16px',
		},
		[theme.breakpoints.down('sm')]: {
			marginLeft: '10px',
			width: `calc(100% - ${drawerWidth}px)`,
			padding: '16px',
			marginRight: '10px',
		},
	}),
	...(open && {
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
		borderBottomLeftRadius: 0,
		borderBottomRightRadius: 0,
		borderRadius: borderRadius,
		width: `calc(100% - ${drawerWidth}px)`,
		[theme.breakpoints.down('md')]: {
			marginLeft: '20px',
		},
		[theme.breakpoints.down('sm')]: {
			marginLeft: '10px',
		},
	}),
}));

export const MainLayout = ({children}) => {
	const theme = useTheme();

	const [drawerOpen, setDrawerOpen] = useState(true);

	const toggleDrawer = () => {
		setDrawerOpen(!drawerOpen);
	};

	return (
		<Box sx={{display: 'flex'}}>
			<CssBaseline />
			{/* header */}
			<AppBar
				enableColorOnDark
				position='fixed'
				color='inherit'
				elevation={0}
				sx={{
					bgcolor: theme.palette.background.default,
					transition: drawerOpen ? theme.transitions.create('width') : 'none',
				}}>
				<Toolbar>
					<Header handleLeftDrawerToggle={toggleDrawer} />
				</Toolbar>
			</AppBar>

			<Sidebar drawerOpen={drawerOpen} handleToggleDrawer={toggleDrawer} />
			<Main theme={theme} open={drawerOpen}>
				{/* <Breadcrumbs
					separator={IconChevronRight}
					navigation={navigation}
					icon
					title
					rightAlign
				/> */}
				{children}
				<Outlet />
			</Main>
			{/* <Customization /> */}
		</Box>
	);
};

MainLayout.propTypes = {
	children: PropTypes.node.isRequired,
};
