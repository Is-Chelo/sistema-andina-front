import PropTypes from 'prop-types';

// material-ui
import {useTheme} from '@mui/material/styles';
import {Box, Drawer, useMediaQuery} from '@mui/material';

import Logo from '../../components/Logo';
import MenuList from './MenuList';

// ==============================|| SIDEBAR DRAWER ||============================== //

const Sidebar = ({drawerOpen, handleToggleDrawer}) => {
	const theme = useTheme();
	const matchUpMd = useMediaQuery(theme.breakpoints.up('md'));

	const drawer = (
		<>
			<Box sx={{display: {xs: 'block', md: 'none'}}}>
				<Box sx={{display: 'flex', p: 2, mx: 'auto'}}>
					<Logo />
				</Box>
			</Box>
			<Box sx={{px: 2}}>
				<MenuList />
				<Box sx={{height: 100}} />
			</Box>
		</>
	);

	return (
		<Box
			component='nav'
			sx={{flexShrink: {md: 0}, width: matchUpMd ? '260px' : 'auto'}}
			aria-label='mailbox folders'>
			<Drawer
				variant={matchUpMd ? 'persistent' : 'temporary'}
				anchor='left'
				open={drawerOpen}
				onClose={handleToggleDrawer}
				sx={{
					'& .MuiDrawer-paper': {
						width: '260px',
						background: theme.palette.background.default,
						color: theme.palette.text.primary,
						borderRight: 'none',
						[theme.breakpoints.up('md')]: {
							top: '88px',
						},
					},
				}}
				ModalProps={{keepMounted: true}}
				color='inherit'>
				{drawer}
			</Drawer>
		</Box>
	);
};

Sidebar.propTypes = {
	drawerOpen: PropTypes.bool,
	handleToggleDrawer: PropTypes.func,
};

export default Sidebar;
