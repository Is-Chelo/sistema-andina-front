import PropTypes from 'prop-types';
import {forwardRef, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';

// material-ui
import {useTheme} from '@mui/material/styles';
import {Avatar, Chip, ListItemButton, ListItemIcon, ListItemText, Typography} from '@mui/material';

// assets
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import {borderRadius} from '../../config';
import {useAuthStore} from '../../store';

// ==============================|| SIDEBAR MENU LIST ITEMS ||============================== //

const NavItem = ({item, level}) => {
	const theme = useTheme();

	const [isSelected, setIsSelected] = useState(); // estado para controlar si el ítem está seleccionado o no

	const Icon = item.icon;
	const itemIcon = item?.icon ? (
		<Icon stroke={1.5} size='1.3rem' />
	) : (
		<FiberManualRecordIcon
			sx={
				{
					// width: customization.isOpen.findIndex((id) => id === item?.id) > -1 ? 8 : 6,
					// height: customization.isOpen.findIndex((id) => id === item?.id) > -1 ? 8 : 6,
				}
			}
			fontSize={level > 0 ? 'inherit' : 'medium'}
		/>
	);

	let itemTarget = '_self';
	if (item.target) {
		itemTarget = '_blank';
	}

	let listItemProps = {
		// eslint-disable-next-line react/display-name
		component: forwardRef((props, ref) => (
			<Link ref={ref} {...props} to={item.url} target={itemTarget} />
		)),
	};

	if (item?.external) {
		listItemProps = {component: 'a', href: item.url, target: itemTarget};
	}
	const setIdMenu = useAuthStore((state) => state.setIdMenu);

	// active menu item on page load
	useEffect(() => {
		const currentIndex = document.location.pathname
			.toString()
			.split('/')
			.findIndex((id) => id === item.id);
		if (currentIndex > -1) {
			setIsSelected(item.id);
			setIdMenu(item.id_module);
		}
	}, [item.id, item.id_module]);

	//
	return (
		<ListItemButton
			id={item.id_module}
			{...listItemProps}
			disabled={item.disabled}
			sx={{
				borderRadius: `${borderRadius}px`,
				mb: 0.5,
				alignItems: 'flex-start',
				py: level > 1 ? 1 : 1.25,
				pl: `${level * 24}px`,
				...(isSelected && {
					backgroundColor: ` ${theme.palette.secondary.light} !important`,
				}),
			}}
			selected={isSelected ? true : false}>
			<ListItemIcon
				sx={{
					my: 'auto',
					minWidth: !item?.icon ? 18 : 36,
					color: isSelected ? theme.palette.secondary.dark : 'inherit',
				}}>
				{itemIcon}
			</ListItemIcon>
			<ListItemText
				primary={
					<Typography
						// variant={
						// 	customization.isOpen.findIndex((id) => id === item.id) > -1
						// 		? 'h5'
						// 		: 'body1'
						// }
						variant='h5'
						sx={{
							color: isSelected ? theme.palette.secondary.dark : 'inherit',
						}}>
						{item.title}
					</Typography>
				}
				secondary={
					item.caption && (
						<Typography
							variant='caption'
							sx={{...theme.typography.subMenuCaption}}
							display='block'
							gutterBottom>
							{item.caption}
						</Typography>
					)
				}
			/>
			{item.chip && (
				<Chip
					color={item.chip.color}
					variant={item.chip.variant}
					size={item.chip.size}
					label={item.chip.label}
					avatar={item.chip.avatar && <Avatar>{item.chip.avatar}</Avatar>}
				/>
			)}
		</ListItemButton>
	);
};

NavItem.propTypes = {
	item: PropTypes.object,
	level: PropTypes.number,
};

export default NavItem;
