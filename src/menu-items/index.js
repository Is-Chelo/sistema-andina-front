// import dashboard from './dashboard';
import settings from './system/settings';
import users from './system/user';
import permissions from './system/permissions';
import dashboard from './dashboard';

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
	items: [dashboard, settings, users, permissions],
};

export default menuItems;
