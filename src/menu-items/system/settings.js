import * as TablerIcons from '@tabler/icons-react';
import {useAuthStore} from '../../store';

// constant
const icons = {
	...TablerIcons,
};

// ====
const menu = useAuthStore.getState().menu;
const settings = {
	id: 'settings',
	title: 'Gestión académica',
	type: 'group',
	// children: [
	// 	{
	// 		id: 'areas',
	// 		title: 'Areas',
	// 		type: 'item',
	// 		url: '/settings/areas',
	// 		icon: icons.IconBoxPadding,
	// 		breadcrumbs: false,
	// 	},
	// 	{
	// 		id: 'matriculaciones',
	// 		title: 'Matriculaciones',
	// 		type: 'item',
	// 		url: '/settings/matriculaciones',
	// 		icon: icons.IconNotebook,
	// 		breadcrumbs: false,
	// 	},
	// 	{
	// 		id: 'programas',
	// 		title: 'Programas',
	// 		type: 'item',
	// 		url: '/settings/programas',
	// 		icon: icons.IconFileCode2,
	// 		breadcrumbs: false,
	// 	},
	// 	{
	// 		id: 'asignaturas',
	// 		title: 'Asignaturas',
	// 		type: 'item',
	// 		url: '/settings/programas',
	// 		icon: icons.IconFileCode2,
	// 		breadcrumbs: false,
	// 	},

	// ],
	children: menu
		.filter((item) => item.type === 'settings')
		.map((item) => {
			return {
				id: item.name.toLowerCase(),
				id_module: item.id,
				title: item.name,
				type: 'item',
				url: item.path_front,
				icon: icons[item['icon']],
				breadcrumbs: false,
			};
		}),
};

export default settings;
