// assets
import * as TablerIcons from '@tabler/icons-react';
import {useAuthStore} from '../../store';

// constant
const icons = {
	...TablerIcons,
};

// ====
const menu = useAuthStore.getState().menu;

const permissions = {
	id: 'permissions',
	title: 'Permisos',
	type: 'group',
	// children: [
	// 	{
	// 		id: 'roles',
	// 		title: 'Roles',
	// 		type: 'item',
	// 		url: '/roles',
	// 		icon: icons.IconTool,
	// 		breadcrumbs: false,
	// 	},
	// 	{
	// 		id: 'permission',
	// 		title: 'Permissions',
	// 		type: 'item',
	// 		url: '/permissions',
	// 		icon: icons.IconShieldLock,
	// 		breadcrumbs: false,
	// 	},
	// ],
	children: menu
		.filter((item) => item.type === 'permissions')
		.map((item) => {
			// console.log(item);
			return {
				id: item.name.toLowerCase(),
				id_module: item.id,
				title: item.name,
				type: 'item',
				url: item.path_front,
				icon: icons[item['icon']],
				breadcrumbs: false,
			};
		}),
};

export default permissions;
