// assets
import * as TablerIcons from '@tabler/icons-react';

import {useAuthStore} from '../../store';

// constant
const icons = {
	...TablerIcons,
};
const menu = useAuthStore.getState().menu;

const users = {
	id: 'users',
	title: 'Usuarios',
	type: 'group',
	children: menu
		.filter((item) => item.type === 'users')
		.map((item) => {
			return {
				id: item.name.toLowerCase(),
				id_module: item.id,
				title: item.name,
				type: 'item',
				url: item.path_front,
				icon: icons[item['icon']],
				breadcrumbs: false,
			};
		}),
};

export default users;
