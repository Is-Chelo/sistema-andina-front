import * as TablerIcons from '@tabler/icons-react';
import {useAuthStore} from '../store';

// constant
const icons = {
	...TablerIcons,
};
// ==============================|| DASHBOARD MENU ITEMS ||============================== //
const menu = useAuthStore.getState().menu;

const dashboard = {
	id: 'dashboard',
	title: 'Inicio',
	type: 'group',
	children: menu
		.filter((item) => item.type === 'home')
		.map((item) => {
			return {
				id: item.name.toLowerCase(),
				id_module: item.id,
				title: item.name,
				type: 'item',
				url: item.path_front,
				icon: icons[item['icon']],
				breadcrumbs: false,
			};
		}),
};

export default dashboard;
