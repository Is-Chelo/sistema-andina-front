import {Navigate, Route, Routes} from 'react-router-dom';
import AuthRoutes from '../auth/routes/AuthRoutes';
import {AppTheme} from '../theme/AppTheme';
import {ToastContainer} from 'react-toastify';
import {Dashboard} from '../pages/Dashboard';
import {ListDocentes} from '../pages/docentes/ListDocentes';
import {ListEstudiantes} from '../pages/estudiantes/ListEstudiantes';
import {ListCoordinadors} from '../pages/coordinadores/ListCoordinador';
import {ListAreas} from '../pages/areas/ListAreas';
import {ListRoles} from '../pages/roles/ListRoles';
import {RolePermission} from '../pages/role-permission/RolePermission';
import {ListPermisos} from '../pages/permissions/ListPermisos';
import {ProtectedRoute} from './ProtectedRoute';
import {useAuthStore} from '../store';
import {ListProgramas} from '../pages/programas/ListProgramas';
import {ListAsignaturas} from '../pages/asignaturas/ListAsignaturas';
import {ListMatriculas} from '../pages/matriculaciones/ListMatriculas';
import {RenderByRol} from '../pages/calificaciones/RenderByRol';
import {RenderKardexByRol} from '../pages/kardex/RenderByRol';
import {ListUser} from '../pages/users/ListUser';
import {RecoverPasswordPage} from '../auth/pages/RecoverPasswordPage';
import {SendCorreoPage} from '../auth/pages/SendCorreoPage';
import {ListFechaEntregaNotas} from '../pages/fechaEntregaNotas/ListFechaEntregaNotas';
import { ListContratos } from '../pages/contratos/ListContratos';

const AppRouter = () => {
	const isAuth = useAuthStore.getState().isAuth;
	return (
		<AppTheme>
			<ToastContainer />
			<Routes>
				<Route path='/' element={<Navigate to='/auth/login' />} />
				<Route path='/auth/*' element={<AuthRoutes />} />
				<Route path='/new-password/:id/:token' element={<RecoverPasswordPage />} />
				<Route path='/recover-password' element={<SendCorreoPage />} />

				<Route element={<ProtectedRoute isAllowed={isAuth} />}>
					<Route path='/dashboard/*' element={<Dashboard />} />
					<Route path='/docentes' element={<ListDocentes />} />
					<Route path='/estudiantes' element={<ListEstudiantes />} />
					<Route path='/coordinadores' element={<ListCoordinadors />} />
					<Route path='/areas' element={<ListAreas />} />
					<Route path='/roles' element={<ListRoles />} />
					<Route path='/roles-permission/:id' element={<RolePermission />} />
					<Route path='/permissions' element={<ListPermisos />} />
					<Route path='/programas' element={<ListProgramas />} />
					<Route path='/asignaturas' element={<ListAsignaturas />} />
					<Route path='/matriculaciones' element={<ListMatriculas />} />
					<Route path='/calificaciones' element={<RenderByRol />} />
					<Route path='/kardex' element={<RenderKardexByRol />} />
					<Route path='/users' element={<ListUser />} />
					<Route path='/fecha-entrega-de-notas' element={<ListFechaEntregaNotas />} />
					<Route path='/contratos-docentes' element={<ListContratos />} />
				</Route>
			</Routes>
		</AppTheme>
	);
};

export default AppRouter;
