import {useEffect, useState} from 'react';
import {
	FormControl,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	OutlinedInput,
	Select,
} from '@mui/material';
import PropTypes from 'prop-types';
import {useFetch} from '../hooks/useFetch';
import {getRequest} from '../api/Requests';

export const SelectDepend = ({setId1, setId2, name, name2, url, urlDependency, valor1, valor2, valor3, valor4}) => {
	// const url = 'api/v1/grades-student/programs-all?active=true';
	// const url2 = `api/v1/grades-student/subjects-by-program-other/${idProgram}`;
	const [idProgram, setIdProgram] = useState(0);
	const [idSubject, setIdSubject] = useState(0);
	const [dataModule, setDataModule] = useState([]);
	const [secondDataModule, setSecondDataModule] = useState([]);

	const data = useFetch(url);

	const [url2, setUrl2] = useState('');

	const handleChangeProgram = (event) => {
		if (event.target.value !== '') {
			setIdProgram(event.target.value);
			setUrl2(`${urlDependency}/${event.target.value}`);
		}
		setId2(0);
		setIdSubject(0);
	};

	const handleChangeSubject = (event) => {
		if (event.target.value !== '') {
			setIdSubject(event.target.value);
		}
		setId2(event.target.value);
	};

	const getServiceDepends = async (endpoint) => {
		if (endpoint !== `${urlDependency}/0`) {
			const datos = await getRequest(endpoint);
			setSecondDataModule(datos);
		} else {
			setSecondDataModule([]);
		}
		setId1(idProgram);
		setId2(idSubject);
	};

	useEffect(() => {
		setDataModule(data);
		getServiceDepends(url2);
	}, [data, idProgram, url2]);

	return (
		<>
			<Grid container spacing={2}>
				<Grid item xs={12} sm={12} lg={6}>
					<FormControl fullWidth sx={{mb: 2}}>
						<InputLabel id='demo-multiple-name-label'>Nombre del {name}</InputLabel>
						<Select
							labelId={`id_${name.toLowerCase()}`}
							id={`id_${name.toLowerCase()}`}
							label={`Nombre del ${name}`}
							value={idProgram}
							name={`id_${name.toLowerCase()}`}
							onChange={handleChangeProgram}
							input={<OutlinedInput label={`Nombre del ${name}`} />}>
							<MenuItem value={0}>Seleccion un registro</MenuItem>
							{dataModule &&
								dataModule.map((moduleDb) => (
									<MenuItem
										key={`key_${moduleDb[valor1]}`}
										value={moduleDb[valor1]}>
										{moduleDb[valor2]}
									</MenuItem>
								))}
						</Select>

						{idProgram < 1 && (
							<FormHelperText sx={{mt: 0, color: '#ff9800'}}>
								Este campo es requerido
							</FormHelperText>
						)}
					</FormControl>
				</Grid>
				<Grid item xs={12} sm={12} lg={6}>
					{secondDataModule && idProgram > 0 ? (
						<FormControl fullWidth sx={{mb: 2}}>
							<InputLabel id='demo-multiple-name-label'>
								Seleccione {name2}
							</InputLabel>
							<Select
								labelId={`id_${name2.toLowerCase()}`}
								id={`id_${name2.toLowerCase()}`}
								label={`Seleccione ${name2}`}
								value={idSubject}
								name={`id_${name2.toLowerCase()}`}
								onChange={handleChangeSubject}
								input={<OutlinedInput label={`Seleccione ${name2}`} />}>
								<MenuItem value={0}>Seleccion un registro</MenuItem>
								{secondDataModule &&
									secondDataModule.map((moduleDb) => (
										<MenuItem
											key={`key_${moduleDb[valor3]}`}
											value={moduleDb[valor3]}>
											{moduleDb[valor4]}
										</MenuItem>
									))}
							</Select>
							{idProgram < 1 && (
								<FormHelperText sx={{mt: 0, color: '#ff9800'}}>
									Este campo es requerido
								</FormHelperText>
							)}
						</FormControl>
					) : (
						<FormControl fullWidth sx={{mb: 2}} disabled>
							<InputLabel id='demo-simple-select-disabled-label'>
								Seleccione {name2}
							</InputLabel>
							<Select
								labelId={`Seleccione ${name2}`}
								id='demo-simple-select-disabled'
								value={0}
								label={`Seleccione ${name2}`}>
								<MenuItem value={0}>
									<em>Seleccione {name2}</em>
								</MenuItem>
							</Select>
							<FormHelperText sx={{mt: 0, color: '#ff9800'}}>
								Primero debe seleccionar un programa
							</FormHelperText>
						</FormControl>
					)}
				</Grid>
			</Grid>
		</>
	);
};

SelectDepend.propTypes = {
	setId1: PropTypes.func,
	setId2: PropTypes.func,
	name: PropTypes.string,
	name2: PropTypes.string,
	url: PropTypes.string,
	urlDependency: PropTypes.string,
	valor1: PropTypes.string,
	valor2: PropTypes.string,
	valor3: PropTypes.string,
	valor4: PropTypes.string,
};
