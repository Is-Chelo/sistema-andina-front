import {Autocomplete, TextField} from '@mui/material';
import {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {getRequest} from '../api/Requests';

export const AutoCompleteSimple = ({
	url,
	labelText,
	setValues,
	campoIterar,
	campoDisable,
	valuesEdit,
	limpiar,
}) => {
	const [error, setError] = useState(false);
	const [value, setValue] = useState(valuesEdit || null);
	const [data, setData] = useState([]);

	useEffect(() => {
		getData();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [limpiar]);

	const getData = async () => {
		const datos = await getRequest(url);
		setData(datos);
	};

	const onBlur = () => {
		setError(!value);
	};

	const handleChange = (event, newValue) => {
		if (newValue !== null) {
			setValue(newValue);
			setValues(newValue.id || '');
		} else {
			setError(!newValue);
			setValue(null);
			setValues(null);
		}
	};

	return (
		<div style={{borderRadius: '10px'}}>
			<Autocomplete
				id='filter-demo'
				onChange={handleChange}
				options={data}
				value={value}
				getOptionDisabled={(option) => !option[campoDisable]}
				isOptionEqualToValue={(option, value) => option.id === value?.id}
				getOptionLabel={(option) => option[campoIterar]}
				renderInput={(params) => (
					<TextField
						{...params}
						fullWidth
						label={labelText}
						placeholder={labelText}
						onBlur={onBlur}
						error={error}
						helperText={error && 'Este campo es requerido.'}
					/>
				)}
			/>
		</div>
	);
};

AutoCompleteSimple.propTypes = {
	url: PropTypes.string,
	labelText: PropTypes.string,
	setValues: PropTypes.func,
	campoIterar: PropTypes.string,
	campoDisable: PropTypes.string,
	limpiar: PropTypes.bool,
	valuesEdit: PropTypes.any,
};
