import {Card, Chip, Grid, Typography} from '@mui/material';
import Carousel from 'react-material-ui-carousel';
import {borderRadius} from '../config';
import {getRequests} from '../api/Requests';
import {useEffect, useState} from 'react';
import moment from 'moment';

export const CarouselComponent = () => {
	const [dataReques, setDataReques] = useState([]);

	useEffect(() => {
		getDatos();
	}, []);

	const getDatos = async () => {
		const data = await getRequests('api/v1/program/program-dash');
		const items = Object.values(data.data.data);
		const chunkSize = 3;

		const datosTransform = items.reduce((accumulator, item, index) => {
			const chunkIndex = Math.floor(index / chunkSize);
			if (!accumulator[chunkIndex]) {
				accumulator[chunkIndex] = {cards: []};
			}
			accumulator[chunkIndex].cards.push({
				area_name: item.area_name,
				date_end: item.date_end,
				date_init: item.date_init,
				id: item.id,
				name: item.name,
				sede: item.sede,
				version: item.version,
			});
			return accumulator;
		}, []);

		setDataReques(datosTransform);
	};
	return (
		<Carousel>
			{dataReques?.map((item, index) => (
				<Grid container spacing={2} key={index}>
					{item.cards.map((card, cardIndex) => (
						<Grid item xs={12} md={4} sm={6} key={cardIndex}>
							<Card
								sx={{
									padding: '20px',
									boxShadow: 'none',
									borderRadius: `${borderRadius}px`,
								}}>
								<Typography variant='body2'>{card.area_name}</Typography>
								<Typography variant='h4' sx={{marginTop: '5px'}}>
									{card.name}
								</Typography>
								<div>
									<Chip
										sx={{marginRight: '5px'}}
										label={'Sede: ' + card.sede}
										color='primary'
										style={{marginTop: '15px'}}
									/>
									<Chip
										style={{marginTop: '15px'}}
										color='secondary'
										label={
											'Inicio: ' + moment(card.date_init).format('YYYY-MM-DD')
										}
										sx={{marginRight: '5px'}}
									/>
									<Chip
										style={{marginTop: '15px'}}
										label={'Fin: ' + moment(card.date_end).format('YYYY-MM-DD')}
									/>
								</div>
							</Card>
						</Grid>
					))}
				</Grid>
			))}
		</Carousel>
	);
};
