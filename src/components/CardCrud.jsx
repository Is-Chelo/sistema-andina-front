import {Card, Grid, Typography} from '@mui/material';
import PropTypes from 'prop-types';

import HomeIcon from '@mui/icons-material/Home';
import {borderRadius} from '../config';
import {useNavigate} from 'react-router-dom';

const CardCrud = ({title, parrafo, category, root = '/dashboard'}) => {
	const navigate = useNavigate();
	const handleNavigation = () => {
		navigate(root);
	};
	return (
		<div style={{height: 120, width: '100%'}}>
			<Card
				sx={{
					padding: '20px',
					boxShadow: 'none',
					borderRadius: `${borderRadius}px`,
				}}>
				<Grid container spacing={0} alignItems='center'>
					<Grid item lg={10} md={9} xs={12} sm={12}>
						<Typography variant='h2'>{title}</Typography>
						<Typography variant='subtitle2' mt={1}>
							{parrafo}
						</Typography>
					</Grid>
					<Grid
						item
						lg={2}
						md={3}
						xs={12}
						sm={12}
						sx={{
							display: 'flex',
							justifyContent: 'end',
							alignItems: 'center',
						}}>
						<HomeIcon
							color='secondary'
							onClick={() => {
								handleNavigation();
							}}
							aria-label='add'
							sx={{
								marginRight: '8px',
								fontSize: '20px',
								cursor: 'pointer', // Agregar esta línea
							}}
						/>

						<Typography
							sx={{
								marginRight: '8px',
								fontSize: '12px',
							}}
							variant='body2'>
							{' '}
							/
						</Typography>
						<Typography
							sx={{
								marginRight: '8px',
								fontSize: '12px',
							}}
							variant='body2'>
							{category}
						</Typography>
						<Typography
							sx={{
								marginRight: '8px',
								fontSize: '12px',
							}}
							variant='body2'>
							{' '}
							/
						</Typography>
						<Typography
							sx={{
								marginRight: '8px',
								fontSize: '12px',
							}}
							variant='body2'>
							{title}
						</Typography>
					</Grid>
				</Grid>
			</Card>
		</div>
	);
};

CardCrud.propTypes = {
	title: PropTypes.string,
	parrafo: PropTypes.string,
	category: PropTypes.string,
	root: PropTypes.string,
};
export default CardCrud;
