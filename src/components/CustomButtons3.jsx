import PropTypes from 'prop-types';
import {Grid, IconButton, Tooltip, Typography, useMediaQuery} from '@mui/material';

import {
	GridToolbarContainer,
	// GridToolbarQuickFilter,
	gridExpandedSortedRowIdsSelector,
	useGridApiContext,
} from '@mui/x-data-grid';
import BrowserUpdatedIcon from '@mui/icons-material/BrowserUpdated';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import {useTheme} from '@emotion/react';
import {getPDF} from '../api/Requests';

const getFilteredRows = ({apiRef}) => gridExpandedSortedRowIdsSelector(apiRef);

export const CustomButtons3 = ({url, name = 'reporte', nameView}) => {
	const apiRef = useGridApiContext();

	const handleExport = (options) => apiRef.current.exportDataAsCsv(options);

	const handlePrint = async () => {
		await getPDF(`${url}`, name);
	};
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));

	return (
		<>
			<GridToolbarContainer sx={{padding: '5px'}}>
				<Grid
					container
					spacing={0}
					alignItems='center'
					justifyContent={matchXS ? 'center' : 'space-between'}>
					<Grid
						item
						lg={9}
						md={6}
						xs={12}
						sm={5}
						sx={{
							display: matchXS ? 'flex' : '',
							justifyContent: matchXS ? 'center' : 'start',
						}}>
						{/* <GridToolbarQuickFilter placeholder='Buscar' /> */}
						<Typography variant='h4'>{nameView}</Typography>
					</Grid>
					<Grid
						item
						lg={3}
						md={4}
						xs={7}
						sm={7}
						display='flex'
						alignItems='center'
						justifyContent='end'
						sx={{
							marginTop: matchXS ? '10px' : '0',
						}}>
						<Tooltip title={'Descargar CSV'} sx={{marginRight: '22px'}}>
							<IconButton
								color='secondary'
								onClick={() => handleExport({getRowsToExport: getFilteredRows})}>
								<BrowserUpdatedIcon fontSize='inherit' />
							</IconButton>
						</Tooltip>
						<Tooltip title={'Imprimir Reporte'} sx={{marginRight: '22px'}}>
							<IconButton
								color='secondary'
								onClick={() => handlePrint({getRowsToExport: getFilteredRows})}>
								<LocalPrintshopOutlinedIcon fontSize='inherit' />
							</IconButton>
						</Tooltip>
					</Grid>
				</Grid>
			</GridToolbarContainer>
		</>
	);
};

CustomButtons3.propTypes = {
	url: PropTypes.string,
	name: PropTypes.string,
	nameView: PropTypes.string,
};
