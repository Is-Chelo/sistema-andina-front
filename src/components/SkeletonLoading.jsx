import {Skeleton} from '@mui/material';

import {Table, TableBody, TableCell, TableContainer, TableRow} from '@mui/material';

export const SkeletonLoading = () => {
	return (
		<TableContainer sx={{padding: '10px'}}>
			<Table>
				<TableBody>
					{Array.from(Array(5).keys()).map((row, index) => (
						<TableRow key={index}>
							<TableCell sx={{border: 'none'}}>
								<Skeleton variant='text' />
							</TableCell>
							<TableCell sx={{border: 'none'}}>
								<Skeleton variant='text' />
							</TableCell>
							<TableCell sx={{border: 'none'}}>
								<Skeleton variant='text' />
							</TableCell>
							<TableCell sx={{border: 'none'}}>
								<Skeleton variant='text' />
							</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};
