import PropTypes from 'prop-types';
import {CircularProgress, Fab, Grid, IconButton, Tooltip, useMediaQuery} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

import {
	GridToolbarContainer,
	GridToolbarQuickFilter,
	gridExpandedSortedRowIdsSelector,
	useGridApiContext,
} from '@mui/x-data-grid';
import BrowserUpdatedIcon from '@mui/icons-material/BrowserUpdated';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import {useTheme} from '@emotion/react';
import {getPDF} from '../api/Requests';
import {useState} from 'react';

const getFilteredRows = ({apiRef}) => gridExpandedSortedRowIdsSelector(apiRef);

export const CustomButtons2 = ({url, toggleDrawer, okInsert, name}) => {
	const apiRef = useGridApiContext();
	const handleExport = (options) => apiRef.current.exportDataAsCsv(options);
	const [buttonPrintDisable, setbuttonPrintDisable] = useState(false);
	const handlePrint = async () => {
		try {
			setbuttonPrintDisable(true);
			await getPDF(`${url}`, name);
		} catch (error) {
			throw new Error(error);
		} finally {
			setbuttonPrintDisable(false);
		}
	};
	const theme = useTheme();
	const matchXS = useMediaQuery(theme.breakpoints.only('xs'));
	return (
		<>
			<GridToolbarContainer sx={{padding: '15px'}}>
				<Grid container spacing={0} alignItems='center' justifyContent='center'>
					<Grid
						item
						lg={9}
						md={6}
						xs={12}
						sm={5}
						sx={{
							display: matchXS ? 'flex' : '',
							justifyContent: matchXS ? 'center' : 'start',
						}}>
						<GridToolbarQuickFilter placeholder='Buscar' />
					</Grid>
					<Grid
						item
						lg={3}
						md={6}
						xs={12}
						sm={7}
						display='flex'
						alignItems='center'
						justifyContent='end'
						sx={{
							marginTop: matchXS ? '10px' : '0',
						}}>
						<Tooltip title={'Descargar CSV'} sx={{marginRight: '22px'}}>
							<IconButton
								color='secondary'
								onClick={() => handleExport({getRowsToExport: getFilteredRows})}>
								<BrowserUpdatedIcon fontSize='inherit' />
							</IconButton>
						</Tooltip>
						<Tooltip title={'Imprimir Reporte'} sx={{marginRight: '22px'}}>
							<IconButton
								color='secondary'
								onClick={() => handlePrint({getRowsToExport: getFilteredRows})}>
								{buttonPrintDisable ? (
									<CircularProgress size={18} />
								) : (
									<LocalPrintshopOutlinedIcon fontSize='inherit' />
								)}
							</IconButton>
						</Tooltip>
						{okInsert && (
							<Tooltip
								sx={{marginLeft: '8px'}}
								title={'Agregar'}
								disableTouchListener
								placement='top'>
								<Fab
									disabled={!okInsert}
									size='small'
									onClick={toggleDrawer(true)}
									color='primary'
									aria-label='add'>
									<AddIcon />
								</Fab>
							</Tooltip>
						)}
					</Grid>
				</Grid>
			</GridToolbarContainer>
		</>
	);
};

CustomButtons2.propTypes = {
	url: PropTypes.string,
	toggleDrawer: PropTypes.func,
	okInsert: PropTypes.bool,
	name: PropTypes.string,
};
