import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import {useEffect, useState} from 'react';
import {Chip} from '@mui/material';
import {getRequest} from '../api/Requests';
import PropTypes from 'prop-types';

export default function AutoCompleteComponent({
	url,
	labelText,
	setValues,
	campoIterar,
	campoDisable,
	valuesEdit,
}) {
	const [error, setError] = useState(false);

	const [value, setValue] = useState(valuesEdit || []);
	const [data, setData] = useState([]);

	const handleChange = (event, newValue) => {
		const onlyIDs = [];
		for (let i = 0; i < newValue.length; i++) {
			onlyIDs.push(newValue[i].id);
		}
		setValue([...newValue]);
		setValues([...onlyIDs]);
		if (onlyIDs.length === 0) setError(true);
		else setError(false);
	};
	const onBlur = () => {
		if (value.length === 0) setError(true);
		else setError(false);
	};

	useEffect(() => {
		getData();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	const getData = async () => {
		const datos = await getRequest(url);
		setData(datos);
	};
	return (
		<Autocomplete
			multiple
			onChange={handleChange}
			options={data}
			value={value}
			getOptionDisabled={(option) => option[campoDisable] === false}
			getOptionLabel={(option) => option[campoIterar]}
			renderTags={(tagValue, getTagProps) =>
				tagValue.map((option, index) => (
					<Chip key={index} label={option[campoIterar]} {...getTagProps({index})} />
				))
			}
			renderInput={(params) => (
				<TextField
					{...params}
					fullWidth
					label={labelText}
					placeholder={labelText}
					onBlur={onBlur}
					error={error}
					helperText={error && 'Este campo es requerido.'}
				/>
			)}
		/>
	);
}

AutoCompleteComponent.propTypes = {
	url: PropTypes.string,
	labelText: PropTypes.string,
	setValues: PropTypes.func,
	campoIterar: PropTypes.string,
	campoDisable: PropTypes.string,
	limpiar: PropTypes.bool,
	valuesEdit: PropTypes.array | PropTypes.object,
};
