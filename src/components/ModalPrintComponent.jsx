import {IconButton, Tooltip, Typography} from '@mui/material';
import PropTypes from 'prop-types';
import {getPDF} from '../api/Requests';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';

export const ModalPrintComponent = ({name, url}) => {
	const handlePrint = async () => {
		await getPDF(`${url}`, name);
	};
	return (
		<div>
			<Typography id='transition-modal-title' variant='h4' component='h2'>
				{name}
			</Typography>
			<Tooltip title={'Imprimir Reporte'} sx={{marginRight: '22px'}}>
				<IconButton color='secondary' onClick={() => handlePrint()}>
					<LocalPrintshopOutlinedIcon fontSize='inherit' />
				</IconButton>
			</Tooltip>
		</div>
	);
};
ModalPrintComponent.propTypes = {
	name: PropTypes.string,
	url: PropTypes.string,
};
