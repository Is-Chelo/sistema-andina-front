import PropTypes from 'prop-types';
import {useEffect, useState} from 'react';
import {useFetch} from '../hooks/useFetch';
import {
	FormControl,
	FormHelperText,
	InputLabel,
	MenuItem,
	OutlinedInput,
	Select,
} from '@mui/material';

export const FieldSelect = ({
	url,
	setValue,
	name,
	dataValue,
	dataLabel,
	formik,
	idEdit,
	limpiar,
}) => {
	const [idValue, setIdValue] = useState(idEdit);
	const [dataModule, setDataModule] = useState([]);

	const data = useFetch(url);
	useEffect(() => {
		setDataModule(data);
		setIdValue(0);
	}, [data, limpiar]);

	const handleChangeProgram = (event) => {
		if (event.target.value !== '') {
			setValue(event.target.value);
			setIdValue(event.target.value);
		}
	};
	
	return (
		<div>
			<FormControl fullWidth sx={{mb: 2}}>
				<InputLabel id='demo-multiple-name-label'>Nombre del {name}</InputLabel>
				<Select
					labelId={`id_${name.toLowerCase()}`}
					id={`id_${name.toLowerCase()}`}
					label={`Nombre del ${name}`}
					value={idEdit > 0 ? idEdit : idValue}
					name={`id_${name.toLowerCase()}`}
					onChange={handleChangeProgram}
					onBlur={formik.handleBlur}
					input={<OutlinedInput label={`Nombre del ${name}`} />}>
					<MenuItem value={0}>Seleccion un registro</MenuItem>
					{dataModule &&
						dataModule.map((moduleDb) => (
							<MenuItem key={moduleDb[dataValue]} value={moduleDb[dataValue]}>
								{moduleDb[dataLabel]}
							</MenuItem>
						))}
				</Select>

				{idValue < 1 && (
					<FormHelperText sx={{mt: 0, color: '#ff9800'}}>
						Este campo es requerido
					</FormHelperText>
				)}
			</FormControl>
		</div>
	);
};

FieldSelect.propTypes = {
	url: PropTypes.string,
	error: PropTypes.string,
	dataValue: PropTypes.string,
	dataLabel: PropTypes.string,
	setValue: PropTypes.func,
	name: PropTypes.string,
	formik: PropTypes.object,
	idEdit: PropTypes.number,
	limpiar: PropTypes.bool,
};
