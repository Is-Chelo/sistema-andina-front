import {useParams} from 'react-router-dom';
import {PasswordRecoverLayout} from '../layout/PasswordRecoverLayout';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {
	Box,
	Button,
	FormControl,
	FormHelperText,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
} from '@mui/material';
import {useTheme} from '@emotion/react';
import {useState} from 'react';
import {Visibility, VisibilityOff} from '@mui/icons-material';
import {postRequest} from '../../api/Requests';

const SignInSchema = Yup.object().shape({
	password: Yup.string().min(6).required('Por favor introduce la contraseña nueva'),
});

const initialValues = {
	password: '',
};

export const RecoverPasswordPage = () => {
	const theme = useTheme();
	const [showPassword, setShowPassword] = useState(false);
	const handleClickShowPassword = () => {
		setShowPassword(!showPassword);
	};
	const {id, token} = useParams();

	const onSubmit = async () => {
		const password = document.getElementById('password').value;

		const response = await postRequest('/auth/reset-password', {
			password: password,
			id,
			token: token.split('+').join('.'),
		});
		setTimeout(() => {
			if (response) window.location = '/auth/login';
		}, 1500);
	};
	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: SignInSchema,
		onSubmit: onSubmit,
	});
	const handleMouseDownPassword = (event) => {
		event.preventDefault();
	};

	return (
		<PasswordRecoverLayout>
			<form onSubmit={formik.handleSubmit}>
				<FormControl
					fullWidth
					error={Boolean(formik.touched.password && formik.errors.password)}
					sx={{...theme.typography.customInput}}>
					<InputLabel htmlFor='outlined-adornment-password-login'>
						Contraseña Nueva
					</InputLabel>
					<OutlinedInput
						sx={{borderRadius: '10px'}}
						id='password'
						type={showPassword ? 'text' : 'password'}
						value={formik.values.password}
						name='password'
						onBlur={formik.handleBlur}
						onChange={formik.handleChange}
						endAdornment={
							<InputAdornment position='end'>
								<IconButton
									aria-label='toggle password visibility'
									onClick={handleClickShowPassword}
									onMouseDown={handleMouseDownPassword}
									edge='end'
									size='large'>
									{showPassword ? <Visibility /> : <VisibilityOff />}
								</IconButton>
							</InputAdornment>
						}
						label='Password'
						inputProps={{}}
					/>
					{formik.touched.password && formik.errors.password && (
						<FormHelperText error id='standard-weight-helper-text-password-login'>
							{formik.errors.password}
						</FormHelperText>
					)}
				</FormControl>

				<Box sx={{mt: 2}}>
					<Button
						disableElevation
						fullWidth
						size='large'
						type='submit'
						variant='contained'
						color='secondary'>
						Guardar
					</Button>
				</Box>
			</form>
		</PasswordRecoverLayout>
	);
};
