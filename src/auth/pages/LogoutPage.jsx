import {useEffect} from 'react';
import {useAuthStore} from '../../store';
import {useNavigate} from 'react-router-dom';

export const LogoutPage = () => {
	const setLogout = useAuthStore((state) => state.setLogout);
	const navigate = useNavigate();

	useEffect(() => {
		setLogout();
		navigate('auth/login');
	}, [navigate, setLogout]);
	return <div>LogoutPage</div>;
};
