import {
	Box,
	Button,
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
	Stack,
	Typography,
} from '@mui/material';
import {useFormik} from 'formik';
import * as Yup from 'yup';

import {useState} from 'react';
import {AuthLayout} from '../layout/AuthLayout';
import {useTheme} from '@emotion/react';
// import Google from '../../assets/images/icons/social-google.svg';
import {Visibility, VisibilityOff} from '@mui/icons-material';
import {loginRequests} from '../../api/auth';
import {useAuthStore} from '../../store';
import {Link} from 'react-router-dom';
const SignInSchema = Yup.object().shape({
	username: Yup.string().max(255).required('Email is required'),
	password: Yup.string().max(255).required('Password is required'),
});

const initialValues = {
	username: '',
	password: '',
	submit: null,
};

export const LoginPage = () => {
	// STORE
	const setToken = useAuthStore((state) => state.setToken);
	const setMenu = useAuthStore((state) => state.setMenu);
	const setUser = useAuthStore((state) => state.setUser);

	const theme = useTheme();
	// const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

	const [checked, setChecked] = useState(true);
	const [showPassword, setShowPassword] = useState(false);
	const handleClickShowPassword = () => {
		setShowPassword(!showPassword);
	};

	const handleMouseDownPassword = (event) => {
		event.preventDefault();
	};

	const onSubmit = async (values) => {
		const resLogin = await loginRequests(values);
		setToken(resLogin.data.token);
		setMenu(resLogin.data.menu);
		setUser({
			id: resLogin.data.id,
			name: resLogin.data.name,
			email: resLogin.data.email,
			rol: resLogin.data.rol,
		});
		setTimeout(() => {
			window.location = '/dashboard';
		}, 2000);
	};

	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: SignInSchema,
		onSubmit: onSubmit,
	});

	return (
		<>
			<AuthLayout>
				{/* <Grid container direction='column' justifyContent='center' spacing={2}>
					<Grid item xs={12}>
						<Button
							disableElevation
							disabled
							fullWidth
							onClick={() => () => {}}
							size='large'
							variant='outlined'
							sx={{
								color: 'grey.700',
								backgroundColor: theme.palette.grey[50],
								borderColor: theme.palette.grey[100],
							}}>
							<Box sx={{mr: {xs: 1, sm: 2, width: 20}}}>
								<img
									src={Google}
									alt='google'
									width={16}
									height={16}
									style={{marginRight: matchDownSM ? 8 : 16}}
								/>
							</Box>
							Entrar Con Google
						</Button>
					</Grid>
					<Grid item xs={12}>
						<Box
							sx={{
								alignItems: 'center',
								display: 'flex',
							}}>
							<Divider sx={{flexGrow: 1}} orientation='horizontal' />
							<Button
								variant='outlined'
								sx={{
									cursor: 'unset',
									m: 2,
									py: 0.5,
									px: 7,
									borderColor: `${theme.palette.grey[100]} !important`,
									color: `${theme.palette.grey[900]}!important`,
									fontWeight: 500,
									borderRadius: `10px`,
								}}
								disableRipple
								disabled>
								OR
							</Button>

							<Divider sx={{flexGrow: 1}} orientation='horizontal' />
						</Box>
					</Grid>
					<Grid item xs={12} container alignItems='center' justifyContent='center'>
						<Box sx={{mb: 2}}>
							<Typography variant='subtitle1'>Entrar con email o nombre de usuario</Typography>
						</Box>
					</Grid>
				</Grid> */}

				<form onSubmit={formik.handleSubmit}>
					<FormControl
						fullWidth
						error={Boolean(formik.touched.username && formik.errors.username)}
						sx={{...theme.typography.customInput}}>
						<InputLabel htmlFor='outlined-adornment-email-login'>
							Email / Usuario
						</InputLabel>
						<OutlinedInput
							id='outlined-adornment-email-login'
							value={formik.values.username}
							name='username'
							onBlur={formik.handleBlur}
							onChange={formik.handleChange}
							label='Email Address / Username'
							inputProps={{}}
							sx={{borderRadius: '10px'}}
						/>
						{formik.touched.username && formik.errors.username && (
							<FormHelperText error id='standard-weight-helper-text-email-login'>
								{formik.errors.username}
							</FormHelperText>
						)}
					</FormControl>

					{/* PASSWORD */}
					<FormControl
						fullWidth
						error={Boolean(formik.touched.password && formik.errors.password)}
						sx={{...theme.typography.customInput}}>
						<InputLabel htmlFor='outlined-adornment-password-login'>
							Password
						</InputLabel>
						<OutlinedInput
							sx={{borderRadius: '10px'}}
							id='outlined-adornment-password-login'
							type={showPassword ? 'text' : 'password'}
							value={formik.values.password}
							name='password'
							onBlur={formik.handleBlur}
							onChange={formik.handleChange}
							endAdornment={
								<InputAdornment position='end'>
									<IconButton
										aria-label='toggle password visibility'
										onClick={handleClickShowPassword}
										onMouseDown={handleMouseDownPassword}
										edge='end'
										size='large'>
										{showPassword ? <Visibility /> : <VisibilityOff />}
									</IconButton>
								</InputAdornment>
							}
							label='Password'
							inputProps={{}}
						/>
						{formik.touched.password && formik.errors.password && (
							<FormHelperText error id='standard-weight-helper-text-password-login'>
								{formik.errors.password}
							</FormHelperText>
						)}
					</FormControl>
					{/* REMEMBER ME */}
					<Stack
						direction='row'
						alignItems='center'
						justifyContent='space-between'
						spacing={1}>
						<FormControlLabel
							control={
								<Checkbox
									checked={checked}
									onChange={(event) => setChecked(event.target.checked)}
									name='checked'
									color='primary'
								/>
							}
							label='Recuérdame'
						/>
						<Typography
							variant='subtitle1'
							color='secondary'
							component={Link}
							to='/recover-password'
							sx={{textDecoration: 'none', cursor: 'pointer'}}>
							Olvidaste tu contraseña?
						</Typography>
					</Stack>
					{formik.errors.submit && (
						<Box sx={{mt: 3}}>
							<FormHelperText error>{formik.errors.submit}</FormHelperText>
						</Box>
					)}

					<Box sx={{mt: 2}}>
						{/* <AnimateButton> */}
						<Button
							disableElevation
							disabled={formik.isSubmitting}
							fullWidth
							size='large'
							type='submit'
							variant='contained'
							color='secondary'
							// component={Link}
							// to="dashboard/default"
							// to="#"
						>
							Ingresar
						</Button>
						{/* </AnimateButton> */}
					</Box>
				</form>
			</AuthLayout>
		</>
	);
};
