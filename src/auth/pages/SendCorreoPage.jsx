import {SendRecoverLayout} from '../layout/SendRecoverLayout';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {
	Box,
	Button,
	FormControl,
	FormHelperText,
	InputLabel,
	OutlinedInput,
	Typography,
} from '@mui/material';
import {useTheme} from '@emotion/react';

import {Link} from 'react-router-dom';
import {postRequest} from '../../api/Requests';
import {useState} from 'react';

const SignInSchema = Yup.object().shape({
	username: Yup.string().max(255).required('Email is required'),
});

const initialValues = {
	username: '',
};

export const SendCorreoPage = () => {
	const theme = useTheme();
	const [buttonDisable, setButtonDisable] = useState(true);

	const onSubmit = async () => {
		setButtonDisable(false);
		const correo = document.getElementById('correo').value;
		const response = await postRequest('/auth/recover-password', {
			email: correo,
		});
		if (response) setButtonDisable(true);
	};
	// FORMIK
	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: SignInSchema,
		onSubmit: onSubmit,
	});

	return (
		<SendRecoverLayout>
			<form onSubmit={formik.handleSubmit}>
				<FormControl
					fullWidth
					error={Boolean(formik.touched.username && formik.errors.username)}
					sx={{...theme.typography.customInput}}>
					<InputLabel htmlFor='outlined-adornment-email-login'>
						Email / Usuario
					</InputLabel>
					<OutlinedInput
						id='correo'
						value={formik.values.username}
						name='username'
						onBlur={formik.handleBlur}
						onChange={formik.handleChange}
						label='Email Address / Username'
						inputProps={{}}
						sx={{borderRadius: '10px'}}
					/>
					{formik.touched.username && formik.errors.username && (
						<FormHelperText error id='standard-weight-helper-text-email-login'>
							{formik.errors.username}
						</FormHelperText>
					)}
				</FormControl>

				<Typography
					variant='subtitle1'
					color='secondary'
					component={Link}
					to='/auth/login'
					sx={{textDecoration: 'none', cursor: 'pointer'}}>
					Iniciar Sesión?
				</Typography>
				<Box sx={{mt: 2}}>
					{buttonDisable ? (
						<Button
							disabled={false}
							disableElevation
							fullWidth
							size='large'
							type='submit'
							variant='contained'
							color='secondary'>
							Enviar
						</Button>
					) : (
						<Button
							disabled={true}
							disableElevation
							fullWidth
							size='large'
							type='submit'
							variant='contained'
							color='secondary'>
							Enviar
						</Button>
					)}
				</Box>
			</form>
		</SendRecoverLayout>
	);
};
