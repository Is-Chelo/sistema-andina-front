import {Box, Divider, Grid, Stack, Typography, useMediaQuery} from '@mui/material';
import PropTypes from 'prop-types';

import MainCard from '../../components/MainCard';
import {Link} from 'react-router-dom';
import {useTheme} from '@emotion/react';
import Logo from '../../components/Logo';

export const PasswordRecoverLayout = ({children}) => {
	const theme = useTheme();
	const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

	return (
		<Grid
			sx={{
				backgroundColor: 'primary.light',
				minHeight: '100vh',
			}}
			container
			spacing={0}
			direction='column'
			justifyContent='center'
			alignItems='center'>
			<MainCard
				sx={{
					maxWidth: {xs: 400, lg: 475},
					margin: {xs: 2.5, md: 3},
					'& > *': {
						flexGrow: 1,
						flexBasis: '50%',
					},
				}}
				content={false}>
				<Box sx={{p: {xs: 2, sm: 3, xl: 5}}}>
					<Grid container spacing={2} alignItems='center' justifyContent='center'>
						<Grid item sx={{mb: 3}}>
							<Link to='#'>
								<Logo />
							</Link>
						</Grid>
						<Grid item xs={12}>
							<Grid
								container
								direction={matchDownSM ? 'column-reverse' : 'row'}
								alignItems='center'
								justifyContent='center'>
								<Grid item>
									<Stack alignItems='center' justifyContent='center' spacing={1}>
										<Typography
											color={theme.palette.secondary.main}
											gutterBottom
											variant={matchDownSM ? 'h3' : 'h3'}>
											Introduce tu Contraseña nueva por favor.
										</Typography>
									</Stack>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={12}>
							{children}
						</Grid>
						<Grid item xs={12}>
							<Divider />
						</Grid>
					</Grid>
				</Box>
			</MainCard>
		</Grid>
	);
};

PasswordRecoverLayout.propTypes = {
	children: PropTypes.node.isRequired,
};
