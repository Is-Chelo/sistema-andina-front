import axios from './axiosConfig';
import {toast} from 'react-toastify';
// LOGIN
export const getRequests = async (url) => {
	try {
		const resp = await axios(url);
		return resp;
	} catch (error) {
		errorHandling(error);
		throw new Error(error);
	}
};

export const getPDF = async (url, nameFile) => {
	try {
		const resp = await axios(url, {
			responseType: 'blob',
		});
		const url2 = window.URL.createObjectURL(new Blob([resp.data]));
		const link = document.createElement('a');
		link.href = url2;
		link.setAttribute('download', `${nameFile}.pdf`);
		document.body.appendChild(link);
		link.click();
	} catch (error) {
		errorHandling(error);
		throw new Error(error);
	}
};

export const getRequest = async (url) => {
	try {
		const resp = await axios(url);
		return resp.data.data;
	} catch (error) {
		errorHandling(error);
		throw new Error(error);
	}
};

export const postRequest = async (url, values = {}) => {
	try {
		const response = await axios.post(`/api/v1${url}`, values);
		if (response.data.status) {
			const {data} = response;
			if (data) {
				toast.success(data.message[0], {
					position: 'top-right',
					autoClose: 3000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
					theme: 'light',
				});
				return response.data;
			}
		} else {
			toast.error('Ocurrió un error en el servidor, por favor contactate con soporte', {
				position: 'top-right',
				autoClose: 3000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
				theme: 'light',
			});
		}
	} catch (error) {
		return errorHandling(error);
	}
};

export const updateRequest = async (id, url, values = {}) => {
	try {
		const response = await axios.put(`/api/v1${url}/${id}`, values);
		if (response.data.status) {
			const {data} = response;
			if (data) {
				toast.success(data.message[0], {
					position: 'top-right',
					autoClose: 3000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
					theme: 'light',
				});
				return response.data;
			}
		} else {
			toast.error('Ocurrió un error en el servidor, por favor contactate con soporte', {
				position: 'top-right',
				autoClose: 3000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
				theme: 'light',
			});
		}
	} catch (error) {
		return errorHandling(error);
	}
};
export const updateAllRequest = async (url, values = {}) => {
	try {
		const response = await axios.put(`/api/v1${url}`, values);
		if (response.data.status) {
			const {data} = response;
			if (data) {
				toast.success(data.message[0], {
					position: 'top-right',
					autoClose: 3000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
					theme: 'light',
				});
				return response.data;
			}
		}
	} catch (error) {
		return errorHandling(error);
	}
};

export const deleteRequest = async (id, url) => {
	try {
		const response = await axios.delete(`/api/v1${url}/${id}`);
		if (response.data.status) {
			const {data} = response;
			if (data) {
				toast.success(data.message[0], {
					position: 'top-right',
					autoClose: 3000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
					theme: 'light',
				});
				return response.data;
			}
		} else {
			toast.error('Ocurrió un error en el servidor, por favor contactate con soporte', {
				position: 'top-right',
				autoClose: 3000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
				theme: 'light',
			});
		}
	} catch (error) {
		errorHandling(error);
		throw new Error(error);
	}
};

// TODO: Manejo de Errores
const errorHandling = (error) => {
	// ERRORS DE PERMISO
	if (error.response.status === 403) {
		let message = '';
		error.response.data.message.forEach((msg) => {
			message += msg + '\n';
		});
		toast.error(message, {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
			theme: 'light',
		});
		return {
			status: error.response.data.status,
			message: message,
		};
	}
	if (error.response.status === 401) {
		let message = '';
		error.response.data.message.forEach((msg) => {
			message += msg + '\n';
		});
		toast.error(message, {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
			theme: 'light',
		});
		setTimeout(() => {
			window.location = '/auth/logout';
		}, 2000);
		return {
			status: error.response.data.status,
			message: message,
		};
	}
	// ERRORES DE CLIENTE
	if (error.response.status === 404 || error.response.status === 400) {
		let message = '';
		error.response.data.message.forEach((msg) => {
			message += msg + '\n';
		});
		toast.error(message, {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
			theme: 'light',
		});
		return {
			status: error.response.data.status,
			message: message,
		};
	}
	if (error.response.status >= 500) {
		toast.error('Ocurrió un error en el servidor', {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
			theme: 'light',
		});
	}
};
