import axios from 'axios';
import {useAuthStore} from '../store';
import {getEnvVariable} from './getEnvVariables';
const aixosGeneral = axios.create({
	baseURL: getEnvVariable().VITE_REACT_APP_API_URL,
	withCredentials: true,
});

aixosGeneral.interceptors.request.use((config) => {
	// Recogemos el token que almacenamos en zustand
	const token = useAuthStore.getState().token;

	config.headers = {
		Authorization: `Bearer ${token}`,
	};
	return config;
});

export default aixosGeneral;
