import axios from './axiosConfig';
import {toast} from 'react-toastify';
// LOGIN
export const loginRequests = async (values = {}) => {
	try {
		const response = await axios.post('/api/v1/auth/login', values);
		if (response.data.status) {
			const {data} = response;
			if (data) {
				toast.success(data.message[0], {
					position: 'top-right',
					autoClose: 3000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
					theme: 'light',
				});
				return response.data;
			}
		}
	} catch (error) {
		errorHandling(error);
		throw new Error(error);
	}
};

// TODO: Manejo de Errores
const errorHandling = (error) => {
	if (error.response.status === 404) {
		let message = '';
		error.response.data.message.forEach((msg) => {
			message += msg + '\n';
		});
		toast.error(message, {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
			theme: 'light',
		});
	} else {
		toast.error('Ocurrió un error en el servidor', {
			position: 'top-right',
			autoClose: 3000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
			theme: 'light',
		});
	}
};
