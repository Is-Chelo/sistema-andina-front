import {useEffect, useState} from 'react';
import {getRequests} from '../api/Requests';

export const useFetch = (url, change) => {
	const [state, setState] = useState();
	const getFetch = async () => {
		const resp = await getRequests(url);
		const data = resp.data.data;
		setState(data);
	};

	useEffect(() => {
		getFetch();
	}, [change]);

	return state;
};
