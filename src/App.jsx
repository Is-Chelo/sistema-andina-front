import AppRouter from './router/AppRouter';
import 'react-toastify/dist/ReactToastify.css';
import 'animate.css/animate.min.css';
export const App = () => {
	return <AppRouter />;
};
