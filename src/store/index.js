import create from 'zustand';
import {persist} from 'zustand/middleware';

export const useAuthStore = create(
	persist(
		(set) => ({
			token: '',
			isAuth: false,
			menu: [],
			id_menu: 0,
			user: {},
			setToken: (token) => {
				set({
					token,
					isAuth: true,
				});
			},
			setUser: (user) => {
				set({
					user,
				});
			},
			setMenu: (menu) => {
				set({
					menu,
				});
			},
			setIdMenu: (id_menu) => {
				set({
					id_menu,
				});
			},
			setLogout: () => {
				set({
					token: '',
					isAuth: false,
					menu: [],
				});
			},
		}),
		{
			name: 'andina',
			only: ['token', 'isAuth'],
		}
	)
);
